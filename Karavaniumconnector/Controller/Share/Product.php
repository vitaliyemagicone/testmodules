<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 5/23/2018
 * Time: 7:16 PM
 */

namespace Emagicone\Karavaniumconnector\Controller\Share;

use \Magento\Framework\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\App\Action\Action as Action;

class Product extends Action
{
    protected $response;
    protected $_pageFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $resultPageLayout = $this->_pageFactory->create();
        return $resultPageLayout;
    }
}