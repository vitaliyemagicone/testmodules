<?php
/**
 *    This file is part of Karavanium Connector.
 *
 *   Karavanium Connector is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Karavanium Connector is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Karavanium Connector.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Emagicone\Karavaniumconnector\Controller\Index;

use Magento\Framework\Filesystem                                        as FileSystem;
use Magento\Catalog\Model\Product\Attribute\Source\Status               as ProductStatus;
use Magento\Shipping\Model\Config\Source\Allmethods                     as ShippingAllMethods;
use Magento\Shipping\Helper\Carrier                                     as CarrierHelper;
use Magento\Shipping\Model\Rate\Result                                  as ShippingModelResult;
use Magento\Catalog\Model\Product\Visibility                            as ProductVisibility;
use Magento\Quote\Api\Data\EstimateAddressInterfaceFactory              as EstimatedAddressFactory;
use Magento\Customer\Api\GroupManagementInterface                       as CustomerGroupManagement;
use Magento\Customer\Api\GroupRepositoryInterface                       as CustomerGroupRepository;
use Magento\Payment\Helper\Data                                         as PaymentHelper;
use Magento\Catalog\Model\Product                                       as ProductModel;
use Magento\Sales\Api\OrderRepositoryInterface                          as OrderRepository;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface             as OrderStatusHistoryRepository;
use Magento\Customer\Api\CustomerRepositoryInterface                    as CustomerRepository;
use Magento\Sales\Api\OrderManagementInterface                          as OrderManagement;
use Emagicone\Karavaniumconnector\Helper\Access;
use Emagicone\Karavaniumconnector\Helper\AESCiphers;
use Emagicone\Karavaniumconnector\Helper\Constants;
use Emagicone\Karavaniumconnector\Helper\Responses;
use Emagicone\Karavaniumconnector\Helper\Tools;
use Magento\Framework\App\Action\Action;
use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Model\AccountManagement;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Model\Customer\Attribute\Backend\Password;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\RegionInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\UrlInterface;
use Magento\Framework\Webapi\Response;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote\Address\Rate;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Directory\Model\CountryFactory;
use Magento\Directory\Model\RegionFactory;
use Magento\Quote\Api\PaymentMethodManagementInterface;
use Magento\Quote\Api\ShippingMethodManagementInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Cart\ShippingMethod;
use Magento\Quote\Api\Data\ShippingMethodInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend;
use Magento\CatalogInventory\Model\StockRegistryProvider;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Url\DecoderInterface;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;

/**
 * Class Index
 * @package Emagicone\Karavaniumconnector\Controller\Index
 */
// todo: andriy - fix issue with vendor product creation
// todo: andriy - add vendor product creation refresh dialog
// todo: andriy - fix issue with fie
//ld padding
class Index extends Action implements CsrfAwareActionInterface
{
    private $action;
    private $username;
    private $password;
    private $user_role;
    private $def_currency;
    private $currency_code;
    private $page;
    private $vendor_id;
    private $user_id;
    private $data;
    private $product_id;
    private $product_ids;
    private $all;
    private $qty;
    private $key = '';
    private $address_id = 0;
    private $address_ids;
    private $device_token = '';
    private $text;
    private $order_id;
    private $rating;
    private $sort_by;
    private $image_size;
    private $page_size;
    private $tag_id;
    private $ids;
    private $address;

    protected $accountManagement;
    protected $adapterFactory;
    protected $addressFactory;
    protected $addressInterface;
    protected $addressInterfaceFactory;
    protected $addressRepositoryInterface;
    protected $arrayBackend;
    protected $carrierAbstract;
    protected $carrierHelper;
    protected $cartManagementInterface;
    protected $cartRepositoryInterface;
    protected $collectionFactory;
    protected $context;
    protected $countryCollectionFactory;
    protected $countryFactory;
    protected $customerFactory;
    protected $customerGroupManagement;
    protected $customerGroupRepository;
    protected $customerInterface;
    protected $customerInterfaceFactory;
    protected $customerRepository;
    protected $customerRepositoryInterface;
    protected $decoderInterface;
    protected $directoryList;
    protected $estimatedAddressFactory;
    protected $file;
    protected $fileFactory;
    protected $fileSystem;
    protected $filterBuilder;
    protected $filterGroup;
    protected $paymentHelper;
    protected $paymentMethodManagementInterface;
    protected $productInterfaceFactory;
    protected $productRepository;
    protected $productRepositoryInterface;
    protected $productStatus;
    protected $productVisibility;
    protected $rate;
    protected $rateRequest;
    protected $rawFactory;
    protected $regionFactory;
    protected $regionInterface;
    protected $regionInterfaceFactory;
    protected $response;
    protected $scopeConfigInterface;
    protected $searchCriteriaBuilder;
    protected $searchCriteriaInterface;
    protected $shippingAllMethods;
    protected $shippingMethod;
    protected $shippingMethodInterface;
    protected $shippingMethodManagementInterface;
    protected $shippingModelResult;
    protected $shippingRate;
    protected $stockItemInterface;
    protected $stockRegistryProvider;
    protected $store;
    protected $storeManagerInterface;
    protected $storeModel;
    protected $uploaderFactory;
    protected $urlInterface;
    protected $sortOrder;
    protected $orderRepository;
    protected $orderStatusHistoryRepository;
    protected $orderManagement;
    protected $filterGroupBuilder;
    protected $stockRegistryInterface;
    protected $productModel;
    protected $productCollection;

    /**
     * @param AccountManagement $accountManagement
     * @param AdapterFactory $adapterFactory
     * @param AddressFactory $addressFactory
     * @param AddressInterface $addressInterface
     * @param AddressInterfaceFactory $addressInterfaceFactory
     * @param AddressRepositoryInterface $addressRepositoryInterface
     * @param ArrayBackend $arrayBackend
     * @param CarrierHelper $carrierHelper
     * @param CartManagementInterface $cartManagementInterface
     * @param CartRepositoryInterface $cartRepositoryInterface
     * @param CollectionFactory $countryCollectionFactory
     * @param Context $context
     * @param CountryFactory $countryFactory
     * @param CustomerFactory $customerFactory
     * @param CustomerGroupManagement $customerGroupManagement
     * @param CustomerGroupRepository $customerGroupRepository
     * @param CustomerInterface $customerInterface
     * @param CustomerInterfaceFactory $customerInterfaceFactory
     * @param CustomerRepository $customerRepositoryInterface
     * @param DecoderInterface $decoderInterface
     * @param EstimatedAddressFactory $estimatedAddressFactory
     * @param File $file
     * @param FileFactory $fileFactory
     * @param FileSystem $fileSystem
     * @param FilterBuilder $filterBuilder
     * @param FilterGroup $filterGroup
     * @param PaymentHelper $paymentHelper
     * @param PaymentMethodManagementInterface $paymentMethodManagementInterface
     * @param ProductInterfaceFactory $productInterfaceFactory
     * @param ProductRepository $productRepository
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param ProductStatus $productStatus
     * @param ProductVisibility $productVisibility
     * @param Rate $shippingRate
     * @param RateRequest $rateRequest
     * @param RawFactory $rawFactory
     * @param RegionFactory $regionFactory
     * @param RegionInterface $regionInterface
     * @param RegionInterfaceFactory $regionInterfaceFactory
     * @param Response $response
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SearchCriteriaInterface $searchCriteriaInterface
     * @param ShippingAllMethods $shippingAllMethods
     * @param ShippingMethod $shippingMethod
     * @param ShippingMethodInterface $shippingMethodInterface
     * @param ShippingMethodManagementInterface $shippingMethodManagementInterface
     * @param ShippingModelResult $shippingModelResult
     * @param StockItemInterface $stockItemInterface
     * @param StockRegistryProvider $stockRegistryProvider
     * @param Store $store
     * @param StoreManagerInterface $storeManagerInterface
     * @param UploaderFactory $uploaderFactory
     * @param SortOrder $sortOrder
     * @param OrderRepository $orderRepository
     * @param OrderStatusHistoryRepository $orderStatusHistoryRepository
     * @param OrderManagement $orderManagement
     * @param FilterGroupBuilder $filterGroupBuilder;
     * @param StockRegistryInterface $stockRegistryInterface;
     * @param ProductModel $productModel;
     * @param ProductCollection $productCollection;
     */

    public function __construct(
        AccountManagement $accountManagement,
        AdapterFactory $adapterFactory,
        AddressFactory $addressFactory,
        AddressInterface $addressInterface,
        AddressInterfaceFactory $addressInterfaceFactory,
        AddressRepositoryInterface $addressRepositoryInterface,
        ArrayBackend $arrayBackend,
        CarrierHelper $carrierHelper,
        CartManagementInterface $cartManagementInterface,
        CartRepositoryInterface $cartRepositoryInterface,
        CollectionFactory $countryCollectionFactory,
        Context $context,
        CountryFactory $countryFactory,
        CustomerFactory $customerFactory,
        CustomerGroupManagement $customerGroupManagement,
        CustomerGroupRepository $customerGroupRepository,
        CustomerInterface $customerInterface,
        CustomerInterfaceFactory $customerInterfaceFactory,
        CustomerRepository $customerRepositoryInterface,
        DecoderInterface $decoderInterface,
        EstimatedAddressFactory $estimatedAddressFactory,
        File $file,
        FileFactory $fileFactory,
        FileSystem $fileSystem,
        FilterBuilder $filterBuilder,
        FilterGroup $filterGroup,
        PaymentHelper $paymentHelper,
        PaymentMethodManagementInterface $paymentMethodManagementInterface,
        ProductInterfaceFactory $productInterfaceFactory,
        ProductRepository $productRepository,
        ProductRepositoryInterface $productRepositoryInterface,
        ProductStatus $productStatus,
        ProductVisibility $productVisibility,
        Rate $shippingRate,
        RateRequest $rateRequest,
        RawFactory $rawFactory,
        RegionFactory $regionFactory,
        RegionInterface $regionInterface,
        RegionInterfaceFactory $regionInterfaceFactory,
        Response $response,
        ScopeConfigInterface $scopeConfigInterface,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SearchCriteriaInterface $searchCriteriaInterface,
        ShippingAllMethods $shippingAllMethods,
        ShippingMethod $shippingMethod,
        ShippingMethodInterface $shippingMethodInterface,
        ShippingMethodManagementInterface $shippingMethodManagementInterface,
        ShippingModelResult $shippingModelResult,
        StockItemInterface $stockItemInterface,
        StockRegistryProvider $stockRegistryProvider,
        Store $store,
        StoreManagerInterface $storeManagerInterface,
        UploaderFactory $uploaderFactory,
        SortOrder $sortOrder,
        OrderRepository $orderRepository,
        OrderStatusHistoryRepository $orderStatusHistoryRepository,
        OrderManagement $orderManagement,
        FilterGroupBuilder $filterGroupBuilder,
        StockRegistryInterface $stockRegistryInterface,
        ProductModel $productModel,
        ProductCollection $productCollection
    ) {
        $this->accountManagement = $accountManagement;
        $this->adapterFactory = $adapterFactory;
        $this->addressFactory = $addressFactory;
        $this->addressInterface = $addressInterface;
        $this->addressInterfaceFactory = $addressInterfaceFactory;
        $this->addressRepositoryInterface = $addressRepositoryInterface;
        $this->arrayBackend = $arrayBackend;
        $this->carrierHelper = $carrierHelper;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->context = $context;
        $this->countryCollectionFactory = $countryCollectionFactory;
        $this->countryFactory = $countryFactory;
        $this->customerFactory = $customerFactory;
        $this->customerGroupManagement = $customerGroupManagement;
        $this->customerGroupRepository = $customerGroupRepository;
        $this->customerInterface = $customerInterface;
        $this->customerInterfaceFactory = $customerInterfaceFactory;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->decoderInterface = $decoderInterface;
        $this->estimatedAddressFactory = $estimatedAddressFactory;
        $this->file = $file;
        $this->fileFactory = $fileFactory;
        $this->fileSystem = $fileSystem;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroup = $filterGroup;
        $this->paymentHelper = $paymentHelper;
        $this->paymentMethodManagementInterface = $paymentMethodManagementInterface;
        $this->productInterfaceFactory = $productInterfaceFactory;
        $this->productRepository = $productRepository;
        $this->productRepositoryInterface = $productRepositoryInterface;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->rateRequest = $rateRequest;
        $this->rawFactory = $rawFactory;
        $this->regionFactory = $regionFactory;
        $this->regionInterface = $regionInterface;
        $this->regionInterfaceFactory = $regionInterfaceFactory;
        $this->response = $response;
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->searchCriteriaInterface = $searchCriteriaInterface;
        $this->shippingAllMethods = $shippingAllMethods;
        $this->shippingMethod = $shippingMethod;
        $this->shippingMethodInterface = $shippingMethodInterface;
        $this->shippingMethodManagementInterface = $shippingMethodManagementInterface;
        $this->shippingModelResult = $shippingModelResult;
        $this->shippingRate = $shippingRate;
        $this->stockItemInterface = $stockItemInterface;
        $this->stockRegistryProvider = $stockRegistryProvider;
        $this->store = $store;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->uploaderFactory = $uploaderFactory;
        $this->sortOrder = $sortOrder;
        $this->orderRepository = $orderRepository;
        $this->orderStatusHistoryRepository = $orderStatusHistoryRepository;
        $this->orderManagement = $orderManagement;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->stockRegistryInterface = $stockRegistryInterface;
        $this->productModel = $productModel;
        $this->productCollection = $productCollection;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        $this->indexAction();
        return $this->_response;
    }

    private function indexAction()
    {
        //Access::clearOldData();

        /**
         * Authorisation section
         */
        if (!$this->getRequest()->has(Constants::PARAM_ACTION)) {
            return $this->generateOutput(Responses::ERROR_UNKNOWN_ACTION);
        }

        $this->action = (string)$this->getRequest()->get(Constants::PARAM_ACTION);
        $requestValues = Constants::AUTH_REQUEST_VALUES;
        foreach ($requestValues as $requestValue) {
            if ($requestValue === Responses::USER_ID) {
                $this->user_id = (int)AESCiphers::decrypt($this->getRequest()->get($requestValue));
                continue;
            }

            if ($this->getRequest()->has($requestValue)) {
                $this->{$requestValue} = $this->getRequest()->get($requestValue);
            }
        }

        if (in_array($this->action , Constants::ALLOWED_ACTIONS, true)) {
            return $this->generateOutput($this->{Constants::ACTION_CALL[$this->action]}());
        }

        if (empty($this->key)) {
            return $this->generateOutput(Responses::ERROR_AUTH_ERROR);
        }

        if (!Access::checkSessionKey($this->user_id, $this->key)) {
            return $this->generateOutput(Responses::ERROR_SESSION_KEY_EXPIRED);
        }

        $params = $this->validateTypes($this->getRequest()->getParams(), Constants::VALIDATION_TYPE_ENUM);
        foreach ($params as $key => $value) {
            if ($key !== Responses::USER_ID) {
                $this->{$key} = $value;
            }
        }

        $this->page         = (empty($this->page) || $this->page < 1) ? 1 : $this->page;
        $this->page_size    = (empty($this->page_size) || $this->page_size < 0)
            ? Constants::PRODUCT_PAGE_LIMIT
            : $this->page_size;

        $this->action = self::snakeCaseToCamelCase($this->action);
        if (!method_exists($this, $this->action)) {
            return $this->generateOutput(Responses::ERROR_METHOD_MISSING);
        }

        $this->generateOutput($this->{$this->action}());
        exit;
    }

    /**
     * @param $array
     * @param $names
     * @return mixed
     */
    private function validateTypes($array, $names)
    {
        foreach ($names as $name => $type) {
            if (isset($array[(string)$name])) {
                switch ($type) {
                    case 'INT':
                        $array[(string)$name] = (int)$array[(string)$name];
                        break;
                    case 'FLOAT':
                        $array[(string)$name] = (float)$array[(string)$name];
                        break;
                    case 'DOUBLE':
                        $array[(string)$name] = (double)$array[(string)$name];
                        break;
                    case 'STR':
                        $array[(string)$name] = str_replace(
                            ["\r", "\n"],
                            ' ',
                            addslashes(htmlspecialchars(trim(urldecode($array[(string)$name]))))
                        );
                        break;
                    case 'STR_HTML':
                        $array[(string)$name] = addslashes(trim($array[(string)$name]));
                        break;
                    case 'HTML_REQUEST':
                        $array[(string)$name] = trim($array[(string)$name]);
                        break;
                    default:
                        $array[(string)$name] = '';
                }
            } else {
                $array[(string)$name] = '';
            }
        }

        $arrayKeys = array_keys($array);

        foreach ($arrayKeys as $key) {
            if (!isset($names[$key])) {
                $array[$key] = '';
            }
        }

        return $array;
    }

    private function generateOutput($data)
    {
        if (!is_array($data)) {
            $data = [$data];
        }

        array_walk_recursive($data, [$this, 'resetNull']);
        $data = Tools::jsonEncode($data);

        // todo:
        //  with json response
        //  \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        //  $this->resultJsonFactory = $resultJsonFactory;
        //  $resultJson = $this->resultJsonFactory->create();
        //  return $resultJson->setData([
        //      'success' => [
        //          'categoryCatalog' => $categoryCatalog,
        //          'attribute' => $attribute,
        //          'options' => $options,
        //          'collection' => $products
        //      ]
        //  ]);
        /** @var \Magento\Framework\App\Response\Http\Interceptor $_response */
        $this->_response->setHeader('Content-Type', 'application/json;charset=utf-8');
        $this->_response->setContent($data);
    }

    // User/Vendor Registration/Login

    private function userRegister()
    {
        $decryptedUsername = AESCiphers::decrypt($this->username);
        $decryptedPassword = AESCiphers::decrypt($this->password);

        if (empty($decryptedUsername) || empty($decryptedPassword)) {
            return Responses::ERROR_INCORRECT_CREDENTIALS;
        }

        if (strlen($decryptedPassword) < Password::MIN_PASSWORD_LENGTH) {
            return [Responses::ERROR => 'incorrect_password_length', 'min_length' => Password::MIN_PASSWORD_LENGTH];
        }

        try {
            $searchCriteria = $this->searchCriteriaBuilder->addFilter(Constants::ATTRIBUTE_KARAVANIUM_USERNAME, $decryptedUsername)->create();
            $customerRepository = $this->customerRepositoryInterface->getList($searchCriteria);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage(), 'min_length' => Password::MIN_PASSWORD_LENGTH];
        }

        if (count($customerRepository->getItems()) > 0) {
            return [Responses::ERROR_ALREADY_REGISTERED, 'min_length' => Password::MIN_PASSWORD_LENGTH];
        }

        $customer = $this->customerFactory->create();
        /** @var \Magento\Customer\Model\Customer\Interceptor  $customer */ //for password
        /** @var \Magento\Customer\Model\Data\Customer $customer */
        $customer
            ->setEmail($decryptedUsername . '@karavanium.com')
            ->setFirstname($decryptedUsername)
            ->setLastname($decryptedUsername)
            ->setPassword($decryptedPassword);

        $customerData = $customer->getDataModel()
            ->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_USERNAME, $decryptedUsername)
            ->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_ROLE, $this->user_role);

        try {
            $customer->updateData($customerData)->save();
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $this->username = AESCiphers::encrypt($decryptedUsername);
        return $this->userLogin();
    }

    private function userLogin($customer = null)
    {
        if ($customer === null) {
            if (empty($this->username) || empty($this->password)) {
                return Responses::ERROR_INCORRECT_CREDENTIALS;
            }

            $userLoginData = Access::getSessionKey(
                $this->username,
                $this->password,
                $customer,
                $this->device_token
            );
        } else {
            $userLoginData = Access::getSessionKey(
                $this->username,
                $this->password,
                $customer,
                $this->device_token
            );
        }

        if (array_key_exists(Responses::ERROR, $userLoginData)) {
            return $userLoginData;
        }

        $this->user_id  = $userLoginData[Responses::USER_ID];
        $userCartData   = $this->getCartSummary($this->user_id);
        $userAllData    = array_merge($userLoginData, $this->getProfileInfoClient(), $userCartData);

        return $userAllData;
    }

    private function getUserDeviceTokens($userId)
    {
        $userDeviceTokensFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\UserDeviceTokens');
        $userDeviceTokensCollection = $userDeviceTokensFactory->getCollection()
            ->addFieldToFilter(Constants::COLUMN_USER_ID, ['eq' => $userId]);

        $userDeviceTokens = [];
        if ($userDeviceTokensCollection->getSize() < 1) {
            return $userDeviceTokens;
        }

        foreach ($userDeviceTokensCollection->getData() as $userDevice) {
            $userDeviceTokens[] = $userDevice;
        }

        return $userDeviceTokens;
    }

    private function checkUserExists()
    {
        $decryptedUsername = AESCiphers::decrypt($this->username);
        $decryptedPassword = AESCiphers::decrypt($this->password);

        if (empty($decryptedUsername) || empty($decryptedPassword)) {
            return [Responses::ERROR_INCORRECT_CREDENTIALS];
        }

        $userExists = false;
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(Constants::ATTRIBUTE_KARAVANIUM_USERNAME, $decryptedUsername)->create();
        try {
            $customerRepository = $this->customerRepositoryInterface->getList($searchCriteria);
            if ($customerRepository->getTotalCount() > 0) {
                $userExists = true;
            }
        } catch (\Exception $e) {
            return [Responses::ERROR => 'failed_to_load_object',];
        }

        // todo: type  casting
        if (strlen($decryptedPassword) < Password::MIN_PASSWORD_LENGTH) {
            return [
                'exists'                    => (int)$userExists,
                'min_length'                => (int)Password::MIN_PASSWORD_LENGTH,
                'incorrect_password_length' => 1
            ];
        }

        return [
            'exists'                    => (int)$userExists,
            'min_length'                => (int)Password::MIN_PASSWORD_LENGTH,
            'incorrect_password_length' => 0
        ];
    }

    // User/Vendor Profile Get/Update

    private function getProfileInfoClient()
    {
        $userId = $this->user_id;
        try {
            $customer = $this->customerRepositoryInterface->getById($userId);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_USER_ID;
        }

        $customerFullName = $customer->getFirstname() . ' ' . $customer->getLastname();
        $customerUsername = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_USERNAME);
        $customerVendorName = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME);
        $customerAvatar = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR);

        $customerUsername = empty($customerUsername) ? '' : $customerUsername->getValue();
        $customerVendorNameValue = empty($customerVendorName) ? '' : $customerVendorName->getValue();
        $customerAvatarValue = empty($customerAvatar) ? '' : $customerAvatar->getValue();

        $avatarUrl = '';
        if (!empty($customerAvatarValue)) {
            $avatarUrl = $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $customerAvatarValue;
        }

        return [
            Responses::KEY_USERNAME => $customerUsername,
            Responses::KEY_VENDOR_NAME => $customerVendorNameValue,
            Responses::KEY_FULLNAME => $customerFullName,
            Responses::KEY_USER_AVATAR => $avatarUrl
        ];
    }

    private function getProfileInfoVendor()
    {
        $userId = $this->user_id;

        try {
            $customer = $this->customerRepositoryInterface->getById($userId);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_USER_ID;
        }

        $customerFullName       = $customer->getFirstname() . ' ' . $customer->getLastname();
        $customerUsername       = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_USERNAME);
        $customerVendorName     = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME);
        $customerVendorCountry  = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_COUNTRY);
        $customerAvatar         = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR);

        $customerUsername           = empty($customerUsername) ? '' : $customerUsername->getValue();
        $customerVendorNameValue    = empty($customerVendorName) ? '' : $customerVendorName->getValue();
        $customerVendorCountryValue = empty($customerVendorCountry) ? '' : $customerVendorCountry->getValue();
        $customerAvatarValue        = empty($customerAvatar) ? '' : $customerAvatar->getValue();

        $avatarUrl = '';
        if (!empty($customerAvatarValue)) {
            $avatarUrl = $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $customerAvatarValue;
        }

        return [
            Responses::KEY_USERNAME => $customerUsername,
            Responses::KEY_VENDOR_NAME => $customerVendorNameValue,
            Responses::KEY_VENDOR_COUNTRY => $customerVendorCountryValue,
            Responses::KEY_FULLNAME => $customerFullName,
            Responses::KEY_USER_AVATAR => $avatarUrl
        ];
    }

    private function getProfileClient()
    {
        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_USER_ID;
        }

        $customerEmail              = $customer->getEmail();
        $customerFirstName          = $customer->getFirstname();
        $customerLastName           = $customer->getLastname();
        $customerVendorName         = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME);
        $customerVendorNameValue    = empty($customerVendorName) ? '' : $customerVendorName->getValue();
        $customerAvatar             = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR);
        $customerAvatarValue        = empty($customerAvatar) ? '' : $customerAvatar->getValue();

        $avatarUrl = '';
        if (!empty($customerAvatarValue)) {
            $avatarUrl = $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $customerAvatarValue;
        }

        return [
            Responses::KEY_EMAIL                => $customerEmail,
            Responses::KEY_VENDOR_NAME          => $customerVendorNameValue,
            Responses::KEY_FIRSTNAME            => $customerFirstName,
            Responses::KEY_LASTNAME             => $customerLastName,
            Responses::KEY_FULLNAME             => $customerFirstName . ' ' . $customerLastName,
            Responses::KEY_USER_AVATAR          => $avatarUrl,
            Responses::MAX_FILE_UPLOAD_IN_BYTES => $this->getMaxFileUploadInBytes()
        ];
    }

    private function getProfileVendor()
    {
        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_USER_ID;
        }

        $customerEmail              = $customer->getEmail();
        $customerFirstName          = $customer->getFirstname();
        $customerLastName           = $customer->getLastname();
        $customerVendorName         = $customer->getCustomAttribute(
            Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME
        );
        $customerVendorNameValue    = empty($customerVendorName) ? '' : $customerVendorName->getValue();
        $customerVendorCountry      = $customer->getCustomAttribute(
            Constants::ATTRIBUTE_KARAVANIUM_VENDOR_COUNTRY
        );
        $customerVendorCountryValue = empty($customerVendorCountry) ? '' : $customerVendorCountry->getValue();
        $customerAvatar             = $customer->getCustomAttribute(
            Constants::ATTRIBUTE_KARAVANIUM_AVATAR
        );
        $customerAvatarValue        = empty($customerAvatar) ? '' : $customerAvatar->getValue();

        $avatarUrl = '';
        if (!empty($customerAvatarValue)) {
            $avatarUrl = $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $customerAvatarValue;
        }

        return array_merge(
            [
                Responses::KEY_EMAIL                => $customerEmail,
                Responses::KEY_VENDOR_NAME          => $customerVendorNameValue,
                Responses::KEY_VENDOR_COUNTRY       => $customerVendorCountryValue,
                Responses::KEY_FIRSTNAME            => $customerFirstName,
                Responses::KEY_LASTNAME             => $customerLastName,
                Responses::KEY_FULLNAME             => $customerFirstName . ' ' . $customerLastName,
                Responses::KEY_USER_AVATAR          => $avatarUrl,
                Responses::MAX_FILE_UPLOAD_IN_BYTES => $this->getMaxFileUploadInBytes()
            ],
            $this->getAddressExtraData()
        );
    }

    private function updateProfileClient()
    {
        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        if (!empty($this->data)) {
            $data = null;
            @$data = Tools::jsonDecode($this->data);
            if (!$data) {
                return Responses::ERROR_INCORRECT_FORMAT_OF_DATA;
            }

            $customerFirstName          = $customer->getFirstname();
            $customerLastName           = $customer->getLastname();
            $customerEmail              = $customer->getEmail();
            $customerVendorName         = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME);
            $customerVendorNameValue    = empty($customerVendorName) ? '' : $customerVendorName->getValue();

            if (!empty($data[Responses::KEY_FIRSTNAME]) && $customerFirstName != $data[Responses::KEY_FIRSTNAME]) {
                $customer->setFirstname($data[Responses::KEY_FIRSTNAME]);
            }

            if (!empty($data[Responses::KEY_LASTNAME]) && $customerLastName != $data[Responses::KEY_LASTNAME]) {
                $customer->setLastname($data[Responses::KEY_LASTNAME]);
            }

            if (!empty($data[Responses::KEY_EMAIL]) && $customerEmail != $data[Responses::KEY_EMAIL]) {
                $customer->setEmail($data[Responses::KEY_EMAIL]);
            }

            if (!empty($data[Responses::KEY_VENDOR_NAME]) && $customerVendorNameValue != $data[Responses::KEY_VENDOR_NAME]) {
                $customer->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME, $data[Responses::KEY_VENDOR_NAME]);
            }

            if ($data[Responses::KEY_REMOVE_AVATAR] == 1 || !empty($_FILES)) {
                $customerAvatar = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR);
                $customerAvatarValue = empty($customerAvatar) ? '' : $customerAvatar->getValue();
                try {
                    $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);

                    $isFile = $this->file->isFile($mediaDirectory->getAbsolutePath());
                    $isExists = $this->file->isExists($mediaDirectory->getAbsolutePath());
                    if ($isFile . $customerAvatarValue && $isExists . $customerAvatarValue) {
                        $this->file->deleteFile($mediaDirectory->getAbsolutePath() . $customerAvatarValue);
                        $customer->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR, null);
                    } elseif ($data[Responses::KEY_REMOVE_AVATAR] == 1) {
                        $customer->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR, null);
                    }
                } catch (FileSystemException $e) {
                    return [Responses::ERROR => $e->getMessage()];
                }

                if (!empty($_FILES)) {
                    try {
                        $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'image']);
                        $uploaderFactory->setAllowCreateFolders(true);
                        $uploaderFactory->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                        $imageAdapter = $this->adapterFactory->create();
                        $uploaderFactory->addValidateCallback('image', $imageAdapter, 'validateUploadFile');
                        $uploaderFactory->setAllowRenameFiles(false);
                        $uploaderFactory->setFilesDispersion(false);

                        if (!$uploaderFactory->checkAllowedExtension($uploaderFactory->getFileExtension())) {
                            return Responses::ERROR_INCORRECT_IMAGE_TYPE;
                        }

                        if (!$uploaderFactory->save(
                            $mediaDirectory->getAbsolutePath('avatar'), $customer->getId() . '_' . time() . '.' . $uploaderFactory->getFileExtension()
                        )) {
                            return Responses::ERROR_IMAGE_CANNOT_BE_SAVE;
                        }
                        $imageName = empty($uploaderFactory->getUploadedFileName()) ? '' : $uploaderFactory->getUploadedFileName();

                        $customerAvatar = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR);
                        $customerAvatarValue = empty($customerAvatar) ? '' : $customerAvatar->getValue();

                        if ($customerAvatarValue != "avatar/$imageName") {
                            $customer->setCustomAttribute(
                                Constants::ATTRIBUTE_KARAVANIUM_AVATAR, "avatar/$imageName"
                            );
                        }

                    } catch (\Exception $e) {
                        return Responses::ERROR_IMAGE_CANNOT_BE_SAVE;
                    }
                }
            }
        }

        try {
            $this->customerRepositoryInterface->save($customer);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        return $this->getProfileClient();
    }

    private function updateProfileVendor()
    {
        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        if (!empty($this->data) && !empty($customer)) {
            $data = null;
            @$data = Tools::jsonDecode($this->data);
            if (!$data) {
                return Responses::ERROR_INCORRECT_FORMAT_OF_DATA;
            }

            $customerFirstName = $customer->getFirstname();
            $customerLastName = $customer->getLastname();
            $customerEmail = $customer->getEmail();
            $customerVendorName = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME);
            $customerVendorNameValue = empty($customerVendorName) ? '' : $customerVendorName->getValue();
            $customerVendorCountry = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_COUNTRY);
            $customerVendorCountryValue = empty($customerVendorCountry) ? '' : $customerVendorCountry->getValue();

            if (!empty($data[Responses::KEY_FIRSTNAME]) && $customerFirstName != $data[Responses::KEY_FIRSTNAME]) {
                $customer->setFirstname($data[Responses::KEY_FIRSTNAME]);
            }

            if (!empty($data[Responses::KEY_LASTNAME]) && $customerLastName != $data[Responses::KEY_LASTNAME]) {
                $customer->setLastname($data[Responses::KEY_LASTNAME]);
            }

            if (!empty($data[Responses::KEY_EMAIL]) && $customerEmail != $data[Responses::KEY_EMAIL]) {
                $customer->setEmail($data[Responses::KEY_EMAIL]);
            }

            if (!empty($data[Responses::KEY_VENDOR_NAME]) && $customerVendorNameValue != $data[Responses::KEY_VENDOR_NAME]) {
                $customer->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME, $data[Responses::KEY_VENDOR_NAME]);
            }

            if (!empty($data[Responses::KEY_VENDOR_COUNTRY]) && $customerVendorCountryValue != $data[Responses::KEY_VENDOR_COUNTRY]) {
                $customer->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_COUNTRY, $data[Responses::KEY_VENDOR_COUNTRY]);
            }

            if ($data[Responses::KEY_REMOVE_AVATAR] == 1 || !empty($_FILES)) {
                try {
                    $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
                    $customerKaravaniumAvatar = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR);
                    $customerKaravaniumAvatarValue = empty($customerKaravaniumAvatar) ? '' : $customerKaravaniumAvatar->getValue();
                    if ($this->file->isFile($mediaDirectory->getAbsolutePath() . $customerKaravaniumAvatarValue) && $this->file->isExists(
                            $mediaDirectory->getAbsolutePath() . $customerKaravaniumAvatarValue
                        )) {
                        $this->file->deleteFile($mediaDirectory->getAbsolutePath() . $customerKaravaniumAvatarValue);
                        $customer->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR, null);
                    } elseif ($data[Responses::KEY_REMOVE_AVATAR] == 1) {
                        $customer->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR, null);
                    }
                } catch (FileSystemException $e) {
                    return [Responses::ERROR => $e->getMessage()];
                }

                if (!empty($_FILES)) {
                    try {
                        $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'image']);
                        $uploaderFactory->setAllowCreateFolders(true);
                        $uploaderFactory->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                        $uploaderFactory->setAllowRenameFiles(false);
                        $uploaderFactory->setFilesDispersion(false);
                        $imageAdapter = $this->adapterFactory->create();
                        $uploaderFactory->addValidateCallback('image', $imageAdapter, 'validateUploadFile');

                        if (!$uploaderFactory->checkAllowedExtension($uploaderFactory->getFileExtension())) {
                            return Responses::ERROR_INCORRECT_IMAGE_TYPE;
                        }

                        if (!$uploaderFactory->save(
                            $mediaDirectory->getAbsolutePath('avatar'),
                            $customer->getId() . '_' . time() . '.' . $uploaderFactory->getFileExtension()
                        )
                        ) {
                            return Responses::ERROR_IMAGE_CANNOT_BE_SAVE;
                        }
                        $imageName = empty($uploaderFactory->getUploadedFileName())
                            ? ''
                            : $uploaderFactory->getUploadedFileName();

                        $customerKaravaniumAvatar       = $customer->getCustomAttribute(
                            Constants::ATTRIBUTE_KARAVANIUM_AVATAR
                        );
                        $customerKaravaniumAvatarValue  = empty($customerKaravaniumAvatar)
                            ? ''
                            : $customerKaravaniumAvatar->getValue();

                        if ($customerKaravaniumAvatarValue !== "avatar/$imageName") {
                            $customer->setCustomAttribute(
                                Constants::ATTRIBUTE_KARAVANIUM_AVATAR, "avatar/$imageName"
                            );
                        }

                    } catch (\Exception $e) {
                        return Responses::ERROR_IMAGE_CANNOT_BE_SAVE;
                    }
                }
            }
        }

        try {
            if (!empty($customer)) {
                $this->customerRepositoryInterface->save($customer);
            }
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        return $this->getProfileVendor();
    }

    private function getProfileRoles()
    {
        $userId = $this->user_id;

        try {
            $customer = $this->customerRepositoryInterface->getById($userId);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_USER_ID;
        }

        $customerKaravaniumRole = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_ROLE);
        $customerKaravaniumRoleValue = empty($customerKaravaniumRole) ? [] : explode(
            ',', $customerKaravaniumRole->getValue()
        );

        $customerKaravaniumRoleValues = [];
        for ($i = 0, $count = count($customerKaravaniumRoleValue); $i < $count; $i++) {
            $customerKaravaniumRoleValues[] = [Responses::KEY_CODE => $customerKaravaniumRoleValue[$i]];
        }

        return [Responses::KEY_USER_ROLE => $customerKaravaniumRoleValues];
    }

    private function updateProfileRoles()
    {
        $userId = $this->user_id;
        try {
            $customer = $this->customerRepositoryInterface->getById($userId);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_USER_ID;
        }

        $customerKaravaniumRole = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_ROLE);
        $customerKaravaniumRoleValue = empty($customerKaravaniumRole) ? [] : explode(
            ',', $customerKaravaniumRole->getValue()
        );

        if (!empty($this->user_role) && $customerKaravaniumRoleValue != explode(',', $this->user_role)) {
            $customer->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_ROLE, $this->user_role);
        }

        try {
            $this->customerRepositoryInterface->save($customer);
        } catch (\Exception $e) {
            return [Responses::ERROR . ': ' . $e->getMessage()];
        }

        return $this->getProfileRoles();
    }

    // Subscription Section

    private function addFavoriteVendor()
    {
        return $this->subscribeToVendor();
    }

    private function removeFavoriteVendor()
    {
        return $this->unsubscribeFromVendor();
    }

    private function subscribeToVendorByProduct()
    {
        if (!$this->product_id) {
            return Responses::ERROR_NO_PRODUCT_DATA;
        }

        try {
            $product = $this->getProductById($this->product_id);
            $this->vendor_id = $product->getKaravaniumUserId();
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_PRODUCT;
        }

        return $this->subscribeToVendor();
    }

    /**
     * @param $productId
     *
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws NoSuchEntityException
     */
    private function getProductById($productId)
    {
        return $this->productRepositoryInterface->getById($productId);
    }

    private function unsubscribeFromVendorByProduct()
    {
        if (!$this->product_id) {
            return Responses::ERROR_NO_PRODUCT_DATA;
        }

        try {
            $product = $this->getProductById($this->product_id);
            $this->vendor_id = $product->getKaravaniumUserId();
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_PRODUCT;
        }

        return $this->unsubscribeFromVendor();
    }

    private function getSubscribedTags()
    {
        $subscribedTags = $this->getTags();
        return [
            'tags'          => $subscribedTags,
            'tags_count'    => count($subscribedTags)
        ];
    }

    private function subscribeToTag()
    {
        $tagId  = $this->tag_id;
        $userId = $this->user_id;
        $userToTagsFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\UserToTagFactory')->create();
        $userToTagsCollection = $userToTagsFactory->getCollection()
            ->addFieldToFilter(Constants::COLUMN_USER_ID, ['eq' => $userId])
            ->addFieldToFilter(Constants::COLUMN_TAG_ID, ['eq' => $tagId])
            ->getData();

        if ($userToTagsCollection) {
            return [
                Responses::SUCCESS => 1,
                'tags_count' => count($this->getTags())
            ];
        }

        $userToTagsFactory->setData(
            [
                Constants::COLUMN_USER_ID  => $userId,
                Constants::COLUMN_TAG_ID   => $tagId
            ]
        );
        $userToTagsFactory->save();

        return [
            Responses::SUCCESS => 1,
            'tags_count' => count($this->getTags())
        ];
    }

    private function unsubscribeFromTag()
    {
        $tagId  = $this->tag_id;
        $userId = $this->user_id;
        $userToTagsFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\UserToTagFactory');
        $userToTags = $userToTagsFactory->create();
        $userToTagsCollection = $userToTags->getCollection()
            ->addFieldToFilter(Constants::COLUMN_USER_ID, ['eq' => $userId]);

        $userToTagsItems = $userToTagsCollection->getItems();
        foreach ($userToTagsItems as $tagAssociationItem) {
            if ($tagAssociationItem->getTagId() == $tagId) {
                $tagAssociationItem->delete($tagAssociationItem->getId());
                break;
            }
        }

        return [
            Responses::SUCCESS => 1,
            'tags_count' => count($this->getTags())
        ];
    }

    private function subscribeToVendor()
    {
        if (!$this->vendor_id) {
            return Responses::ERROR_NO_DATA;
        }
        $vendorId = $this->vendor_id;

        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        if (!empty($customer)) {
            $favoriteVendors = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);

            $favoriteVendorsValue = empty($favoriteVendors) ? [] : explode(',', $favoriteVendors->getValue());
            @$addFavoriteVendors = in_array(
                $vendorId, $favoriteVendorsValue
            ) ? $favoriteVendorsValue : array_push(
                $favoriteVendorsValue, $vendorId
            );
            $customer->setCustomAttribute(
                Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS, implode(',', $favoriteVendorsValue)
            );

            try {
                $this->customerRepositoryInterface->save($customer);
                $this->compareWithFirestoreSubscribers($vendorId, $this->getUserIdsForVendor($vendorId));
            } catch (\Exception $e) {
                return [Responses::ERROR => $e->getMessage()];
            }
        }
        return $this->getSubscriptionResponse();
    }

    private function getUserIdsForVendor($vendorId)
    {
        $vendorFilterOne = $this->filterBuilder
            ->setField(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS)
            ->setConditionType('like')
            ->setValue("%$vendorId,%")
            ->create();
        $vendorFilterTwo = $this->filterBuilder
            ->setField(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS)
            ->setConditionType('like')
            ->setValue((string)$vendorId)
            ->create();

        $this->searchCriteriaBuilder->addFilters([$vendorFilterOne, $vendorFilterTwo]);
        $customerList = $this->customerRepositoryInterface->getList($this->searchCriteriaBuilder->create());

        $subscriberList = [];
        foreach ($customerList->getItems() as $customer) {
            $subscriberList[] = $customer->getId();
        }

        return $subscriberList;
    }

    private function compareWithFirestoreSubscribers($vendorId, $subscriberList, $unsubscribtion = false)
    {
    }

    private function unsubscribeFromVendor()
    {
        if (!$this->vendor_id) {
            return Responses::ERROR_NO_DATA;
        }

        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $favoriteVendors = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
        $favoriteVendorsValue = explode(',', trim(empty($favoriteVendors) ? '' : $favoriteVendors->getValue()));
        $removeFavoriteVendorsValue = array_diff($favoriteVendorsValue, [$this->vendor_id]);
        $customer->setCustomAttribute(
            Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS, implode(',', $removeFavoriteVendorsValue)
        );

        try {
            $this->customerRepositoryInterface->save($customer);
            $this->compareWithFirestoreSubscribers($this->vendor_id, $this->getUserIdsForVendor($this->vendor_id), true);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        return $this->getSubscriptionResponse();
    }

    private function getSubscriptionProductCount()
    {
        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (NoSuchEntityException $e) {
            return [Responses::ERROR => $e->getMessage()];
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $subscribedAttribute = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
        $subscriptionIds = (null !== $subscribedAttribute ? $subscribedAttribute->getValue() : 0);

        $collection = $this->productCollection->create()
            ->setVisibility($this->productVisibility->getVisibleInCatalogIds())
            ->addFieldToSelect('entity_id')
            ->addFieldToFilter(Constants::ATTRIBUTE_KARAVANIUM_USER_ID, ['in' => $subscriptionIds])
            ->addFieldToFilter(Constants::COLUMN_STATUS, ['eq' => ProductStatus::STATUS_ENABLED]);

        return (int)$collection->count();
//        $filterOne = $this->filterBuilder
//            ->setField(Constants::ATTRIBUTE_KARAVANIUM_USER_ID)
//            ->setConditionType('in')
//            ->setValue($subscriptionIds)
//            ->create();
//        $filterTwo = $this->filterBuilder
//            ->setField(Constants::COLUMN_STATUS)
//            ->setConditionType('eq')
//            ->setValue(ProductStatus::STATUS_ENABLED)
//            ->create();
//        $filterGroupOne = $this->filterGroupBuilder->addFilter($filterOne)->create();
//        $filterGroupTwo = $this->filterGroupBuilder->addFilter($filterTwo)->create();

//        $this->searchCriteriaBuilder->setFilterGroups([$filterGroupOne, $filterGroupTwo]);
//        $searchCriteria = $this->searchCriteriaBuilder->create();
//        $product = $this->productRepositoryInterface->getList($searchCriteria);
//
//        return $product->getTotalCount();
    }

    private function getSubscriptionResponse()
    {
        return [
            Responses::SUCCESS          => 1,
            'products_subscribed_count' => $this->getSubscriptionProductCount()
        ];
    }

    private function getVendorAdditionalInformation()
    {
        try {
            $vendor = $this->customerRepositoryInterface->getById($this->vendor_id);

            $vendorVendorName = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME);
            $vendorAvatarUrl = !empty($vendorAvatar = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR))
                ? $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $vendorAvatar->getValue()
                : '';
            $vendorRating = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_RATING);
            $vendorVendorNameValue = empty($vendorVendorName) ? '' : $vendorVendorName->getValue();
            $vendorRatingValue = empty($vendorRating) ? '' : $vendorRating->getValue();
            $vendorSubscribed = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
            $vendorSubscribedValue = !empty($vendorSubscribed) ? $vendorSubscribed->getValue() : '';
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
        return [
            Responses::KEY_VENDOR_NAME => $vendorVendorNameValue,
            Responses::KEY_VENDOR_RATING => $vendorRatingValue,
            Responses::KEY_VENDOR_AVATAR => $vendorAvatarUrl,
            Responses::KEY_VENDOR_SUBSCRIBED =>
                !empty(strstr($vendorSubscribedValue, $this->vendor_id))
                    ? 1
                    : 0
        ];
    }

    private function getUsersChatData()
    {
        if (empty($this->ids)) {
            return Responses::ERROR_NO_DATA;
        }

        $filterOne = $this->filterBuilder
            ->setField(Constants::COLUMN_ENTITY_ID)
            ->setConditionType('in')
            ->setValue($this->ids)
            ->create();
        $filterGroupOne = $this->filterGroupBuilder->addFilter($filterOne)->create();
        $this->searchCriteriaBuilder->setFilterGroups([$filterGroupOne]);
        $searchCriteria = $this->searchCriteriaBuilder->create();

        $vendors = [];
        try {
            $vendorList = $this->customerRepositoryInterface->getList($searchCriteria);
            foreach ($vendorList->getItems() as $vendor) {
                $vendorAvatarUrl = !empty($vendorAvatar = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR))
                    ? $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $vendorAvatar->getValue()
                    : '';

                $fullName = !empty($vendorName = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME))
                    ? $vendorName->getValue()
                    : $vendor->getFirstname() . ' ' . $vendor->getLastname();

                $isVendor = !empty($userType = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_ROLE))
                    ? (in_array('vendor', explode(',', $userType->getValue())) ?  1 : 0)
                    : 0;

                $vendors[] = [
                    Responses::KEY_USER_ID      => $vendor->getId(),
                    Responses::KEY_FULLNAME     => $fullName,
                    Responses::KEY_USER_AVATAR  => $vendorAvatarUrl,
                    'is_vendor'                 => $isVendor
                ];
            }

        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        return ['data' => $vendors];
    }

    // Addresses Get/Add/Update/Remove
    private function getProfileAddresses()
    {
        try {
            $customer = $this->customerRepositoryInterface->getById((int) $this->user_id);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_USER_ID;
        }

        try {
            $addresses = $customer->getAddresses();
            $country = $this->countryFactory->create();
            $region = $this->regionFactory->create();
        } catch (\Exception $e) {
            return Responses::ERROR_ADDRESS_INFO_ISSUE;
        }

        $addressesPrepared = [];
        foreach ($addresses as $address) {
            $addressesPrepared[] = $this->getAddressData($address, $country, $region);
        }

        return [
            Responses::KEY_ADDRESSES_COUNT => count($addresses),
            Responses::KEY_ADDRESSES => $addressesPrepared
        ];
    }

    private function getAddressData($address, $country, $region)
    {
        /** @var \Magento\Customer\Model\Data\Address $address */
        /** @var \Magento\Directory\Model\Country $country */
        /** @var \Magento\Directory\Model\Region $region */
        $streets = $address->getStreet();
        $streetsPrepared = [];
        for ($i = 0, $count = count($streets); $i < $count; $i++) {
            $streetsPrepared[] = [Responses::KEY_NAME => $streets[$i]];
        }

        return [
            Responses::KEY_ADDRESS_ID => $address->getId(),
            Responses::KEY_REGION_CODE => empty($address->getRegionId()) ? '' : $address->getRegion()->getRegionCode(),
            Responses::KEY_REGION => $address->getRegionId() > 0
                ? $region->loadByCode($address->getRegion()->getRegionCode(), $address->getCountryId())
                    ->getDefaultName()
                : $address->getRegion()->getRegion(),
            Responses::KEY_COUNTRY_ID => $address->getCountryId(),
            Responses::KEY_COUNTRY => $country->loadByCode($address->getCountryId())->getName(),
            Responses::KEY_STREETS => $streetsPrepared,
            Responses::KEY_COMPANY => $address->getCompany(),
            Responses::KEY_TELEPHONE => $address->getTelephone(),
            Responses::KEY_FAX => $address->getFax(),
            Responses::KEY_POSTCODE => $address->getPostcode(),
            Responses::KEY_CITY => $address->getCity(),
            Responses::KEY_FIRSTNAME => $address->getFirstname(),
            Responses::KEY_LASTNAME => $address->getLastname(),
            Responses::KEY_MIDDLENAME => $address->getMiddlename(),
            Responses::KEY_PREFIX => $address->getPrefix(),
            Responses::KEY_SUFFIX => $address->getSuffix(),
            Responses::KEY_VAT_ID => $address->getVatId(),
            Responses::KEY_CUSTOMER_ID => $address->getCustomerId(),
            Responses::KEY_IS_DEFAULT_SHIPPING => (int)$address->isDefaultShipping(),
            Responses::KEY_IS_DEFAULT_BILLING => (int)$address->isDefaultBilling(),
        ];
    }

    private function getProfileNewAddressData()
    {
        return $this->getAddressExtraData();
    }

    private function getAddressExtraData()
    {
        try {
            $collection = $this->countryCollectionFactory->create()->loadByStore();
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
        $countriesWithRequiredStates = $collection->getCountriesWithRequiredStates();

        $countiesAll = $collection->toOptionArray();
        $countries = [];
        for ($i = 1, $countCountries = count($countiesAll); $i < $countCountries; $i++) {
            $regions = [];
            if (array_key_exists($countiesAll[$i][Responses::KEY_VALUE], $countriesWithRequiredStates)) {
                $regionList = $countriesWithRequiredStates[$countiesAll[$i][Responses::KEY_VALUE]]->getRegionCollection();
                foreach ($regionList as $region) {
                    $regions[] = [
                        Responses::KEY_NAME => $region->getName(),
                        Responses::KEY_CODE => $region->getCode()
                    ];
                }
            }

            $countries[] = [
                Responses::KEY_NAME => $countiesAll[$i]['label'],
                Responses::KEY_COUNTRY_ID => $countiesAll[$i]['value'],
                Responses::KEY_REGIONS => $regions
            ];
        }

        return [Responses::KEY_COUNTRIES => $countries];
    }

    private function getProfileAddress($addressId = null)
    {
        if ($this->address_id < 1 && $addressId < 1) {
            $this->address_id = $addressId;
        }

        if ($this->address_id < 1 && ($addressId == null || $addressId < 1)) {
            return Responses::ERROR_NO_SUCH_ADDRESS_ID;
        }

        $userAddress = [];
        try {
            $customer = $this->customerRepositoryInterface->getById((int) $this->user_id);
            $addresses = $customer->getAddresses();
            foreach ($addresses as $address) {
                if ($this->address_id == $address->getId()) {
                    $userAddress = $address;
                    break;
                }
            }
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_USER_ID;
        }

        try {
            $country = $this->countryFactory->create();
            $region = $this->regionFactory->create();
        } catch (\Exception $e) {
            return Responses::ERROR_ADDRESS_INFO_ISSUE;
        }

        $result = $this->getAddressExtraData();
        $result[Responses::KEY_ADDRESS] = $this->getAddressData($userAddress, $country, $region);

        return $result;
    }

    private function addAddressInfo()
    {
        try {
            $this->saveProfileAddress();
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        } finally {
            return [Responses::KEY_SUCCESS => 1];
        }
    }

    private function saveProfileAddress()
    {
        if (empty($this->data)) {
            return Responses::ERROR_NO_DATA;
        }

        try {
            $data = Tools::jsonDecode($this->data);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        if (!$data) {
            return Responses::ERROR_INCORRECT_FORMAT_OF_DATA;
        }

        $addressId = (int) $data[Responses::KEY_ADDRESS_ID];
        try {
            $address = $addressId > 0 ? $this->addressRepositoryInterface->getById($addressId) : $this->addressInterfaceFactory->create();
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $streets = [];
        for ($i = 0, $count = count($data[Responses::KEY_STREETS]); $i < $count; $i++) {
            $streets[] = $data[Responses::KEY_STREETS][$i][Responses::KEY_NAME];
        }

        $address->setCustomerId($this->user_id)
            ->setFirstname($data[Responses::KEY_FIRSTNAME])
            ->setLastname($data[Responses::KEY_LASTNAME])
            ->setCountryId($data[Responses::KEY_COUNTRY_ID])
            ->setPostcode($data[Responses::KEY_POSTCODE])
            ->setCity($data[Responses::KEY_CITY])
            ->setTelephone($data[Responses::KEY_TELEPHONE])
            ->setStreet($streets);

        $region = $this->regionFactory->create();
        try {
            $collection = $this->countryCollectionFactory->create()->loadByStore();
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $countriesWithRequiredStates = $collection->getCountriesWithRequiredStates();
        if (array_key_exists($data[Responses::KEY_COUNTRY_ID], $countriesWithRequiredStates)) {
            $loadedRegion = $region->loadByCode($data[Responses::KEY_REGION_CODE], $data[Responses::KEY_COUNTRY_ID]);
            $address->setRegionId($loadedRegion->getId());
            $address->setRegion(
                $this->regionInterface->setRegionId($loadedRegion->getId())->setRegionCode($loadedRegion->getCode())->setRegion(
                    $loadedRegion->getDefaultName()
                )
            );
        } else {
            $address->setRegionId('');
            $address->setRegion($this->regionInterface->setRegion(empty($data[Responses::KEY_REGION]) ? '' : $data[Responses::KEY_REGION]));
        }

        try {
            $savedAddress = $this->addressRepositoryInterface->save($address);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        try {
            $address = $this->addressRepositoryInterface->getById($savedAddress->getId());
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $isDefaultShipping = array_key_exists(Responses::KEY_IS_DEFAULT_SHIPPING, $data) ? $data[Responses::KEY_IS_DEFAULT_SHIPPING] : 0;
        $isDefaultBilling = array_key_exists(Responses::KEY_IS_DEFAULT_BILLING, $data) ? $data[Responses::KEY_IS_DEFAULT_BILLING] : 0;

        $this->address_id = $address->getId();
        if ($addressId == 0 && $isDefaultShipping == 0 && $isDefaultBilling == 0) {
            $this->autoCheckFirstAddress();
            return $this->getProfileAddress($address->getId());
        }

        if ($addressId >= 0 && ($isDefaultShipping >= 0 || $isDefaultBilling >= 0)) {
            if ($isDefaultShipping != 0) {
                $this->setProfileDefaultShippingAddress();
            } else {
                $this->setProfileDefaultShippingAddress(false);
            }
            if ($isDefaultBilling != 0) {
                $this->setProfileDefaultBillingAddress();
            } else {
                $this->setProfileDefaultBillingAddress(false);
            }
        }
        $this->autoCheckFirstAddress();

        return $this->getProfileAddress($address->getId());
    }

    private function setProfileDefaultBillingAddress($param = true)
    {
        if (!$this->address_id) {
            return Responses::ERROR_NO_ADDRESS_ID_SPECIFIED;
        }

        try {
            $currentAddress = $this->addressRepositoryInterface->getById((int)$this->address_id);
            $customer       = $this->customerRepositoryInterface->getById((int) $currentAddress->getCustomerId());
            $addresses      = $customer->getAddresses();

            $addressId = null;
            $addressList = [];
            foreach ($addresses as $address) {
                if ($param == true && $currentAddress->getId() == $address->getId()) {
                    $addressList[] = $address->setIsDefaultBilling(true);
                    $addressId = $currentAddress->getId();
                    continue;
                }

                if ($param == false && $currentAddress->getId() != $address->getId() && $address->isDefaultBilling() == true) {
                    $addressList[] = $address->setIsDefaultBilling(true);
                    $addressId = $address->getId();
                    continue;
                }

                $addressList[] = $address->setIsDefaultBilling(false);
                continue;
            }

            $customer->setAddresses($addressList)->setDefaultBilling($addressId);

            $this->customerRepositoryInterface->save($customer);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        return $this->getProfileAddresses();
    }

    private function setProfileDefaultShippingAddress($param = true)
    {
        if (!$this->address_id) {
            return Responses::ERROR_NO_ADDRESS_ID_SPECIFIED;
        }

        try {
            $currentAddress = $this->addressRepositoryInterface->getById($this->address_id);
            $customer = $this->customerRepositoryInterface->getById((int) $currentAddress->getCustomerId());
            $addresses = $customer->getAddresses();
            $addressId = null;
            $addressList = [];
            foreach ($addresses as $address) {
                if ($param === true && $currentAddress->getId() === $address->getId()) {
                    $addressList[] = $address->setIsDefaultShipping(true);
                    $addressId = $currentAddress->getId();
                    continue;
                }

                if ($param == false && $currentAddress->getId() != $address->getId() && $address->isDefaultShipping() == true) {
                    $addressList[] = $address->setIsDefaultShipping(true);
                    $addressId = $address->getId();
                    continue;
                }

                $addressList[] = $address->setIsDefaultShipping(false);
                continue;
            }

            $customer->setAddresses($addressList)->setDefaultShipping($addressId);

            $this->customerRepositoryInterface->save($customer);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        return $this->getProfileAddresses();
    }

    public function autoCheckFirstAddress()
    {
        $statusAddr = true;
        $checkAddresess = $this->getProfileAddresses();
        $notdefaultaddreses = [];
        for ($i = 0, $countCheckedAddresess = count($checkAddresess[Responses::KEY_ADDRESSES]); $i < $countCheckedAddresess; $i++) {
            if ($checkAddresess[Responses::KEY_ADDRESSES][$i][Responses::KEY_IS_DEFAULT_SHIPPING] != 1) {
                $notdefaultaddreses[] = $checkAddresess[Responses::KEY_ADDRESSES][$i];
            }

            if (count($notdefaultaddreses) === $countCheckedAddresess) {
                $statusAddr = false;
                break;
            }
        }

        if ($statusAddr === false) {
            try {
                $customer = $this->customerRepositoryInterface->getById($this->user_id);
                $addressId = null;
                $addressList = [];
                $addresses = $customer->getAddresses();
                foreach ($addresses as $address) {
                    if (count($addressList) > 0) {
                        $addressList[] = $address->setIsDefaultShipping(false);
                        continue;
                    }

                    $addressList[] = $address->setIsDefaultShipping(true);
                    $addressId = $address->getId();
                }
                $customer->setAddresses($addressList)->setDefaultShipping($addressId);
                $this->customerRepositoryInterface->save($customer);
            } catch (\Exception $e) {

            }
        }
    }

    private function remove_profile_addresses()
    {
        if (empty($this->address_ids)) {
            return Responses::ERROR_EMPTY_ADDRESS_IDS;
        }

        $ids = explode(',', $this->address_ids);
        for ($i = 0, $count = count($ids); $i < $count; $i++) {
            try {
                $this->addressRepositoryInterface->deleteById($ids[$i]);
            } catch (NoSuchEntityException $e) {
            } catch (LocalizedException $e) {
                return [Responses::ERROR => $e->getMessage()];
            }
        }

        return $this->getProfileAddresses();
    }

    // Products

    private function getProducts()
    {
        $productsCollection = $this->productCollection->create()
            ->setVisibility($this->productVisibility->getVisibleInCatalogIds())
            ->addFieldToSelect('entity_id')
            ->addFieldToSelect(Constants::ATTRIBUTE_KARAVANIUM_USER_ID)
            ->addFieldToSelect(Constants::ATTRIBUTE_KARAVANIUM_SKU)
            ->addFieldToFilter(Constants::ATTRIBUTE_KARAVANIUM_USER_ID, ['in' => $subscriptionIds])
            ->addFieldToFilter(Constants::COLUMN_STATUS, ['eq' => ProductStatus::STATUS_ENABLED])
            ->joinField('qty',
            'cataloginventory_stock_item',
            'qty',
            'product_id=entity_id',
            '{{table}}.stock_id=1',
            'left'
            )
            ->joinTable('cataloginventory_stock_item', 'product_id=entity_id', array('stock_status' => 'is_in_stock'))
//        $productsModel = Tools::getProductModel($this->context->getObjectManager());
//        $productsCollection = $productsModel->getCollection();
//        $productsCollection->addFieldToFilter('status', ['eq' => ProductStatus::STATUS_ENABLED]);
//
//        /** @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $productsCollection */
//        $productsCollection->getSelect()->joinLeft(
//            ['cpkui' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
//            'cpkui.entity_id = e.entity_id AND cpkui.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_user_id' LIMIT 1) AND cpkui.store_id = 0", []
//        )->joinLeft(
//            ['cs' => Tools::getDbTablePrefix() . 'customer_entity'], 'cs.entity_id = cpkui.value', []
//        )->joinLeft(
//            ['cekn' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
//            'cekn.entity_id = cs.entity_id AND cekn.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_vendor_name' LIMIT 1)", ['cekn.value AS karavanium_vendor_name']
//        )->joinLeft(
//            ['cekr' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
//            'cekr.entity_id = cs.entity_id AND cekr.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_rating' LIMIT 1)", ['cekr.value AS karavanium_rating']
//        )->joinLeft(
//            ['ceka' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
//            'ceka.entity_id = cs.entity_id AND ceka.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_avatar' LIMIT 1)", ['ceka.value AS karavanium_avatar']
//        )->limit(
//            $this->page_size, ($this->page - 1) * $this->page_size
//        );

//        $sortBy = $this->sort_by;
//        if (empty($sortBy)) {
//            $sortBy = 'id';
//        }
//
//        switch ($sortBy) {
//            case 'name':
//                $orderBy = !empty($this->order_by) ? $this->order_by : 'ASC';
//                $productsCollection->getSelect()->order(["name $orderBy"]);
//                break;
//            case 'id':
//                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
//                $productsCollection->getSelect()->order(["e.entity_id $orderBy"]);
//                break;
//            case 'qty':
//                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
//                $productsCollection->getSelect()->order(["si.qty $orderBy"]);
//                break;
//            case 'price':
//                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
//                $productsCollection->getSelect()->order(["price $orderBy"]);
//                break;
//            case 'status':
//                $orderBy = !empty($this->order_by) ? $this->order_by : 'ASC';
//                $productsCollection->getSelect()->order(["status $orderBy"]);
//                break;
//        }

        $products = [];
        foreach ($productsCollection as $product) {
            /** @var \Magento\Catalog\Model\Product $product*/
            try {
                $product->load($product->getEntityId());
            } catch (\Exception $e) {
                [Responses::ERROR => $e->getMessage()];
            }

            $stockState = $this->context->getObjectManager()->create('Magento\CatalogInventory\Api\StockRegistryInterface');
            $stock = $stockState->getStockItem(
                $product->getEntityId(), $product->getStore()->getWebsiteId()
            );

            try {
                $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
                $customer = $this->customerRepositoryInterface->getById($this->user_id);
                $vendorSubscribed = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
                $baseImageId = $this->getBaseImageId($product->getMediaGalleryEntries());
                $mediaGallery = $this->getMediaGalleryByProduct($product->getEntityId(), $product->getMediaGalleryImages()->getItems(), $baseImageId, $mediaDirectory);
            } catch (\Exception $e) {
                return [Responses::ERROR => $e->getMessage()];
            }

            $vendorSubscribedValue = !empty($vendorSubscribed) ? $vendorSubscribed->getValue() : '';
            $baseUrl = $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);

            $products[] = [
                Responses::KEY_PRODUCT_ID => $product->getEntityId(),
                Responses::KEY_VENDOR_ID            => empty($userId = $product->getKaravaniumUserId()) ? 0 : $userId,
                Responses::KEY_VENDOR_NAME => $product->getData(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME),
                Responses::KEY_VENDOR_RATING => !empty($product->getData(Constants::ATTRIBUTE_KARAVANIUM_RATING))
                    ? $product->getData(Constants::ATTRIBUTE_KARAVANIUM_RATING)
                    : 0,
                Responses::KEY_VENDOR_AVATAR => !empty($product->getData(Constants::ATTRIBUTE_KARAVANIUM_AVATAR))
                    ? $baseUrl . $product->getData(Constants::ATTRIBUTE_KARAVANIUM_AVATAR)
                    : '',
                Responses::KEY_VENDOR_SUBSCRIBED => !empty(strstr($vendorSubscribedValue, $product->getKaravaniumUserId())) ? 1 : 0,
                Responses::KEY_SKU => !empty($customSku = $product->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_SKU))
                    ? $customSku->getValue()
                    : '',
                Responses::KEY_NAME => $product->getName(),
                Responses::KEY_DESCRIPTION => $product->getDescription(),
                Responses::KEY_IMAGES => $mediaGallery,
                Responses::KEY_STATUS => ($product->getStatus() === ProductStatus::STATUS_ENABLED) ? 1 : 0,
                Responses::KEY_QTY => (int) $stock->getQty(),
                Responses::KEY_PRICE => $price = empty($product->getPrice()) ? 0 : $product->getPrice(),
                Responses::KEY_FORMATTED_PRICE      => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                Responses::KEY_AVAILABLE_FOR_ORDER  => $this->isProductAvailable($product) ? 1 : 0
            ];
        }

        return [
            Responses::KEY_PRODUCTS_COUNT => $productsCollection->getSize(),
            Responses::KEY_PRODUCTS => $products
        ];
    }

    private function getProductsForVendor()
    {
        $products = [];

        $vendorPageSize = $this->page_size;
        $vendorProductCount = $this->vendorProductCount();
        if ((int) ceil($vendorProductCount/$vendorPageSize) < $this->page) {
            return $this->productResponse($vendorProductCount, $products);
        }

        $productsModel = Tools::getProductModel($this->context->getObjectManager());
        $productsCollection = $productsModel->getCollection();

        /** @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $productsCollection */
        $productsCollection->getSelect()
            ->joinInner(
                ['cpkui' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
                'cpkui.entity_id = e.entity_id AND cpkui.attribute_id =  (SELECT attribute_id FROM ' .
                Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_user_id' LIMIT 1) 
                AND cpkui.store_id = 0
                AND cpkui.value =" . $this->user_id, []
            )->joinLeft(
                ['cust_sku' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
                'cust_sku.entity_id = e.entity_id AND cust_sku.attribute_id = (SELECT attribute_id FROM ' .
                Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_sku' LIMIT 1) 
                AND cust_sku.store_id = 0",
                ['cust_sku.value AS custom_sku']
            )->limit(
                $this->page_size, ($this->page - 1) * $vendorPageSize
            );

        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        } catch (FileSystemException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
        $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
        $productsCollection->getSelect()->order(["e.entity_id $orderBy"]);

        var_dump($productsCollection->getAllIds());
        $products = [];
        foreach ($productsCollection as $product) {
            /** @var \Magento\Catalog\Model\Product $product */
            try {
                $product->load($product->getEntityId());
            } catch (\Exception $e) {
                [Responses::ERROR => $e->getMessage()];
            }

            $stockState = $this->context->getObjectManager()->create('Magento\CatalogInventory\Api\StockRegistryInterface');
            $stock = $stockState->getStockItem($product->getEntityId(), $product->getStore()->getWebsiteId());

            $mediaGallery = [];
            try {
                $productObject = $this->context->getObjectManager()->create('Magento\Catalog\Model\Product');
                $productObject->load($product->getEntityId());
                $baseImageId = $this->getBaseImageId($productObject->getMediaGalleryEntries());
                $mediaGallery = $this->getMediaGalleryByProduct($product->getEntityId(), $productObject->getMediaGalleryImages()->getItems(), $baseImageId, $mediaDirectory);
                $productObject->getExtensionAttributes()->getStockItem();
            } catch (\Exception $e) {}

            $products[] = [
                Responses::KEY_PRODUCT_ID           => $product->getEntityId(),
                Responses::KEY_SKU                  => $product['custom_sku'],
                Responses::KEY_NAME                 => $product->getName(),
                Responses::KEY_IMAGES               => $mediaGallery,
                Responses::KEY_STATUS               => ($product->getStatus() === ProductStatus::STATUS_ENABLED) ? 1 : 0,
                Responses::KEY_QTY                  => (int) $stock->getQty(),
                Responses::KEY_PRICE                => $price = empty($product->getPrice()) ? 0 : $product->getPrice(),
                Responses::KEY_FORMATTED_PRICE      => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                Responses::KEY_AVAILABLE_FOR_ORDER  => $this->isProductAvailable($product) ? 1 : 0
            ];
        }

        return $this->productResponse($vendorProductCount, $products);
    }

    private function getProductsSubscribed()
    {
        $productsModel = Tools::getProductModel($this->context->getObjectManager());
        $productsCollection = $productsModel->getCollection();
        $productsCollection->addFieldToFilter('status', ['eq' => ProductStatus::STATUS_ENABLED]);

        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
        $vendorSubscribed = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
        $vendorSubscription = !empty($vendorSubscribed) ? $vendorSubscribed->getValue() : '0';

        /** @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $productsCollection */
        $productsCollection->getSelect()->joinLeft(
            ['cpkui' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'cpkui.entity_id = e.entity_id AND cpkui.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_user_id' LIMIT 1) AND cpkui.store_id = 0", []
        )->joinLeft(
            ['cs' => Tools::getDbTablePrefix() . 'customer_entity'], 'cs.entity_id = cpkui.value', []
        )->joinLeft(
            ['cekn' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekn.entity_id = cs.entity_id AND cekn.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_vendor_name' LIMIT 1)", ['cekn.value AS karavanium_vendor_name']
        )->joinLeft(
            ['cekr' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekr.entity_id = cs.entity_id AND cekr.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_rating' LIMIT 1)", ['cekr.value AS karavanium_rating']
        )->joinLeft(
            ['ceka' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'ceka.entity_id = cs.entity_id AND ceka.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_avatar' LIMIT 1)", ['ceka.value AS karavanium_avatar']
        )->where("cpkui.value IN ($vendorSubscription)");

        $productsCountObject = clone $productsCollection;
        $productsCountObject->getSelect()->reset(\Magento\Framework\DB\Select::GROUP);
        $productCount = $productsCountObject->getConnection()->fetchOne($productsCountObject->getSelectCountSql());
        $productsCollection->getSelect()->limit($this->page_size, ($this->page - 1) * $this->page_size);
        $sortBy = $this->sort_by;
        if (empty($sortBy)) {
            $sortBy = 'id';
        }

        switch ($sortBy) {
            case 'name':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'ASC';
                $productsCollection->getSelect()->order(["name $orderBy"]);
                break;
            case 'id':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
                $productsCollection->getSelect()->order(["e.entity_id $orderBy"]);
                break;
            case 'qty':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
                $productsCollection->getSelect()->order(["si.qty $orderBy"]);
                break;
            case 'price':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
                $productsCollection->getSelect()->order(["price $orderBy"]);
                break;
            case 'status':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'ASC';
                $productsCollection->getSelect()->order(["status $orderBy"]);
                break;
        }

        try {
            $baseUrl = $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        } catch (FileSystemException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $products = [];
        foreach ($productsCollection as $product) {
            /** @var \Magento\Catalog\Model\Product $product*/
            try {
                $product->load($product->getEntityId());
            } catch (\Exception $e) {
                $e->getMessage();
            }

            $stockState = $this->context->getObjectManager()->create('Magento\CatalogInventory\Api\StockRegistryInterface');
            $stockData = $stockState->getStockItem($product->getEntityId(), $product->getStore()->getWebsiteId());

            $mediaGallery = [];
            try {
                $productImage = $this->context->getObjectManager()->create('Magento\Catalog\Model\Product');
                $productImage->load($product->getEntityId());
                $baseImageId = $this->getBaseImageId($productImage->getMediaGalleryEntries());
                $mediaGallery = $this->getMediaGalleryByProduct($product->getEntityId(), $productImage->getMediaGalleryImages()->getItems(), $baseImageId, $mediaDirectory);
            } catch (\Exception $e) {}

            $products[] = [
                Responses::KEY_PRODUCT_ID => $product->getEntityId(),
                Responses::KEY_VENDOR_ID => empty($userId = $product->getKaravaniumUserId()) ? 0 : $userId,
                Responses::KEY_VENDOR_NAME => $product->getData(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME),
                Responses::KEY_VENDOR_RATING => !empty($product->getData(Constants::ATTRIBUTE_KARAVANIUM_RATING))
                    ? $product->getData(Constants::ATTRIBUTE_KARAVANIUM_RATING)
                    : 0,
                Responses::KEY_VENDOR_AVATAR => !empty($product->getData(Constants::ATTRIBUTE_KARAVANIUM_AVATAR))
                    ? $baseUrl . $product->getData(Constants::ATTRIBUTE_KARAVANIUM_AVATAR)
                    : '',
                Responses::KEY_VENDOR_SUBSCRIBED => !empty(strstr($vendorSubscription, $product->getKaravaniumUserId())) ? 1 : 0,
                Responses::KEY_SKU => !empty($customSku = $product->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_SKU))
                    ? $customSku->getValue()
                    : '',
                Responses::KEY_NAME => $product->getName(),
                Responses::KEY_DESCRIPTION => $product->getDescription(),
                Responses::KEY_IMAGES => $mediaGallery,
                Responses::KEY_STATUS => ($product->getStatus() == ProductStatus::STATUS_ENABLED) ? 1 : 0,
                Responses::KEY_QTY => (int) $stockData->getQty(),
                Responses::KEY_PRICE => $price = empty($product->getPrice()) ? 0 : $product->getPrice(),
                Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                Responses::KEY_AVAILABLE_FOR_ORDER => $this->isProductAvailable($product) ? 1 : 0
            ];
        }

        return [
            Responses::KEY_PRODUCTS_COUNT => $productCount,
            Responses::KEY_PRODUCTS => $products
        ];
    }

    private function isProductAvailable($product)
    {
        $product = $this->productRepository->getById($product->getEntityId());
        /** @var \Magento\CatalogInventory\Api\Data\StockItemInterface $stockData*/
        $stockState = $this->context->getObjectManager()->create('Magento\CatalogInventory\Api\StockRegistryInterface');
        $stockData = $stockState->getStockItem($product->getId(), $product->getStore()->getWebsiteId());
        $availableForOrders = false;
        if (!empty($stockData)
            && $product->getStatus() == ProductStatus::STATUS_ENABLED
            && (bool)$stockData->getManageStock()
            && (bool)$stockData->getBackorders()
            && $stockData->getIsInStock()
        ) {
            $availableForOrders = true;
        } elseif (!empty($stockData)
            && $product->getStatus() == ProductStatus::STATUS_ENABLED
            && !$stockData->getManageStock()
            && $stockData->getQty() > 0
            && $stockData->getIsInStock()
        ) {
            $availableForOrders = true;
        }

        return $availableForOrders;
    }

    private function getProductsByIds()
    {
        if (empty($this->product_ids)) {
            return Responses::ERROR_EMPTY_PRODUCT_IDS;
        }

        $productsModel = Tools::getProductModel($this->context->getObjectManager());
        $productsCollection = $productsModel->getCollection();
        $productsCollection->addFieldToFilter('status', ['eq' => ProductStatus::STATUS_ENABLED]);

        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
        $vendorSubscribed   = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
        $vendorSubscription = !empty($vendorSubscribed) ? $vendorSubscribed->getValue() : '0';

        /** @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $productsCollection */
        $productsCollection->getSelect()->joinLeft(
            ['cpkui' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'cpkui.entity_id = e.entity_id AND cpkui.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_user_id' LIMIT 1) AND cpkui.store_id = 0", []
        )->joinLeft(
            ['cs' => Tools::getDbTablePrefix() . 'customer_entity'], 'cs.entity_id = cpkui.value', []
        )->joinLeft(
            ['cekn' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekn.entity_id = cs.entity_id AND cekn.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_vendor_name' LIMIT 1)", ['cekn.value AS karavanium_vendor_name']
        )->joinLeft(
            ['cekr' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekr.entity_id = cs.entity_id AND cekr.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_rating' LIMIT 1)", ['cekr.value AS karavanium_rating']
        )->joinLeft(
            ['ceka' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'ceka.entity_id = cs.entity_id AND ceka.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_avatar' LIMIT 1)", ['ceka.value AS karavanium_avatar']
        )->where(
            "e.entity_id IN ($this->product_ids)"
        );

        $productsCountObject = clone $productsCollection;
        $productsCountObject->getSelect()->reset(\Magento\Framework\DB\Select::GROUP);
        $productCount = $productsCountObject->getConnection()->fetchOne($productsCountObject->getSelectCountSql());
        $productsCollection->getSelect()->limit($this->page_size, ($this->page - 1) * $this->page_size);
        $sortBy = $this->sort_by;
        if (empty($sortBy)) {
            $sortBy = 'id';
        }

        switch ($sortBy) {
            case 'name':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'ASC';
                $productsCollection->getSelect()->order(["name $orderBy"]);
                break;
            case 'id':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
                $productsCollection->getSelect()->order(["e.entity_id $orderBy"]);
                break;
            case 'qty':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
                $productsCollection->getSelect()->order(["si.qty $orderBy"]);
                break;
            case 'price':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
                $productsCollection->getSelect()->order(["price $orderBy"]);
                break;
            case 'status':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'ASC';
                $productsCollection->getSelect()->order(["status $orderBy"]);
                break;
        }

        try {
            $baseUrl = $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        } catch (FileSystemException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $products = [];
        foreach ($productsCollection as $product) {
            /** @var \Magento\Catalog\Model\Product $product*/
            try {
                $product->load($product->getEntityId());
            } catch (\Exception $e) {
                $e->getMessage();
            }

            $stockState = $this->context->getObjectManager()->create('Magento\CatalogInventory\Api\StockRegistryInterface');
            $stockData = $stockState->getStockItem($product->getEntityId(), $product->getStore()->getWebsiteId());

            $mediaGallery = [];
            try {
                $productImage = $this->context->getObjectManager()->create('Magento\Catalog\Model\Product');
                $productImage->load($product->getEntityId());
                $baseImageId = $this->getBaseImageId($productImage->getMediaGalleryEntries());
                $mediaGallery = $this->getMediaGalleryByProduct($product->getEntityId(),
                    $productImage->getMediaGalleryImages()->getItems(),
                    $baseImageId,
                    $mediaDirectory
                );
            } catch (\Exception $e) {}

            $products[] = [
                Responses::KEY_PRODUCT_ID => $product->getEntityId(),
                Responses::KEY_VENDOR_ID => empty($userId = $product->getKaravaniumUserId()) ? 0 : $userId,
                Responses::KEY_VENDOR_NAME => $product->getData(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME),
                Responses::KEY_VENDOR_RATING => !empty($product->getData(Constants::ATTRIBUTE_KARAVANIUM_RATING))
                    ? $product->getData(Constants::ATTRIBUTE_KARAVANIUM_RATING)
                    : 0,
                Responses::KEY_VENDOR_AVATAR => !empty($product->getData(Constants::ATTRIBUTE_KARAVANIUM_AVATAR))
                    ? $baseUrl . $product->getData(Constants::ATTRIBUTE_KARAVANIUM_AVATAR)
                    : '',
                Responses::KEY_VENDOR_SUBSCRIBED => !empty(strstr($vendorSubscription, $product->getKaravaniumUserId())) ? 1 : 0,
                Responses::KEY_SKU => !empty($customSku = $product->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_SKU))
                    ? $customSku->getValue()
                    : '',
                Responses::KEY_NAME => $product->getName(),
                Responses::KEY_DESCRIPTION => $product->getDescription(),
                Responses::KEY_IMAGES => $mediaGallery,
                Responses::KEY_STATUS => ($product->getStatus() == ProductStatus::STATUS_ENABLED) ? 1 : 0,
                Responses::KEY_QTY => (int) $stockData->getQty(),
                Responses::KEY_PRICE => $price = empty($product->getPrice()) ? 0 : $product->getPrice(),
                Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                Responses::KEY_AVAILABLE_FOR_ORDER  => $this->isProductAvailable($product) ? 1 : 0
            ];
        }

        return [
            Responses::KEY_PRODUCTS_COUNT => $productCount,
            Responses::KEY_PRODUCTS => $products
        ];
    }

    private function searchProductsForClient()
    {
        $productsModel = Tools::getProductModel($this->context->getObjectManager());
        $productsCollection = $productsModel->getCollection();
        $productsCollection->addFieldToFilter('status', ['eq' => ProductStatus::STATUS_ENABLED]);

        /** @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $productsCollection */
        $productsCollection->getSelect()->joinLeft(
            ['main_name' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'main_name.entity_id = e.entity_id AND main_name.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'name' AND entity_type_id = 4 LIMIT 1)", []
        )->joinLeft(
            ['cpkui' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'cpkui.entity_id = e.entity_id AND cpkui.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_user_id' LIMIT 1) AND cpkui.store_id = 0", []
        )->joinLeft(
            ['cs' => Tools::getDbTablePrefix() . 'customer_entity'], 'cs.entity_id = cpkui.value', []
        )->joinLeft(
            ['cekn' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekn.entity_id = cs.entity_id AND cekn.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_vendor_name' LIMIT 1)", ['cekn.value AS karavanium_vendor_name']
        )->joinLeft(
            ['cekr' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekr.entity_id = cs.entity_id AND cekr.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_rating' LIMIT 1)", ['cekr.value AS karavanium_rating']
        )->joinLeft(
            ['ceka' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'ceka.entity_id = cs.entity_id AND ceka.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_avatar' LIMIT 1)", ['ceka.value AS karavanium_avatar']
        )->joinLeft(
            ['t' => Tools::getDbTablePrefix() . Constants::TABLE_TAGS], "t.value LIKE '%$this->text%'", []
        )->joinLeft(
            ['ttp' => Tools::getDbTablePrefix() . Constants::TABLE_TAG_TO_PRODUCT], "ttp.product_id = e.entity_id AND t.tag_id = ttp.tag_id",
            ['ttp.tag_id']
        )->where('(main_name.value LIKE ? ', "%$this->text%"
        )->orWhere('ttp.tag_id > 0)'
        )->group('e.entity_id'
        )->order('e.entity_id DESC');

        $productsCountObject = clone $productsCollection;
        $productCount = count($productsCountObject->getItems());

        $productsCollection->getSelect()->limit($this->page_size,($this->page - 1) * $this->page_size);
        $products = [];
        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        } catch (FileSystemException $e) {
        }
        foreach ($productsCollection as $product) {
            /** @var \Magento\Catalog\Model\Product $product*/
            try {
                $product->load($product->getEntityId());
            } catch (\Exception $e) {
                return [Responses::ERROR => $e->getMessage()];
            }

            $stockState = $this->context->getObjectManager()->create('Magento\CatalogInventory\Api\StockRegistryInterface');
            $stock = $stockState->getStockItem(
                $product->getEntityId(), $product->getStore()->getWebsiteId()
            );
            try {
                $customer = $this->customerRepositoryInterface->getById($this->user_id);
                $vendorSubscribed = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
            } catch (NoSuchEntityException $e) {
            } catch (LocalizedException $e) {
                return [Responses::ERROR => $e->getMessage()];
            }

            try {
                $productImage = $this->context->getObjectManager()->create('Magento\Catalog\Model\Product');
                $productImage->load($product->getEntityId());
                $baseImageId = $this->getBaseImageId($productImage->getMediaGalleryEntries());
                $mediaGallery = $this->getMediaGalleryByProduct(
                    $product->getEntityId(),
                    $productImage->getMediaGalleryImages()->getItems(),
                    $baseImageId,
                    $mediaDirectory
                );
            } catch (\Exception $e) {}

            $vendorSubscribedValue = !empty($vendorSubscribed) ? $vendorSubscribed->getValue() : '';
            $baseUrl = $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);
            $products[] = [
                Responses::KEY_PRODUCT_ID => $product->getEntityId(),
                Responses::KEY_VENDOR_ID => empty($userId = $product->getKaravaniumUserId()) ? 0 : $userId,
                Responses::KEY_VENDOR_NAME => $product->getData(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME),
                Responses::KEY_VENDOR_RATING => !empty($product->getData(Constants::ATTRIBUTE_KARAVANIUM_RATING))
                    ? $product->getData(Constants::ATTRIBUTE_KARAVANIUM_RATING)
                    : 0,
                Responses::KEY_VENDOR_AVATAR => !empty($product->getData(Constants::ATTRIBUTE_KARAVANIUM_AVATAR))
                    ? $baseUrl . $product->getData(Constants::ATTRIBUTE_KARAVANIUM_AVATAR)
                    : '',
                Responses::KEY_VENDOR_SUBSCRIBED => !empty(strstr($vendorSubscribedValue, $product->getKaravaniumUserId())) ? 1 : 0,
                Responses::KEY_SKU => !empty(
                $customSku = $product->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_SKU)
                )
                    ? $customSku->getValue()
                    : '',
                Responses::KEY_NAME => $product->getName(),
                Responses::KEY_DESCRIPTION => $product->getDescription(),
                Responses::KEY_IMAGES => $mediaGallery,
                Responses::KEY_STATUS => ($product->getStatus() == ProductStatus::STATUS_ENABLED) ? 1 : 0,
                Responses::KEY_QTY => (int) $stock->getQty(),
                Responses::KEY_PRICE => $price = empty($product->getPrice()) ? 0 : $product->getPrice(),
                Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                Responses::KEY_AVAILABLE_FOR_ORDER  => $this->isProductAvailable($product) ? 1 : 0
            ];
        }

        return [
            Responses::KEY_PRODUCTS_COUNT => $productCount,
            Responses::KEY_PRODUCTS => $products
        ];
    }

    private function searchProductsForVendor()
    {
        $productsCollection = Tools::getProductModel($this->context->getObjectManager())->getCollection()->addAttributeToSelect(
            [
                'entity_id',
                'name',
                'description',
                'sku',
                'status',
                'karavanium_user_id',
                'entity_type'
            ]
        );

        /** @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $productsCollection */
        $productsCollection->getSelect()->joinLeft(
            ['main_name' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'main_name.entity_id = e.entity_id AND main_name.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'name' AND entity_type_id = 4 LIMIT 1) AND main_name.store_id = 0", []
        )->joinLeft(
            ['cpkui' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'cpkui.entity_id = e.entity_id AND cpkui.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_user_id' LIMIT 1) AND cpkui.store_id = 0", []
        )->joinLeft(
            ['cs' => Tools::getDbTablePrefix() . 'customer_entity'], 'cs.entity_id = cpkui.value', []
        )->joinLeft(
            ['cekn' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekn.entity_id = cs.entity_id AND cekn.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_vendor_name' LIMIT 1)", ['cekn.value AS karavanium_vendor_name']
        )->joinLeft(
            ['cekr' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekr.entity_id = cs.entity_id AND cekr.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_rating' LIMIT 1)", ['cekr.value AS karavanium_rating']
        )->joinLeft(
            ['ceka' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'ceka.entity_id = cs.entity_id AND ceka.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_avatar' LIMIT 1)", ['ceka.value AS karavanium_avatar']
        )->joinLeft(
            ['t' => Tools::getDbTablePrefix() . Constants::TABLE_TAGS], "t.value LIKE '%$this->text%'", []
        )->joinLeft(
            ['ttp' => Tools::getDbTablePrefix() . Constants::TABLE_TAG_TO_PRODUCT],
            'ttp.product_id = e.entity_id AND t.tag_id = ttp.tag_id',
            ['ttp.tag_id']
        )->where('cpkui.value = ?', $this->user_id
        )->where('main_name.value LIKE ?', "%$this->text%"
        )->group('e.entity_id'
        )->order('e.entity_id DESC');

        $productsCountObject = clone $productsCollection;
        $productCount = count($productsCountObject->getItems());

        $productsCollection->getSelect()->limit($this->page_size - 1,($this->page - 1) * $this->page_size);
        $sortBy = $this->sort_by;
        if (empty($sortBy)) {
            $sortBy = 'id';
        }

        switch ($sortBy) {
            case 'name':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'ASC';
                $productsCollection->getSelect()->order(["name $orderBy"]);
                break;
            case 'id':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
                $productsCollection->getSelect()->order(["e.entity_id $orderBy"]);
                break;
            case 'qty':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
                $productsCollection->getSelect()->order(["si.qty $orderBy"]);
                break;
            case 'price':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'DESC';
                $productsCollection->getSelect()->order(["price $orderBy"]);
                break;
            case 'status':
                $orderBy = !empty($this->order_by) ? $this->order_by : 'ASC';
                $productsCollection->getSelect()->order(["status $orderBy"]);
                break;
        }

        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        } catch (FileSystemException $e) {
        }

        $products = [];
        foreach ($productsCollection as $product) {
            /** @var \Magento\Catalog\Model\Product $product*/
            try {
                $product->load($product->getEntityId());
            } catch (\Exception $e) {
                return [Responses::ERROR => $e->getMessage()];
            }

            $stockState = $this->context->getObjectManager()->create('Magento\CatalogInventory\Api\StockRegistryInterface');
            $stock = $stockState->getStockItem($product->getEntityId(), $product->getStore()->getWebsiteId());

            try {
                $productObject = $this->context->getObjectManager()->create('Magento\Catalog\Model\Product');
                $productObject->load($product->getEntityId());
                $baseImageId = $this->getBaseImageId($productObject->getMediaGalleryEntries());
                $mediaGallery = $this->getMediaGalleryByProduct(
                    $product->getEntityId(),
                    $productObject->getMediaGalleryImages()->getItems(),
                    $baseImageId,
                    $mediaDirectory
                );
                $stockData = $productObject->getExtensionAttributes()->getStockItem();
            } catch (\Exception $e) {}

            $products[] = [
                Responses::KEY_PRODUCT_ID => $product->getEntityId(),
                Responses::KEY_SKU => !empty($customSku = $product->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_SKU))
                    ? $customSku->getValue()
                    : '',
                Responses::KEY_NAME => $product->getName(),
                Responses::KEY_IMAGES => $mediaGallery,
                Responses::KEY_STATUS => ($product->getStatus() == ProductStatus::STATUS_ENABLED) ? 1 : 0,
                Responses::KEY_QTY => (int) $stock->getQty(),
                Responses::KEY_PRICE => $price = empty($product->getPrice()) ? 0 : $product->getPrice(),
                Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                Responses::KEY_AVAILABLE_FOR_ORDER  => $this->isProductAvailable($product) ? 1 : 0
            ];
        }

        return [
            Responses::KEY_PRODUCTS_COUNT => $productCount,
            Responses::KEY_PRODUCTS => $products
        ];
    }

    private function getTagDetails()
    {
        $productsModel = Tools::getProductModel($this->context->getObjectManager());
        $productsCollection = $productsModel->getCollection();
        $productsCollection->addFieldToFilter('status', ['eq' => ProductStatus::STATUS_ENABLED]);

        /** @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $productsCollection */
        $productsCollection->getSelect(
        )->joinLeft(
            ['main_name' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'main_name.entity_id = e.entity_id AND main_name.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'name' AND entity_type_id = 4 LIMIT 1)", []
        )->joinLeft(
            ['cpkui' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'cpkui.entity_id = e.entity_id AND cpkui.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_user_id' LIMIT 1) AND cpkui.store_id = 0", []
        )->joinLeft(
            ['cs' => Tools::getDbTablePrefix() . 'customer_entity'], 'cs.entity_id = cpkui.value', []
        )->joinLeft(
            ['cekn' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekn.entity_id = cs.entity_id AND cekn.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_vendor_name' LIMIT 1)", ['cekn.value AS karavanium_vendor_name']
        )->joinLeft(
            ['cekr' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekr.entity_id = cs.entity_id AND cekr.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_rating' LIMIT 1)", ['cekr.value AS karavanium_rating']
        )->joinLeft(
            ['ceka' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'ceka.entity_id = cs.entity_id AND ceka.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_avatar' LIMIT 1)", ['ceka.value AS karavanium_avatar']
        )->joinLeft(
            ['ttp' => Tools::getDbTablePrefix() . Constants::TABLE_TAG_TO_PRODUCT], 'ttp.product_id = e.entity_id',
            ['ttp.tag_id']
        )->where('ttp.tag_id = ?', $this->tag_id
        )->group('e.entity_id'
        )->order('e.entity_id DESC');

        $productsCollection->getSelect()->limit($this->page_size,($this->page - 1) * $this->page_size);
        $products = [];
        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        } catch (FileSystemException $e) {
        }
        foreach ($productsCollection as $product) {
            /** @var \Magento\Catalog\Model\Product $product*/
            try {
                $product->load($product->getEntityId());
            } catch (\Exception $e) {
                return [Responses::ERROR => $e->getMessage()];
            }

            $stockState = $this->context->getObjectManager()->create('Magento\CatalogInventory\Api\StockRegistryInterface');
            $stock = $stockState->getStockItem(
                $product->getEntityId(), $product->getStore()->getWebsiteId()
            );
            try {
                $customer = $this->customerRepositoryInterface->getById($this->user_id);
                $vendorSubscribed = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
            } catch (NoSuchEntityException $e) {
            } catch (LocalizedException $e) {
                return [Responses::ERROR => $e->getMessage()];
            }

            try {
                $productImage = $this->context->getObjectManager()->create('Magento\Catalog\Model\Product');
                $productImage = $this->context->getObjectManager()->create('Magento\Catalog\Model\Product');
                $productImage->load($product->getEntityId());
                $baseImageId = $this->getBaseImageId($productImage->getMediaGalleryEntries());
                $mediaGallery = $this->getMediaGalleryByProduct(
                    $product->getEntityId(),
                    $productImage->getMediaGalleryImages()->getItems(),
                    $baseImageId,
                    $mediaDirectory
                );
            } catch (\Exception $e) {}

            $vendorSubscribedValue = !empty($vendorSubscribed) ? $vendorSubscribed->getValue() : '';
            $baseUrl = $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);
            $products[] = [
                Responses::KEY_PRODUCT_ID => $product->getEntityId(),
                Responses::KEY_VENDOR_ID => empty($userId = $product->getKaravaniumUserId()) ? 0 : $userId,
                Responses::KEY_VENDOR_NAME => $product->getData(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME),
                Responses::KEY_VENDOR_RATING => !empty($product->getData(Constants::ATTRIBUTE_KARAVANIUM_RATING))
                    ? $product->getData(Constants::ATTRIBUTE_KARAVANIUM_RATING)
                    : 0,
                Responses::KEY_VENDOR_AVATAR => !empty($product->getData(Constants::ATTRIBUTE_KARAVANIUM_AVATAR))
                    ? $baseUrl . $product->getData(Constants::ATTRIBUTE_KARAVANIUM_AVATAR)
                    : '',
                Responses::KEY_VENDOR_SUBSCRIBED => !empty(strstr($vendorSubscribedValue, $product->getKaravaniumUserId())) ? 1 : 0,
                Responses::KEY_SKU => !empty($customSku = $product->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_SKU))
                    ? $customSku->getValue()
                    : '',
                Responses::KEY_NAME => $product->getName(),
                Responses::KEY_DESCRIPTION => $product->getDescription(),
                Responses::KEY_IMAGES => $mediaGallery,
                Responses::KEY_STATUS => ($product->getStatus() == ProductStatus::STATUS_ENABLED) ? 1 : 0,
                Responses::KEY_QTY => (int) $stock->getQty(),
                Responses::KEY_PRICE => $price = empty($product->getPrice()) ? 0 : $product->getPrice(),
                Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code)
            ];
        }

        return [
            'tag' => $this->getTag(),
            Responses::KEY_PRODUCTS => $products
        ];
    }

    private function getBaseImageId($mediaEntries)
    {
        $result = 0;
        foreach ($mediaEntries as $media) {
            if (!in_array('image', $imageType = $media->getTypes())) {
                continue;
            }
            $result = $media->getId();
        }

        return $result;
    }

    private function getMediaGalleryByProduct($productId, $mediaItems, $baseImageId, $mediaDirectory)
    {
        $baseImage = [];
        $otherImages = [];
        foreach ($mediaItems as $mediaItem) {
            if ($mediaItem->getValueId() == $baseImageId) {
                $isImageBase = ($mediaItem->getValueId() == $baseImageId) ? 1 : 0;
                $baseImage[] = $this->imageResponse($productId, $mediaItem->getValueId(), $isImageBase, $this->resizeImage($mediaItem->getUrl(), $mediaDirectory));
                continue;
            }
            $isImageBase = ($mediaItem->getValueId() == $baseImageId) ? 1 : 0;
            $otherImages[] = $this->imageResponse($productId, $mediaItem->getValueId(), $isImageBase, $this->resizeImage($mediaItem->getUrl(), $mediaDirectory));
        }

        return array_merge($baseImage, $otherImages);
    }

    private function getLastImageByProduct($productId)
    {
        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        } catch (FileSystemException $e) {}

        $productImage   = $this->context->getObjectManager()->create('Magento\Catalog\Model\Product');
        $productImage->load($productId);
        $imageItem      = $productImage->getMediaGalleryImages()->getLastItem();
        $imageItemId    = $imageItem ->getValueId();
        $imageItemUrl   = $imageItem ->getUrl();
        $baseImageId    = $this->getBaseImageId($productImage->getMediaGalleryEntries());
        $isImageBase    = ($imageItemId == $baseImageId) ? 1 : 0;

        return $this->imageResponse($productId, $imageItemId, $isImageBase, $this->resizeImage($imageItemUrl, $mediaDirectory));
    }

    private function imageResponse($productId, $valueId, $isImageBase, $imageUrl)
    {
        return [
            Responses::KEY_PRODUCT_ID   => (int) $productId,
            Responses::KEY_IMAGE_ID     => (int) $valueId,
            Responses::KEY_BASE_IMAGE   => (int) $isImageBase,
            Responses::KEY_URL          => (string) $imageUrl
        ];
    }

    private function productResponse(int $productCount, array $products)
    {
        return [
            Responses::KEY_PRODUCTS_COUNT   => $productCount,
            Responses::KEY_PRODUCTS         => $products
        ];
    }

    private function getDataForNewProduct()
    {
        return [
            Responses::PRODUCT => [
                Constants::COLUMN_STATUS            => ProductStatus::STATUS_ENABLED,
                Responses::TAGS                     => $this->getTagsByLastProduct(),
                Responses::MAX_FILE_UPLOAD_IN_BYTES => $this->getMaxFileUploadInBytes(),
                'backorders'                        => '1'
            ]
        ];
    }

    private function getProductToEdit()
    {
        $productId = $this->product_id;
        if ($productId <= 0) {
            return Responses::ERROR_NO_SUCH_PRODUCT;
        }

        /** @var \Magento\Catalog\Model\Product $product */
        try {
            $product = $this->productRepositoryInterface->getById($productId);
        } catch (NoSuchEntityException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
        $productStock = $product->getQuantityAndStockStatus();

        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
            $baseImageId = $this->getBaseImageId($product->getMediaGalleryEntries());
            $mediaGallery = $this->getMediaGalleryByProduct($productId, $product->getMediaGalleryImages()->getItems(), $baseImageId, $mediaDirectory);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $productData = [
            Responses::KEY_PRODUCT_ID               => $product->getEntityId(),
            Responses::KEY_NAME                     => $product->getName(),
            Responses::KEY_SKU                      => $product->getKaravaniumSku(),
            Responses::KEY_DESCRIPTION              => $product->getDescription(),
            Responses::KEY_STATUS                   => ($product->getStatus() == ProductStatus::STATUS_ENABLED) ? 1 : 0,
            Responses::KEY_QTY                      => (int) $productStock[Constants::COLUMN_QTY],
            Responses::KEY_PRICE                    => $price = (double) $product->getPrice(),
            Responses::KEY_FORMATTED_PRICE          => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
            Responses::KEY_TAGS                     => $this->getProductTags($product->getEntityId()),
            Responses::KEY_IMAGES                   => $mediaGallery,
            Responses::KEY_MAX_FILE_UPLOAD_IN_BYTES => $this->getMaxFileUploadInBytes(),
        ];
        $stockData = $product->getExtensionAttributes()->getStockItem();

        if ($stockData->getBackorders() && $stockData->getManageStock() && $stockData->getIsInStock()) {
            $productData = array_merge($productData, ['backorders' => 1]);
        }
        return [Responses::PRODUCT => $productData];
    }

    private function getProductDetails()
    {
        $productId = $this->product_id;
        if ($productId <= 0) {
            return Responses::ERROR_NO_SUCH_PRODUCT;
        }

        /** @var \Magento\Catalog\Model\Product $product */
        try {
            $product = $this->productRepository->getById($productId);
        } catch (NoSuchEntityException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $productStock = $product->getQuantityAndStockStatus();
        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
            $vendor = $this->customerRepositoryInterface->getById($product->getKaravaniumUserId());
            $vendorSubscribed = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
            $baseImageId = $this->getBaseImageId($product->getMediaGalleryEntries());
            $mediaGallery = $this->getMediaGalleryByProduct($product->getEntityId(), $product->getMediaGalleryImages()->getItems(), $baseImageId, $mediaDirectory);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $vendorSubscribedValue = !empty($vendorSubscribed) ? $vendorSubscribed->getValue() : '';
        $baseUrl = $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);

        return [
            Responses::PRODUCT => [
                Responses::KEY_PRODUCT_ID               => $product->getEntityId(),
                Responses::KEY_VENDOR_ID                => empty($userId = $product->getKaravaniumUserId()) ? 0 : $userId,
                Responses::KEY_VENDOR_NAME              => !empty($vendorName = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME))
                    ? $vendorName->getValue()
                    : 0,
                Responses::KEY_VENDOR_COUNTRY           => !empty($vendorCountry = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_COUNTRY))
                    ? $vendorCountry->getValue()
                    : '',
                Responses::KEY_VENDOR_RATING            => !empty($vendorRating = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_RATING))
                    ? $vendorRating->getValue()
                    : 0,
                Responses::KEY_VENDOR_AVATAR            => !empty($vendorAvatar = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR))
                    ? $baseUrl . $vendorAvatar->getValue()
                    : '',
                Responses::KEY_VENDOR_SUBSCRIBED        => !empty(strstr($vendorSubscribedValue, $product->getKaravaniumUserId()))
                    ? 1
                    : 0,
                Responses::KEY_SKU                      => $product->getData(Constants::ATTRIBUTE_KARAVANIUM_SKU),
                Responses::KEY_NAME                     => $product->getName(),
                Responses::KEY_DESCRIPTION              => $product->getDescription(),
                Responses::KEY_IMAGES                   => $mediaGallery,
                Responses::KEY_STATUS                   => ($product->getStatus() == ProductStatus::STATUS_ENABLED)
                    ? 1
                    : 0,
                Responses::KEY_TAGS                     => $this->getProductTags($product->getEntityId()),
                Responses::KEY_QTY                      => (int) $productStock[Constants::COLUMN_QTY],
                Responses::KEY_PRICE                    => $price = (double) $product->getPrice(),
                Responses::KEY_FORMATTED_PRICE          => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                Responses::KEY_MAX_FILE_UPLOAD_IN_BYTES => $this->getMaxFileUploadInBytes()
            ]
        ];
    }

    private function getVendorDetails()
    {
        $vendor = [];
        $products = [];
        if (empty($this->vendor_id)) {
            return [Responses::ERROR => 'empty_vendor_id'];
        }

        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        } catch (FileSystemException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        try {
            $user = $this->customerRepositoryInterface->getById($this->user_id);
            $vendorSubscribed = $user->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
            $vendorSubscribedValue = !empty($vendorSubscribed) ? $vendorSubscribed->getValue() : '';

            $vendor = $this->customerRepositoryInterface->getById($this->vendor_id);
            $vendorVendorName = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME);
            $vendorVendorNameValue = empty($vendorVendorName) ? '' : $vendorVendorName->getValue();
            $vendorAvatarUrl = !empty($vendorAvatar = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR))
                ? $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $vendorAvatar->getValue()
                : '';
            $vendorRating = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_RATING);
            $vendorRatingValue = empty($vendorRating) ? 0 : $vendorRating->getValue();
            $vendorCountry = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_COUNTRY);
            $vendorCountryValue = empty($vendorCountry) ? '' : $vendorCountry->getValue();
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_USER_ID;
        }

        $vendorPageSize = $this->page_size;
        $vendorProductCount = $this->vendorDetailsProductCount();
        if ((int)ceil($vendorProductCount / $vendorPageSize) < $this->page) {
            $sortOrder = $this->sortOrder->setField(Constants::COLUMN_ENTITY_ID)->setDirection(SortOrder::SORT_DESC);
            $filterOne = $this->filterBuilder
                ->setField(Constants::ATTRIBUTE_KARAVANIUM_USER_ID)
                ->setConditionType('eq')
                ->setValue($this->vendor_id)
                ->create();
            $filterTwo = $this->filterBuilder
                ->setField(Constants::COLUMN_STATUS)
                ->setConditionType('eq')
                ->setValue(ProductStatus::STATUS_ENABLED)
                ->create();
            $filterGroupOne = $this->filterGroupBuilder->addFilter($filterOne)->create();
            $filterGroupTwo = $this->filterGroupBuilder->addFilter($filterTwo)->create();

            $this->searchCriteriaBuilder->setSortOrders([$sortOrder]);
            $this->searchCriteriaBuilder->setFilterGroups([$filterGroupOne, $filterGroupTwo]);
            $this->searchCriteriaBuilder->setPageSize($vendorPageSize);
            $this->searchCriteriaBuilder->setCurrentPage($this->page);

            $searchCriteria = $this->searchCriteriaBuilder->create();
            $productList = $this->productRepository->getList($searchCriteria);
            foreach ($productList->getItems() as $product) {
                /** @var \Magento\Catalog\Model\Product $product */
                $stockState = $this->context->getObjectManager()->create('Magento\CatalogInventory\Api\StockRegistryInterface');
                $stock = $stockState->getStockItem($product->getEntityId(), $product->getStore()->getWebsiteId());

                $mediaGallery = [];
                try {
                    $productObject = $this->context->getObjectManager()->create('Magento\Catalog\Model\Product');
                    $productObject->load($product->getEntityId());
                    $baseImageId = $this->getBaseImageId($productObject->getMediaGalleryEntries());
                    $mediaGallery = $this->getMediaGalleryByProduct($product->getEntityId(),
                        $productObject->getMediaGalleryImages()->getItems(),
                        $baseImageId,
                        $mediaDirectory
                    );
                } catch (\Exception $e) {}

                $products[] = [
                    Responses::KEY_PRODUCT_ID => $product->getEntityId(),
                    Responses::KEY_SKU => $product->getKaravaniumSku(),
                    Responses::KEY_NAME => $product->getName(),
                    Responses::KEY_DESCRIPTION => $product->getDescription(),
                    Responses::KEY_IMAGES => $mediaGallery,
                    Responses::KEY_STATUS => ($product->getStatus() == ProductStatus::STATUS_ENABLED) ? 1 : 0,
                    Responses::KEY_QTY => (int)$stock->getQty(),
                    Responses::KEY_PRICE => $price = empty($product->getPrice()) ? 0 : $product->getPrice(),
                    Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency,
                        $this->currency_code),
                    Responses::KEY_VENDOR_ID => $this->vendor_id,
                    Responses::KEY_VENDOR_NAME => $vendorVendorNameValue,
                    Responses::KEY_VENDOR_COUNTRY => $this->getCountryTitleById($vendorCountryValue),
                    Responses::KEY_VENDOR_RATING => $vendorRatingValue,
                    Responses::KEY_VENDOR_AVATAR => $vendorAvatarUrl,
                    Responses::KEY_VENDOR_SUBSCRIBED =>
                        !empty(strstr($vendorSubscribedValue, $this->vendor_id))
                            ? 1
                            : 0,
                    Responses::KEY_AVAILABLE_FOR_ORDER => $this->isProductAvailable($product) ? 1 : 0
                ];
            }
        }

        $vendor = [
            Responses::KEY_VENDOR_ID => $this->vendor_id,
            Responses::KEY_VENDOR_NAME => $vendorVendorNameValue,
            Responses::KEY_VENDOR_COUNTRY => $this->getCountryTitleById($vendorCountryValue),
            Responses::KEY_VENDOR_RATING => $vendorRatingValue,
            Responses::KEY_VENDOR_AVATAR => $vendorAvatarUrl,
            Responses::KEY_VENDOR_SUBSCRIBED =>
                !empty(strstr($vendorSubscribedValue, $this->vendor_id))
                    ? 1
                    : 0
        ];

        return array_merge(
            ['vendor' => $vendor],
            $this->productResponse($vendorProductCount, $products)
        );
    }

    private function getTagsByLastProduct()
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter(Constants::ATTRIBUTE_KARAVANIUM_USER_ID, $this->user_id)->create();

        $products = $this->productRepositoryInterface->getList($searchCriteria)->getItems();

        $productId = 0;
        foreach ($products as $product) {
            $productId = $product->getId();
        }

        try {
            return $this->getProductTags($productId);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
    }

    private function saveProduct()
    {
        if (empty($this->data)) {
            return Responses::ERROR_NO_DATA;
        }

        try {
            $data = Tools::jsonDecode($this->data);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        if (!$data) {
            return Responses::ERROR_INCORRECT_FORMAT_OF_DATA;
        }

        $newProductId = (int)$data[Responses::KEY_PRODUCT_ID];
        try {
            $product = $newProductId > 0
                ? $this->productRepositoryInterface->getById($newProductId)
                : $this->productInterfaceFactory->create();
        } catch (NoSuchEntityException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        /** @var \Magento\Catalog\Model\Product $product */

        $currentDate = date('Y-m-d H:i:s');
        if ($newProductId == 0) {

            $product->setSku(md5(time() . rand()));
            $product->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE);
            $product->setAttributeSetId(4);
            $product->setCreatedAt($currentDate);
            $product->setUpdatedAt($currentDate);
            $product->setVisibility(4);
            $urlKey = $this->checkProductUrlKeyDuplicates(
                $this->context->getObjectManager()->create(\Magento\Catalog\Model\Product\Url::class)->formatUrlKey($data[Constants::COLUMN_NAME])
            );
            $product->setUrlKey($urlKey);
            $product->setWebsiteIds(
                [$this->storeManagerInterface->getStore()->getWebsite()->getWebsiteId()]
            );
            $product->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_USER_ID, $this->user_id);
            $product->setCustomAttribute(
                Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME,
                array_key_exists(Responses::KEY_VENDOR_NAME, $data) ? $data[Responses::KEY_VENDOR_NAME] : $product->getCustomAttribute(
                    Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME
                )
            );
        } else {
            $product->setUpdatedAt($currentDate);
        }

        if (array_key_exists(Constants::COLUMN_STATUS, $data)) {
            $status = (int) $data[Constants::COLUMN_STATUS] > 0 ? ProductStatus::STATUS_ENABLED : ProductStatus::STATUS_DISABLED;
            $product->setStatus($status);
        }

        $product->setName(array_key_exists(Responses::KEY_NAME, $data) ? $data[Responses::KEY_NAME] : $product->getName());
        $product->setPrice(array_key_exists(Responses::KEY_PRICE, $data) ? (double) $data[Responses::KEY_PRICE] : $product->getPrice());
        $product->setDescription(
            array_key_exists(Responses::KEY_DESCRIPTION, $data) ? $data[Responses::KEY_DESCRIPTION] : $product->getDescription()
        );
        $product->setData(
            Constants::ATTRIBUTE_KARAVANIUM_SKU, array_key_exists(Responses::KEY_SKU, $data) ? $data[Responses::KEY_SKU] : $product->getData(
            Constants::ATTRIBUTE_KARAVANIUM_SKU
        )
        );

        $productQty = array_key_exists(Constants::COLUMN_QTY, $data)
            ? (int)(!empty($dataQty = $data[Constants::COLUMN_QTY]) ? $dataQty : 0)
            : (int)$product->getQty();

        /**
         * Set stock data
         *
         * @param array $stockData
         * @return $this
         *
         * @deprecated 101.1.0 as Product model shouldn't be responsible for stock status
         * @see StockItemInterface when you want to change the stock data
         * @see StockStatusInterface when you want to read the stock data for representation layer (storefront)
         * @see StockItemRepositoryInterface::save as extension point for customization of saving process
         * @since 101.1.0
         */
        $stockData = [
            Constants::COLUMN_IS_IN_STOCK   => 1,
            Constants::COLUMN_QTY           => $productQty,
        ];

        if (array_key_exists('backorders', $data)) {
            $stockData = array_merge($stockData, ['use_config_manage_stock' => 0, 'manage_stock' => $data['backorders'], 'backorders' => $data['backorders']]);
        }
        $product->setStockData($stockData);

        if (array_key_exists(Responses::KEY_IMAGES, $data)) {
            $imageProcessor = $this->context->getObjectManager()->create('\Magento\Catalog\Model\Product\Gallery\Processor');
            $productImages = $product->getMediaGalleryImages();
            $imagesIds = [];
            for ($i = 0, $imageCount = count($data[Responses::KEY_IMAGES]); $i < $imageCount; $i++) {
                $imagesIds[] = $data[Responses::KEY_IMAGES][$i][Responses::KEY_IMAGE_ID];
            }

            foreach ($productImages as $productImage) {
                if (!in_array($productImage->getValueId(), $imagesIds)) {
                    $imageProcessor->removeImage($product, $productImage->getFile());
                }
            }
        }

        $newProductId == 0 ? $this->setProductIsNew($product) : null;
        try {
            $savedProduct = $this->productRepositoryInterface->save($product);
            $productId = $savedProduct->getId();
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $this->createProductTags($data[Responses::TAGS], $productId);

        if ($this->isProductAvailable($savedProduct)
            && $this->checkProductIsNew($savedProduct)
            /*&& (array_key_exists(Responses::KEY_HAS_IMAGE, $data)
                ? $data[Responses::KEY_HAS_IMAGE] == 0
                : false)*/
        ) {
            $this->sendNewProductToFirestore($savedProduct);
            $this->unsetProductIsNew($savedProduct);
        }

        $this->product_id = $productId;
        return $this->getProductToEdit();
    }

    private function setProductIsNew($product)
    {
        $product->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_IS_NEW, 1);
    }

    private function unsetProductIsNew($product)
    {
        $product->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_IS_NEW, 0);
    }

    private function checkProductIsNew($product)
    {
        return ($isNew = $product->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_IS_NEW))
            ? ($isNew->getValue() == 1 ? true : false)
            : false;
    }

    private function sendNewProductToFirestore($savedProduct)
    {
        $logPath = '/home/karavanium/public_html/fsmlog.log';
        file_put_contents($logPath, "\r\n------------------------- FSM (START) -------------------------\r\n", FILE_APPEND);

        $product = $this->productRepositoryInterface->getById($savedProduct->getEntityId());
        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
            $baseImageId = $this->getBaseImageId($product->getMediaGalleryEntries());
            $mediaGallery = $this->getMediaGalleryByProduct(
                $product->getId(),
                $product->getMediaGalleryImages()->getItems(),
                $baseImageId,
                $mediaDirectory
            );
        } catch (Exception $e) {}

        $customer = $this->customerRepositoryInterface->getById($this->user_id);
        $vendorName = $customer->getCustomAttribute('karavanium_vendor_name');
        $vendorAvatar = $customer->getCustomAttribute('karavanium_avatar');
        $vendorRating = $customer->getCustomAttribute('karavanium_rating');

        $mediaGallery = empty($mediaGallery) ? '' : json_encode($mediaGallery);
        $notificationData = [
            'productId'         => $product->getId(),
            'vendorId'          => $customer->getId(),
            'vendorName'        => !empty($vendorName) ? $vendorName->getValue() : '',
            'vendorRating'      => !empty($vendorRating) ? $vendorRating->getValue() : 0,
            'vendorAvatar'      => !empty($vendorAvatar)
                ? $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $vendorAvatar->getValue()
                : '',
            'productName'       => $product->getName(),
            'productImage'      => $mediaGallery,
            'formattedPrice' => Tools::getConvertedAndFormattedPrice(
                $product->getPrice(),
                $this->def_currency,
                $this->currency_code
            )
        ];

        file_put_contents($logPath, "\r\n------------------------- FSM (DATA) -------------------------\r\n", FILE_APPEND);
        file_put_contents($logPath, var_export($notificationData, true), FILE_APPEND);
        //todo: get list
        $devices = array();
        $usersIds = $this->getUserIdsForVendor($customer->getId());
        file_put_contents($logPath, "\r\n------------------------- FSM (USER_IDS) -------------------------\r\n", FILE_APPEND);
        file_put_contents($logPath, var_export($usersIds, true), FILE_APPEND);
        foreach ($usersIds as $userId) {
            $userDeviceTokensFactory = Tools::getObjectManager()->create('Emagicone\Karavaniumconnector\Model\UserDeviceTokens');

            /** @var \Emagicone\Karavaniumconnector\Model\UserDeviceTokens $userDeviceTokensFactory */
            $specifiedDeviceToken = $userDeviceTokensFactory
                ->getCollection()
                ->addFieldToFilter(Constants::COLUMN_USER_ID, ['eq' => $userId]);
            $devices = $specifiedDeviceToken->getData();
            file_put_contents($logPath, "\r\n------------------------- FSM (DEVICES) -------------------------\r\n", FILE_APPEND);
            file_put_contents($logPath, var_export($devices, true), FILE_APPEND);

            if (is_callable('curl_init') && !empty($devices)) {
                foreach ($devices as $device) {
                    $data = array(
                        'to'            => $device['device_token'],
                        // If we ever will work with notification title/body.
                        // Should be fixed firstly from firebase side - https://github.com/firebase/quickstart-android/issues/4
                        // 'notification'  => array(
                        //     'body'          => $notificationTitle,
                        //     'title'         => $notificationBody,
                        //     'icon'          => 'ic_launcher',
                        //     'sound'         => 'default',
                        //     'badge'         => '1'
                        // ),
                        'data'          => array(
                            'notificationData'  => $notificationData,
                            'notificationType'  => 'new_product'
                        ),
                        'priority'      => 'high'
                    );

                    $headers = array(
                        'Authorization: key=AAAAKjNQMaY:APA91bHCxBq3h42jY0xosQe_4HDU6-X4G6wQzeR6ID0gFuPEmUcFRpwd91gLZyph2ePZACGkkJAXJXY3yewtoKTFS8cy1KPCDRTLWtIUKbWM3C2tKmoDMaFodWXAf6nWkoLsNa4UzxNG',
                        'Content-Type: application/json'
                    );

                    $url = 'https://fcm.googleapis.com/fcm/send';
                    $ch = curl_init();
                    /** @noinspection CurlSslServerSpoofingInspection */
                    curl_setopt_array(
                        $ch,
                        array(
                            CURLOPT_URL             => $url,
                            CURLOPT_POST            => true,
                            CURLOPT_HTTPHEADER      => $headers,
                            CURLOPT_RETURNTRANSFER  => true,
                            CURLOPT_SSL_VERIFYHOST  => 0,
                            CURLOPT_SSL_VERIFYPEER  => false,
                            CURLOPT_POSTFIELDS      => json_encode($data)
                        )
                    );

                    $result = curl_exec($ch);

                    file_put_contents($logPath, "\r\n------------------------- FSM (CURL RESPONSE) -------------------------\r\n", FILE_APPEND);
                    file_put_contents($logPath, var_export($result, true), FILE_APPEND);
                    if ($result === false && curl_errno($ch)) {
                        file_put_contents($logPath, "\r\n------------------------- FSM (CURL RESPONSE) -------------------------\r\n", FILE_APPEND);
                        file_put_contents($logPath, var_export(curl_error($ch), true), FILE_APPEND);
                    }

                    curl_close($ch);
                }
            }
        }
    }

    private function createProductTags($tags, $productId)
    {
        $tagToProductFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\TagToProductFactory');

        /** @var \Emagicone\Karavaniumconnector\Model\Tags $tagsModelFactory */
        /** @var \Emagicone\Karavaniumconnector\Model\TagToProduct $tagToProductModelFactory */

        $tagToProducts = $tagToProductFactory->create();
        $tagToProductsCollection = $tagToProducts->getCollection()->addFieldToFilter(Constants::COLUMN_PRODUCT_ID, ['eq' => $productId]);

        $this->deleteProductTags($tagToProductsCollection, $this->addProductTags($tags, $tagToProducts, $productId));
    }

    private function deleteProductTags($tagToProductsCollection, $tagsIds)
    {
        $tagAssociationItems = $tagToProductsCollection->getItems();
        foreach ($tagAssociationItems as $tagAssociationItem) {
            if (!in_array($tagAssociationItem->getTagId(), $tagsIds)) {
                $tagAssociationItem->delete($tagAssociationItem->getId());
                continue;
            }
        }
    }

    private function addProductTags($tags, $tagToProducts, $productId)
    {
        $tagsIds = [];
        $tagsFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\TagsFactory');
        if (!empty($tags)) {
            foreach ($tags as $tag) {
                $productTags = $tagsFactory->create();
                $productTagsCollection = $productTags->getCollection()->addFieldToFilter(
                    Constants::COLUMN_VALUE, ['eq' => $tag['title']]
                );

                $tagItem = $productTagsCollection->getFirstItem();
                if (count($tagItem->getData()) > 0) {
                    $tagsIds[] = $tagItem->getId();
                    continue;
                }
                $productTags->setValue($tag['title']);
                $tagSaved = $productTags->save();
                $tagsIds[] = $tagSaved->getTagId();

            }

            for ($i = 0, $itemCount = count($tagsIds); $i < $itemCount; $i++) {
                $tagsChecker = $tagToProducts->getCollection()
                    ->addFieldToFilter(Constants::COLUMN_PRODUCT_ID, ['eq' => $productId])
                    ->addFieldToFilter(Constants::COLUMN_TAG_ID, ['eq' => $tagsIds[$i]])
                    ->getData();
                if (count($tagsChecker) > 0) {
                    continue;
                }

                $tagToProducts->setData(
                    [
                        Constants::COLUMN_TAG_ID => $tagsIds[$i],
                        Constants::COLUMN_PRODUCT_ID => $productId
                    ]
                );
                $tagToProducts->save();
            }
        }

        return $tagsIds;
    }

    private function getProductTags($productId)
    {
        $tagsFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\TagsFactory');
        $tagToProductFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\TagToProductFactory');
        $savedTags = $tagToProductFactory->create()
            ->getCollection()
            ->addFieldToFilter(Constants::COLUMN_PRODUCT_ID, ['eq' => $productId])
            ->getData();

        $tagsIds = [];
        for ($i = 0, $countTags = count($savedTags); $i < $countTags; $i++) {
            $tagsIds[] = $savedTags[$i][Constants::COLUMN_TAG_ID];
        }

        $tags = [];
        $tagsResult = $tagsFactory->create()->getCollection()->addFieldToFilter(Constants::COLUMN_TAG_ID, ['in' => $tagsIds])->getData();
        foreach ($tagsResult as $tag) {
            $tags[] = [
                'id' => $tag[Constants::COLUMN_TAG_ID],
                'title' => $tag[Constants::COLUMN_VALUE]
            ];
        }

        return $tags;
    }

    private function getTag()
    {
        $tagsFactory            = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\TagsFactory');
        $tagToProductFactory    = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\TagToProductFactory');
        $tagsResult = $tagsFactory->create()->getCollection()->addFieldToFilter(Constants::COLUMN_TAG_ID, ['eq' => $this->tag_id])->getData();
        $userToTagsFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\UserToTagFactory')->create();
        $userToTagsCollection = $userToTagsFactory->getCollection()
            ->addFieldToFilter(Constants::COLUMN_USER_ID, ['eq' => $this->user_id])
            ->addFieldToFilter(Constants::COLUMN_TAG_ID, ['eq' => $this->tag_id])
            ->getSize();


        $tagsCount = $tagToProductFactory->create()
            ->getCollection()
            ->addFieldToFilter(Constants::COLUMN_TAG_ID, ['in' => $this->tag_id])
            ->getSize();

        $tag = [];
        foreach ($tagsResult as $oneTag) {
            $tag = [
                'id' => $oneTag[Constants::COLUMN_TAG_ID],
                'title' => $oneTag[Constants::COLUMN_VALUE],
                'products_count' => $tagsCount,
                'subscribed'=> $userToTagsCollection
            ];
        }

        return $tag;
    }

    private function getTags()
    {
        $userId = $this->user_id;
        $userToTagsFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\UserToTagFactory');
        $tagToProductFactory    = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\TagToProductFactory');
        $tagsFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\TagsFactory');
        $userToTags = $userToTagsFactory->create();
        $userToTagsCollection = $userToTags->getCollection()
            ->addFieldToFilter(Constants::COLUMN_USER_ID, ['eq' => $userId])
            ->getData();

        $tagsIds = [];
        for ($i = 0, $tagsCount = count($userToTagsCollection); $i < $tagsCount; $i++) {
            $tagsIds[] = $userToTagsCollection[$i][Constants::COLUMN_TAG_ID];
        }

        $tags = [];
        $tagsResult = $tagsFactory->create()->getCollection()->addFieldToFilter(Constants::COLUMN_TAG_ID, ['in' => $tagsIds])->getData();
        foreach ($tagsResult as $tag) {
            $tagsCount = $tagToProductFactory->create()
                ->getCollection()
                ->addFieldToFilter(Constants::COLUMN_TAG_ID, ['eq' => $tag[Constants::COLUMN_TAG_ID]])
                ->getSize();

            $tags[] = [
                'id'                => $tag[Constants::COLUMN_TAG_ID],
                'title'             => $tag[Constants::COLUMN_VALUE],
                'products_count'    => $tagsCount,
                'subscribed'        => 1
            ];
        }

        return $tags;
    }

    private function addProductImage()
    {
        $productId = $this->product_id;
        $product = $this->context->getObjectManager()->create('Magento\Catalog\Model\ProductFactory')->create();
        $product->load($productId);
        $logPath = '/home/karavanium/public_html/images.log';
        file_put_contents($logPath, "\r\n------------------------- FSM (START) -------------------------\r\n", FILE_APPEND);

        if (!empty($_FILES)) {
            try {
                /** @var \Magento\Framework\Filesystem\Directory\ReadInterface $mediaDirectory */
                $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')->getDirectoryRead(DirectoryList::MEDIA);
            } catch (FileSystemException $e) {
                return [Responses::ERROR => $e->getMessage()];
            }

            try {
                $uploader = $this->_objectManager
                    ->create('Magento\MediaStorage\Model\File\Uploader', ['fileId' => 'image']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploader->setAllowCreateFolders(true);
                $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
                $uploader->addValidateCallback('image', $imageAdapter, 'validateUploadFile');
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);

                if (!$uploader->checkAllowedExtension($uploader->getFileExtension())) {
                    return Responses::ERROR_INCORRECT_IMAGE_TYPE;
                }

                try {
                    $pathToImages = $mediaDirectory->getAbsolutePath('product_images');
                    file_put_contents($logPath, "\r\n$pathToImages\r\n", FILE_APPEND);
                    $result = $uploader->save($pathToImages);
                    if (!$result) {
                        return Responses::ERROR_IMAGE_CANNOT_BE_SAVE;
                    }

                    $imgpath = $this->_prepareFile($result['file']);
                    $imgpathArray = explode('/', $imgpath);
                    unset($imgpathArray[count($imgpathArray) - 1]);
                    $imgpath = implode('/', $imgpathArray);
                    $this->chmod_r($result['path'] . '/' . $imgpath);
                    chmod($result['path'] . '/' . $this->_prepareFile($result['file']), 0777);
                    unset($result['tmp_name'], $result['path']);
                    $result['file'] = $result['file'];

                    file_put_contents($logPath, "\r\n" . var_export($result, true) . "\r\n", FILE_APPEND);
                    $imageName = empty($uploader->getUploadedFileName())
                        ? ''
                        : $uploader->getUploadedFileName();
                    file_put_contents($logPath, "\r\n$imageName\r\n", FILE_APPEND);
                    if (!empty($imageName)) {
                        $getMediaItems = $product->getMediaGalleryImages();
                        $imageType = [];
                        if (count($getMediaItems) < 1) {
                            $imageType = ['image'];
                        }
                        $imagePath = rtrim($pathToImages, '/') . '/' . ltrim($imageName, '/');
                        $product->addImageToMediaGallery($imagePath, array('image', 'small_image', 'thumbnail'),
                            true, false);
                    } else {
                        file_put_contents($logPath, "\r\n---\r\n", FILE_APPEND);
                        return Responses::ERROR_IMAGE_CANNOT_BE_SAVE;
                    }

                    if ($this->isProductAvailable($product) && $this->checkProductIsNew($product)) {
                        $this->sendNewProductToFirestore($product);
                        $this->unsetProductIsNew($product);
                    }

                    $product->save();
                } catch (\Exception $e) {
                    $em = $e->getMessage();
                    file_put_contents($logPath, "\r\n$em\r\n", FILE_APPEND);
                    return [Responses::ERROR => $e->getMessage()];
                }

            } catch (\Exception $e) {
                $em = $e->getMessage();
                file_put_contents($logPath, "\r\n$em\r\n", FILE_APPEND);
                return Responses::ERROR_IMAGE_CANNOT_BE_SAVE;
            }

            $product = $this->context->getObjectManager()->create('Magento\Catalog\Model\ProductFactory')->create();
            $product->load($productId);
            if ($product->getImage() == 'no_selection') {
                $productImages = $product->getMediaGalleryImages();
                $product->setImage($productImages->getLastItem()->getFile());

                if ($this->isProductAvailable($product) && $this->checkProductIsNew($product)) {
                    $this->sendNewProductToFirestore($product);
                    $this->unsetProductIsNew($product);
                }

                try {
                    $product->save();
                } catch (\Exception $e) {
                    return [Responses::ERROR => $e->getMessage()];
                }
            }
        }

        return [
            Responses::SUCCESS  => 1,
            'image'             => $this->getLastImageByProduct($productId)
        ];
    }

    private function chmod_r($path)
    {

        $dir = new \DirectoryIterator($path);
        foreach ($dir as $item) {
            chmod($item->getPathname(), 0777);
            if ($item->isDir() && !$item->isDot()) {
                chmod_r($item->getPathname());
            }
        }
    }

    private function _prepareFile($file)
    {
        return ltrim(str_replace('\\', '/', $file), '/');
    }

    private function getTagsByText()
    {
        $tagsFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\TagsFactory');

        $tags = [];
        $tagsResult = $tagsFactory->create()
            ->getCollection()
            ->addFieldToFilter(Constants::COLUMN_VALUE, ['like' => '%' . $this->text . '%'])
            ->getData();
        foreach ($tagsResult as $tag) {
            $tags[] = [
                'id' => $tag[Constants::COLUMN_TAG_ID],
                'title' => $tag[Constants::COLUMN_VALUE]
            ];
        }

        return [Responses::TAGS => $tags];
    }

    private function deleteProduct()
    {
        $productId = (int) $this->product_id;
        if ($productId <= 0) {
            return [Responses::ERROR => 'incorrect_product_id'];
        }

        try {
            $registry = $this->context->getObjectManager()->get('\Magento\Framework\Registry');
            $registry->register('isSecureArea', true);
            $product = $this->productRepository->getById($productId);
            $this->productRepository->deleteById($product->getSku());
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        return [
            Responses::KEY_PRODUCTS_COUNT => $this->vendorProductCount(),
            Responses::KEY_PRODUCT => [Responses::KEY_PRODUCT_ID => $productId]
        ];
    }

    private function vendorProductCount()
    {
        $filterOne = $this->filterBuilder
            ->setField(Constants::ATTRIBUTE_KARAVANIUM_USER_ID)
            ->setConditionType('eq')
            ->setValue($this->user_id)
            ->create();

        $filterGroupOne = $this->filterGroupBuilder->addFilter($filterOne)->create();

        $this->searchCriteriaBuilder->setFilterGroups([$filterGroupOne]);
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $product = $this->productRepositoryInterface->getList($searchCriteria);

        return $product->getTotalCount();
    }

    private function vendorDetailsProductCount()
    {
        $filterOne = $this->filterBuilder
            ->setField(Constants::ATTRIBUTE_KARAVANIUM_USER_ID)
            ->setConditionType('eq')
            ->setValue($this->vendor_id)
            ->create();
        $filterTwo = $this->filterBuilder
            ->setField(Constants::COLUMN_STATUS)
            ->setConditionType('eq')
            ->setValue(ProductStatus::STATUS_ENABLED)
            ->create();

        $filterGroupOne = $this->filterGroupBuilder->addFilter($filterOne)->create();
        $filterGroupTwo = $this->filterGroupBuilder->addFilter($filterTwo)->create();

        $this->searchCriteriaBuilder->setFilterGroups([$filterGroupOne, $filterGroupTwo]);
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $product = $this->productRepositoryInterface->getList($searchCriteria);

        return $product->getTotalCount();
    }

    private function searchVendorProductCount(array $filterGroups)
    {
        $this->searchCriteriaBuilder->setFilterGroups($filterGroups);
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $product = $this->productRepositoryInterface->getList($searchCriteria);

        return $product->getTotalCount();
    }

    private function disableProduct()
    {
        return $this->changeProductStatus(ProductStatus::STATUS_DISABLED);
    }

    private function enableProduct()
    {
        return $this->changeProductStatus(ProductStatus::STATUS_ENABLED);
    }

    private function changeProductStatus($status)
    {
        $productId = (int) $this->product_id;
        if ($productId <= 0) {
            return [Responses::ERROR => 'incorrect_product_id'];
        }

        try {
            $product = $this->productRepositoryInterface->getById($productId, true, 0);
            $product->setStatus(!empty($status) ? $status : $product->getStatus());
            $this->productRepositoryInterface->save($product);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        return $this->getVendorProduct();
    }

    private function getVendorProduct()
    {
        $productsCollection = Tools::getProductModel($this->context->getObjectManager())->getCollection();

        /** @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $productsCollection */
        $productsCollection->getSelect(
        )->joinLeft(
            ['main_name' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'main_name.entity_id = e.entity_id AND main_name.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'name' AND entity_type_id = 4 LIMIT 1) AND main_name.store_id = 0",
            ['main_name.value AS name']
        )->joinLeft(
            ['main_description' => Tools::getDbTablePrefix() . 'catalog_product_entity_text'],
            'main_description.entity_id = e.entity_id AND main_description.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'description' AND entity_type_id = 4 LIMIT 1) AND main_description.store_id = 0",
            ['main_description.value AS description']
        )->joinLeft(
            ['main_price' => Tools::getDbTablePrefix() . 'catalog_product_entity_decimal'],
            'main_price.entity_id = e.entity_id AND main_price.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'price' AND entity_type_id = 4 LIMIT 1) AND main_price.store_id = 0",
            ['main_price.value AS price']
        )->joinLeft(
            ['cust_user_id' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'cust_user_id.entity_id = e.entity_id AND cust_user_id.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_user_id' LIMIT 1) AND cust_user_id.store_id = 0", []
        )->joinLeft(
            ['cust_sku' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'cust_sku.entity_id = e.entity_id AND cust_sku.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_sku' LIMIT 1) AND cust_sku.store_id = 0",
            ['cust_sku.value AS custom_sku']
        )->where(
            'e.entity_id = ?', $this->product_id
        )->where(
            'cust_user_id.value = ?', $this->user_id
        );

        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        } catch (FileSystemException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
        $vendorProduct = $productsCollection->getConnection()->fetchAll($productsCollection->getSelectSql());
        $stockState = $this->context->getObjectManager()->create('Magento\CatalogInventory\Api\StockRegistryInterface');

        try {
            $product = $this->productRepositoryInterface->getById($this->product_id);
            $baseImageId = $this->getBaseImageId($product->getMediaGalleryEntries());
            $mediaGallery = $this->getMediaGalleryByProduct($product->getId(), $product->getMediaGalleryImages()->getItems(), $baseImageId, $mediaDirectory);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_PRODUCT;
        }

        $changedProduct = [];
        for ($i = 0, $vendorProductCount = count($vendorProduct); $i < $vendorProductCount; $i++) {
            $stock = $stockState->getStockItem($vendorProduct[$i]['entity_id'], 0);
            $changedProduct = [
                Responses::KEY_PRODUCT_ID       => $vendorProduct[$i]['entity_id'],
                Responses::KEY_SKU              => $vendorProduct[$i]['custom_sku'],
                Responses::KEY_NAME             => $vendorProduct[$i][Responses::KEY_NAME],
                Responses::KEY_DESCRIPTION      => $vendorProduct[$i][Responses::KEY_DESCRIPTION],
                Responses::KEY_IMAGES           => $mediaGallery,
                Responses::KEY_STATUS           => ($product->getStatus() == ProductStatus::STATUS_ENABLED) ? 1 : 0,
                Responses::KEY_QTY              => (int) $stock->getQty(),
                Responses::KEY_PRICE            => $price = (double)$vendorProduct[$i]['price'],
                Responses::KEY_FORMATTED_PRICE  => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code)
            ];

        }

        return [Responses::KEY_PRODUCT => $changedProduct];
    }

    private function getVendors()
    {
        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $favoriteVendors = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
        $favoriteVendorsValue = empty($favoriteVendors) ? [] : explode(',', $favoriteVendors->getValue());

        $vendorsCollection = Tools::getCustomerModel($this->context->getObjectManager())
            ->getCollection()
            ->addAttributeToSelect(
                [
                    'karavanium_role',
                    'karavanium_avatar',
                    'karavanium_rating',
                    'karavanium_vendor_name'
                ]
            )->addAttributeToFilter('karavanium_role', ['like' => '%vendor%']);

        $vendorsCollection->getSelect()->limit(
            Constants::VENDOR_PAGE_LIMIT, ($this->page - 1) * Constants::VENDOR_PAGE_LIMIT
        );
        $vendorsCount = $vendorsCollection->getSize();

        $vendors = [];
        foreach ($vendorsCollection as $vendor) {
            try {
                $vendor->load($vendor->getId());
            } catch (\Exception $e) {
                return Responses::ERROR . ': ' . $e->getMessage();
            };

            $vendors[] = [
                Responses::KEY_VENDOR_ID => $vendor->getId(),
                Responses::KEY_VENDOR_NAME => $vendor->getKaravaniumVendorName(),
                Responses::KEY_VENDOR_AVATAR => !empty($vendor->getKaravaniumAvatar())
                    ? $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $vendor->getKaravaniumAvatar()
                    : '',
                Responses::KEY_VENDOR_RATING => empty($vendor->getKaravaniumRating()) ? 0 : $vendor->getKaravaniumRating(),
                Responses::KEY_VENDOR_SUBSCRIBED => in_array($vendor->getId(), $favoriteVendorsValue) ? 1 : 0,

            ];
        }

        return [
            Responses::KEY_VENDORS_COUNT => $vendorsCount,
            Responses::KEY_VENDORS => $vendors,
        ];
    }

    private function getSubscribedVendors()
    {
        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $favoriteVendors = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
        $favoriteVendorsValue = empty($favoriteVendors) ? [] : explode(',', $favoriteVendors->getValue());
        if (empty($favoriteVendorsValue)) {
            return [
                Responses::KEY_VENDORS_COUNT => 0,
                Responses::KEY_VENDORS => [],
            ];
        }

        $vendorsCollection = Tools::getCustomerModel($this->context->getObjectManager())->getCollection()->addAttributeToSelect(
            [
                'karavanium_role',
                'karavanium_avatar',
                'karavanium_rating',
                'karavanium_vendor_name'
            ]
        )->addAttributeToFilter('karavanium_role', ['like' => '%vendor%']);

        $vendorsCollection->getSelect()
            ->where('e.entity_id IN (' . implode(',', $favoriteVendorsValue) . ')')
            ->order('e.entity_id DESC');

        $vendorsCount = $vendorsCollection->getSize();

        $vendors = [];
        foreach ($vendorsCollection as $vendor) {
            try {
                $vendor->load($vendor->getId());
            } catch (\Exception $e) {
                return Responses::ERROR . ': ' . $e->getMessage();
            }

            $vendors[] = $this->vendorObjectResponse($vendor, $favoriteVendorsValue);
        }

        return [
            Responses::KEY_VENDORS_COUNT => $vendorsCount,
            Responses::KEY_VENDORS => $vendors,
        ];
    }

    private function vendorObjectResponse($vendor = null, $favoriteVendorsValue = [])
    {
        return [
            Responses::KEY_VENDOR_ID => empty($vendor) ? '' : $vendor->getId(),
            Responses::KEY_VENDOR_NAME => empty($vendor) ? '' : $vendor->getKaravaniumVendorName(),
            Responses::KEY_VENDOR_AVATAR => empty($vendor) ? '' : (!empty($vendor->getKaravaniumAvatar())
                ? $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $vendor->getKaravaniumAvatar() : ''),
            Responses::KEY_VENDOR_RATING => empty($vendor)
                ? 0 : (empty($vendor->getKaravaniumRating()) ? 0 : $vendor->getKaravaniumRating()),
            Responses::KEY_VENDOR_SUBSCRIBED => empty($vendor)
                ? 0 : (in_array($vendor->getId(), $favoriteVendorsValue) ? 1 : 0),
        ];
    }

    private function searchVendors()
    {
        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $favoriteVendors = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
        $favoriteVendorsValue = empty($favoriteVendors) ? [] : explode(',', $favoriteVendors->getValue());

        $vendorsCollection = Tools::getCustomerModel($this->context->getObjectManager())->getCollection()->addAttributeToSelect(
            [
                'karavanium_role',
                'karavanium_avatar',
                'karavanium_rating',
                'karavanium_vendor_name'
            ]
        )->addAttributeToFilter('karavanium_role', ['like' => '%vendor%']);

        $vendorsCollection->getSelect()
            ->order('e.entity_id DESC')
            ->limit(Constants::VENDOR_PAGE_LIMIT, ($this->page - 1) * Constants::VENDOR_PAGE_LIMIT);

        $vendorsCount = $vendorsCollection->getSize();

        $vendors = [];
        foreach ($vendorsCollection as $vendor) {
            try {
                $vendor->load($vendor->getId());
            } catch (\Exception $e) {
                return Responses::ERROR . ': ' . $e->getMessage();
            }

            $vendors[] = [
                Responses::KEY_VENDOR_ID => $vendor->getId(),
                Responses::KEY_VENDOR_NAME => $vendor->getKaravaniumVendorName(),
                Responses::KEY_VENDOR_AVATAR => !empty($vendor->getKaravaniumAvatar())
                    ? $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $vendor->getKaravaniumAvatar()
                    : '',
                Responses::KEY_VENDOR_RATING => empty($vendor->getKaravaniumRating()) ? 0 : $vendor->getKaravaniumRating(),
                Responses::KEY_VENDOR_SUBSCRIBED => in_array($vendor->getId(), $favoriteVendorsValue) ? 1 : 0,
            ];
        }

        return [
            Responses::KEY_VENDORS_COUNT => $vendorsCount,
            Responses::KEY_VENDORS => $vendors,
        ];
    }

    //Cart/Order
    private function getCartProducts($isDeleted = false)
    {
        $items = [];
        if ($isDeleted == true) {
            $summary = $this->getCartSummary($this->user_id);
            $summary['items'] = $items;

            return $summary;
        }

        try {
            $cart = $this->cartManagementInterface->getCartForCustomer($this->user_id);
            $itemModel = $this->context->getObjectManager()->create('Magento\Quote\Model\Quote\Item')
                ->getCollection()
                ->addFieldToFilter('quote_id', ['eq' => $cart->getId()]);
        } catch (\Exception $e) {
            $summary = $this->getCartSummary($this->user_id);
            $summary['items'] = $items;

            return $summary;
        }
        $mediaDirectory = '';
        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        } catch (FileSystemException $e) {}

        try {
            $itemModel->getSelect()->order('item_id DESC');
            $itemModels = $itemModel->getData();
            foreach ($itemModels as $item) {
                $product = $this->productRepository->getById($item['product_id']);
                try {
                    $baseImageId = $this->getBaseImageId($product->getMediaGalleryEntries());
                    $mediaGallery = $this->getMediaGalleryByProduct($product->getId(), $product->getMediaGalleryImages()->getItems(), $baseImageId, $mediaDirectory);
                } catch (\Exception $e) {
                    return Responses::ERROR_NO_SUCH_PRODUCT;
                }
                $items[] = [
                    'cart_item_id'          => $item['item_id'],
                    'product_id'            => $item['product_id'],
                    'name'                  => $item['name'],
                    'images'                => !empty($mediaGallery) ? $mediaGallery : [],
                    'qty_ordered'           => (int) $item['qty'],
                    'price'                 => $price = empty($item['price']) ? 0 : $item['price'],
                    'formatted_price'       => Tools::getConvertedAndFormattedPrice(
                        $price, $this->def_currency, $this->currency_code
                    ),
                    'formatted_price_total' => Tools::getConvertedAndFormattedPrice(
                        $item['row_total'], $this->def_currency, $this->currency_code
                    ),
                    'vendor_id'             => $product->getKaravaniumUserId()
                ];
            }
        } catch (\Exception $e) {}

        $summary = $this->getCartSummary($this->user_id, $cart);
        $summary['items'] = $items;

        return $summary;
    }

    private function addProductToCart()
    {
        if (!$this->product_id) {
            return Responses::ERROR_NO_PRODUCT_DATA;
        }

        $this->qty = 1;
        $result = $this->changeCartProductQty();
        $result['product'] = $this->getProduct($this->product_id);

        return $result;
    }

    private function changeCartProductQty()
    {
        if (!$this->product_id || !$this->qty) {
            return Responses::ERROR_NO_PRODUCT_DATA;
        }
        $data = ['product_id' => $this->product_id, 'qty' => $this->qty];

        try {
            $cart = $this->cartRepositoryInterface->getActiveForCustomer($this->user_id);
        } catch (\Exception $e) {}

        if (empty($cart)) {
            try {
                $store = $this->storeManagerInterface->getStore();

                $customer = $this->customerRepositoryInterface->getById($this->user_id);
                $cartId = $this->cartManagementInterface->createEmptyCart();
                $this->cartManagementInterface->assignCustomer($cartId, $customer->getId(), $store->getId());
                $cart = $this->cartManagementInterface->getCartForCustomer($this->user_id);
            } catch (\Exception $e) {
                return [Responses::ERROR => $e->getMessage()];
            }
        }

        try {
            $product = $this->productRepositoryInterface->getById($data['product_id']);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_PRODUCT;
        }

        try {
            $items = $cart->getItemsCollection();
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $wasItemInCart = false;

        if (!empty($items) && $items->getSize() > 0) {
            foreach ($items as $item) {
                if ($item->getQty() < 1) {
                    return [Responses::ERROR => 'qty_lower_than_zero'];
                }

                if ($item->getProductId() != $data['product_id']) {
                    continue;
                }

                $qty = $item->getQty() + $data['qty'];
                if ($qty < 1) {
                    $qty = 1;
                }

                $item->setQty($qty);

                $wasItemInCart = true;

                break;
            }
        }

        if (!$wasItemInCart) {
            $objParam = new \Magento\Framework\DataObject();
            $objParam->setData($data);

            try {
                if (!is_object($test = $cart->addProduct($product, $objParam))) {
                    return Responses::ERROR_PRODUCT_NOT_ADDED;
                }
            } catch (\Exception $e) {

                return [Responses::ERROR => $e->getMessage()];
            }
        }

        $cart->collectTotals();
        $this->cartRepositoryInterface->save($cart);

        $summary = $this->getCartSummary($this->user_id, $cart);
        $summary['item'] = $this->getItems();

        return $summary;
    }

    private function removeCartProducts()
    {
        if (empty($this->product_ids) && empty($this->all)) {
            return Responses::ERROR_NO_PRODUCT_DATA;
        }

        try {
            $cart = $this->cartRepositoryInterface->getActiveForCustomer($this->user_id);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_CART;
        }

        if (!$this->product_ids && $this->all == 1) {
            $cart->removeAllItems()->delete();
            $cart->collectTotals();

            return $this->getCartProducts($cart->isDeleted());

        }

        $productIds = explode(',', $this->product_ids);
        try {
            $quoteItems = $cart->getAllVisibleItems();
            foreach ($quoteItems as $item) {
                if (in_array($item->getProductId(), $productIds)) {
                    $cart->removeItem($item->getItemId());
                }
            }

            $cart->collectTotals();
            $this->cartRepositoryInterface->save($cart);
        } catch (\Exception $e) {
            return $e;
        }

        return array_merge([Responses::SUCCESS => 1], $this->getCartSummary($this->user_id));
    }

    private function getShippingMethodsByAddress()
    {
        try {
            $cart = $this->cartRepositoryInterface->getForCustomer($this->user_id);
        } catch (\Exception $e) {
            return [Responses::ERROR => \Zend_Debug::dump($e->getMessage())];
        }

        $carriers = [];
        if (!empty($this->data)) {
            $address = null;
            try {
                $address = Tools::jsonDecode($this->data);
            } catch (\Exception $e) {
                return [Responses::ERROR => \Zend_Debug::dump($e->getMessage())];
            }

            if ($address && is_array($address)) {
                if ($address['address_id'] > 0) {
                    $availableCarriers = $this->shippingMethodManagementInterface->estimateByAddressId($cart->getId(), $address['address_id']);
                } else {
                    /** @var \Magento\Quote\Api\Data\EstimateAddressInterface $estimatedAddress */
                    $estimatedAddress = $this->estimatedAddressFactory->create();
                    $estimatedAddress->setCountryId($address['country_id']);
                    $estimatedAddress->setPostcode($address['postcode']);
                    array_key_exists('region', $address)
                        ? $estimatedAddress->setRegion($address['region'])
                        : null;
                    array_key_exists('region_id', $address)
                        ? $estimatedAddress->setRegionId($address['region_id'])
                        : (!empty($regionId = $this->getRegionId($address['country_id'], $address['region_code']))
                        ? $estimatedAddress->setRegionId($regionId)
                        : null);

                    $availableCarriers = $this->shippingMethodManagementInterface->estimateByAddress($cart->getId(), $estimatedAddress);
                }

                $methods = [];
                foreach ($availableCarriers as $carrier) {
                    $carrierCode = $carrier->getCarrierCode();
                    $methods[$carrierCode]['methods'][] = [
                        'method_title' => is_object($methodTitle = $carrier->getMethodTitle()) ? $methodTitle->getText() : $methodTitle,
                        'method_code' => $carrier->getMethodCode(),
                        'price' => Tools::getConvertedAndFormattedPrice(
                            (int) $carrier->getPriceExclTax(), $this->def_currency, $this->currency_code
                        )
                    ];

                    if (!array_key_exists($carrierCode, $carriers)) {
                        $carriers[$carrierCode] = [
                            'carrier_code' => $carrierCode,
                            'carrier_title' => $carrier->getCarrierTitle(),
                            'method_code' => $carrier->getMethodCode(),
                            'price' => Tools::getConvertedAndFormattedPrice(
                                (int) $carrier->getPriceExclTax(), $this->def_currency, $this->currency_code
                            )
                        ];
                    }

                    $methodsListY = [];
                    if (array_key_exists($carrierCode, $methods)) {
                        $methodsListY = $methods[$carrierCode]['methods'];
                    }
                    $carriers[$carrierCode]['methods'] = count($methodsListY) > 1 ? $methodsListY : [];
                }
            }
        }

        return [
            'shipping_methods_count'    => count($carriers),
            'shipping_methods'          => array_values($carriers)
        ];
    }

    private function getCheckoutTotal()
    {
        if (!empty($this->shipping_address)) {

            if (!is_array($this->shipping_address)) {
                try {
                    $shippingAddressData = Tools::jsonDecode(htmlspecialchars_decode($this->shipping_address));
                } catch (\Exception $e) {
                    return [Responses::ERROR => \Zend_Debug::dump($e->getMessage())];
                }
            } else {
                $shippingAddressData = $this->shipping_address;
            }

            try {
                $cart = $this->cartRepositoryInterface->getForCustomer($this->user_id);
            } catch (\Exception $e) {
                return [Responses::ERROR => \Zend_Debug::dump($e->getMessage())];
            }

            $needleShippingArray = [
                'firstname',
                'lastname',
                'streets',
                'city',
                'postcode',
                'telephone',
                'fax',
                'country_id',
                'region',
                'region_code',
                'region_id',
                'save_in_address_book'
            ];

            $shippingAddressDataPrepared = [];
            for ($i = 0, $shippingCount = count($needleShippingArray); $i < $shippingCount; $i++) {
                if (array_key_exists($needleShippingArray[$i], $shippingAddressData)) {
                    $shippingAddressDataPrepared[$needleShippingArray[$i]] = $shippingAddressData[$needleShippingArray[$i]];
                }
            }

            if (count($shippingAddressDataPrepared) < 4) {
                return [Responses::ERROR => 'some_data_not_exist'];
            }

            $shippingAddressDataStreet = '';
            if (is_array($shippingAddressDataPrepared['streets'])) {
                for ($i = 0, $count = count($shippingAddressDataPrepared['streets']); $i < $count; $i++) {
                    $shippingAddressDataStreet .= $shippingAddressDataPrepared['streets'][$i]['name'];
                    if ($count > 1) {
                        $shippingAddressDataStreet .= ' ';
                    }
                }

                $shippingAddressDataPrepared['street'] = $shippingAddressDataStreet;
            }
            unset($shippingAddressDataPrepared['streets']);

            array_key_exists('region_code', $shippingAddressDataPrepared)
                ? !empty($regionId = $this->getRegionId(
                $shippingAddressDataPrepared['country_id'],
                $shippingAddressDataPrepared['region_code']
            )
            )
                ? $shippingAddressDataPrepared['region_id'] = $regionId
                : null
                : null;

            $shippingAddressDataPrepared['save_in_address_book'] = 1;
            try {
                $cart->getBillingAddress()->addData($shippingAddressDataPrepared);
                $cart->getShippingAddress()->addData($shippingAddressDataPrepared);
            } catch (\Exception $e) {
                return [Responses::ERROR => $e->getMessage()];
            }

            $shippingRate = $this->shippingRate->setCode('cargoshipping_cargoshipping');
            $shippingRate->getPrice(1);

            $cart->getShippingAddress()->setCollectShippingRates(true);
            $cart->getShippingAddress()->collectShippingRates();
            $cart->getShippingAddress()->setShippingMethod('cargoshipping_cargoshipping');
            $cart->getShippingAddress()->setPaymentMethod('checkmo');
            $cart->getShippingAddress()->addShippingRate($shippingRate);
            $cart->getPayment()->setMethod('checkmo');
            $cart->setPaymentMethod('checkmo');
            $cart->setInventoryProcessed(false);
            $cart->getPayment()->importData(['method' => 'checkmo']);
            $cart->collectTotals();

            try {
                $cart->save();
            } catch (\Exception $e) {
                return [Responses::ERROR => \Zend_Debug::dump($e->getMessage())];
            }
        }

        try {
            $cart = $this->cartRepositoryInterface->getActiveForCustomer($this->user_id);
        } catch (NoSuchEntityException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
        $shippingAddress = $cart->getShippingAddress();
        if (empty($shippingAddress)) {
            return [Responses::ERROR => 'no_default_address'];
        }

        return array_merge(
            $this->getCartSummary($this->user_id),
            [
                'subtotal' => Tools::getConvertedAndFormattedPrice(
                    (double) $cart->getSubtotal(), $this->def_currency, $this->currency_code
                ),
                'discount' => Tools::getConvertedAndFormattedPrice(
                    (double) $shippingAddress->getDiscountAmount(), $this->def_currency, $this->currency_code
                ),
                'shipping' => Tools::getConvertedAndFormattedPrice(
                    (double) $shippingAddress->getShippingAmount(), $this->def_currency, $this->currency_code
                ),
                'item_groups' => $this->getCartItemsByVendors()
            ]);

    }

    private function placeOrder()
    {
        $defaultShippingAddress = [];
        if (!empty($this->address)) {
            $defaultShippingAddress = Tools::jsonDecode(htmlspecialchars_decode($this->address));
        }

        if (empty($defaultShippingAddress)) {
            $allAddresses = $this->getProfileAddresses();
            for ($i = 0, $allAddressesCount = count($allAddresses[Responses::KEY_ADDRESSES]); $i < $allAddressesCount; $i++) {
                if ($allAddresses[Responses::KEY_ADDRESSES][$i][Responses::KEY_IS_DEFAULT_SHIPPING] != 1) {
                    continue;
                }
                $defaultShippingAddress = $allAddresses[Responses::KEY_ADDRESSES][$i];
            }

            if (empty($defaultShippingAddress)) {
                try {
                    $cart = $this->cartRepositoryInterface->getActiveForCustomer($this->user_id);
                } catch (NoSuchEntityException $e) {
                    return [Responses::ERROR => $e->getMessage()];
                }

                if (empty($shippingAddress = $cart->getShippingAddress()) || empty($this->getDefaultShippingAddress())) {
                    return array_merge([Responses::ERROR => 'no_address'], $this->getProfileNewAddressData());
                }
            }
        }

        if (!empty($defaultShippingAddress)) {
            try {
                $cart = $this->cartRepositoryInterface->getForCustomer($this->user_id);
            } catch (\Exception $e) {
                return [Responses::ERROR => \Zend_Debug::dump($e->getMessage())];
            }

            $needleShippingArray = [
                'firstname',
                'lastname',
                'streets',
                'city',
                'postcode',
                'telephone',
                'fax',
                'country_id',
                'region',
                'region_code',
                'region_id',
                'save_in_address_book'
            ];

            $shippingAddressDataPrepared = [];
            for ($i = 0, $shippingCount = count($needleShippingArray); $i < $shippingCount; $i++) {
                if (array_key_exists($needleShippingArray[$i], $defaultShippingAddress)) {
                    $shippingAddressDataPrepared[$needleShippingArray[$i]] = $defaultShippingAddress[$needleShippingArray[$i]];
                }
            }

            if (count($shippingAddressDataPrepared) < 4) {
                return [Responses::ERROR => 'some_data_not_exist'];
            }

            $shippingAddressDataStreet = '';
            if (is_array($shippingAddressDataPrepared['streets'])) {
                for ($i = 0, $count = count($shippingAddressDataPrepared['streets']); $i < $count; $i++) {
                    $shippingAddressDataStreet .= $shippingAddressDataPrepared['streets'][$i]['name'];
                    if ($count > 1) {
                        $shippingAddressDataStreet .= ' ';
                    }
                }

                $shippingAddressDataPrepared['street'] = $shippingAddressDataStreet;
            }
            unset($shippingAddressDataPrepared['streets']);

            array_key_exists('region_code', $shippingAddressDataPrepared) ? !empty(
            $regionId = $this->getRegionId(
                $shippingAddressDataPrepared['country_id'], $shippingAddressDataPrepared['region_code']
            )
            ) ? $shippingAddressDataPrepared['region_id'] = $regionId : null : null;
            $shippingAddressDataPrepared['save_in_address_book'] = 1;


            try {
                $cart->getBillingAddress()->addData($shippingAddressDataPrepared);
                $cart->getShippingAddress()->addData($shippingAddressDataPrepared);
            } catch (\Exception $e) {
                return [Responses::ERROR => $e->getMessage()];
            }

            $shippingRate = $this->shippingRate->setCode('cargoshipping_cargoshipping');
            $shippingRate->getPrice(1);

            $cart->getShippingAddress()->setCollectShippingRates(true);
            $cart->getShippingAddress()->collectShippingRates();
            $cart->getShippingAddress()->setShippingMethod('cargoshipping_cargoshipping');
            $cart->getShippingAddress()->setPaymentMethod('checkmo');
            $cart->getShippingAddress()->addShippingRate($shippingRate);
            $cart->getPayment()->setMethod('checkmo');
            $cart->setPaymentMethod('checkmo');
            $cart->setInventoryProcessed(false);
            $cart->getPayment()->importData(['method' => 'checkmo']);
            $cart->collectTotals();

            try {
                $cart->save();
            } catch (\Exception $e) {
                return [Responses::ERROR => \Zend_Debug::dump($e->getMessage())];
            }
        }
        $this->autoCheckFirstAddress();
        if (count($orders = $this->createNewCartByVendor()) > 0 && !array_key_exists('error', $orders)) {
            $ordersIds = [];
            for ($k = 0, $countOrders = count($orders); $k < $countOrders; $k++) {
                $ordersIds[] = $orders[$k][Responses::KEY_ORDER_ID];
            }
            return array_merge($this->getOrdersClientRe(implode(',', $ordersIds)), $this->getCartSummary($this->user_id));
        }

        return array_key_exists('error', $orders) ? $orders : [];
    }

    private function createOrder()
    {
        $result = [];
        try {
            $preparedCart = $this->cartRepositoryInterface->getActiveForCustomer($this->user_id);
            $placedOrder = $this->cartManagementInterface->placeOrder($preparedCart->getId());
            $order = $this->context->getObjectManager()->create('Magento\Sales\Model\Order')->load($placedOrder);
            $order->addStatusHistoryComment('This comment is programatically added to last order in this Magento setup');
            $order->save();
            $result['order_id'] = $order->getEntityId();
            $result['order_number'] = $order->getRealOrderId();

        } catch (\Exception $e) {
            return [Responses::ERROR => \Zend_Debug::dump($e->getMessage())];
        }

        return $result;
    }

    private function getCartItemsByVendors()
    {
        try {
            $basedCart = $this->cartRepositoryInterface->getActiveForCustomer($this->user_id);
        } catch (NoSuchEntityException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $basedItems = array_reverse($basedCart->getAllItems());
        $vendorList = [];
        $vendorsItemList = [];
        foreach ($basedItems as $item) {
            $productInformation = $this->getProduct($item->getProductId());
            $vendorId = $productInformation[Responses::KEY_USER_ID];
            $vendorName = $productInformation[Responses::KEY_VENDOR_NAME];

            $vendorList[$vendorId] = [
                Responses::KEY_VENDOR_ID => $vendorId,
                Responses::KEY_VENDOR_NAME => $vendorName
            ];

            $vendorsItemList[$vendorId]['items'][] = [
                'cart_item_id' => $item->getItemId(),
                Responses::KEY_PRODUCT_ID => $item->getProductId(),
                Responses::KEY_VENDOR_ID => $vendorId,
                Responses::KEY_NAME => $item->getName(),
                Responses::KEY_QTY_ORDERED => (int) $item->getQty(),
                Responses::KEY_PRICE => $price = empty($item->getPrice()) ? 0 : $item->getPrice(),
                Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                Responses::KEY_IMAGE_URL => !empty($productInformation[Responses::KEY_IMAGES]) ? $productInformation[Responses::KEY_IMAGES][0][Responses::KEY_URL] : '',
                Responses::KEY_PRICE_TOTAL => $priceTotal = empty($item->getRowTotal()) ? 0 : $item->getRowTotal(),
                Responses::KEY_FORMATTED_PRICE_TOTAL => Tools::getConvertedAndFormattedPrice($priceTotal, $this->def_currency, $this->currency_code),
            ];

            $vendorList[$vendorId]['items'] = $vendorsItemList[$vendorId]['items'];
        }

        return array_values($vendorList);
    }

    private function createNewCartByVendor()
    {
        $preparedOrdersByVendors = $this->prepareVendorCart();
        $this->disableActiveCart();

        $orders = [];
        for ($i = 0, $orderCount = count($preparedOrdersByVendors); $i < $orderCount; $i++) {
            $vendorItems = $preparedOrdersByVendors[$i][Responses::KEY_ITEMS];
            for ($k = 0, $itemCount = count($vendorItems); $k < $itemCount; $k++) {
                $this->product_id = $vendorItems[$k][Responses::KEY_PRODUCT_ID];
                $this->qty = (int) $vendorItems[$k][Responses::KEY_QTY];
                $this->changeCartProductQty();
            }
            $vendorShippingAddress = $preparedOrdersByVendors[$i][Responses::KEY_SHIPPING_ADDRESS];
            $this->shipping_address = $vendorShippingAddress;
            $this->getCheckoutTotal();

            $result = [];
            try {
                $preparedCart = $this->cartRepositoryInterface->getActiveForCustomer($this->user_id);
                $placedOrder = $this->cartManagementInterface->placeOrder($preparedCart->getId());
                $order = $this->context->getObjectManager()->create('Magento\Sales\Model\Order')->load($placedOrder);
                $order->save();
                $vendorOrders = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\VendorOrdersFactory')->create();
                $vendorOrders->setData(
                    [
                        Constants::COLUMN_VENDOR_ID => (int) $preparedOrdersByVendors[$i][Responses::KEY_VENDOR_ID],
                        Constants::COLUMN_ORDER_ID => (int) $order->getEntityId()
                    ]
                );
                $vendorOrders->save();

                $result[Responses::KEY_ORDER_ID] = $order->getEntityId();
                $result[Responses::KEY_ORDER_NUMBER] = $order->getRealOrderId();

            } catch (\Exception $e) {
                return [Responses::ERROR => \Zend_Debug::dump($e->getMessage())];
            }

            $orders[] = $result;
        }

        return $orders;
    }

    private function disableActiveCart()
    {
        try {
            $basedCart = $this->cartRepositoryInterface->getActiveForCustomer($this->user_id);
        } catch (NoSuchEntityException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
        /** @var \Magento\Quote\Model\Quote $currentQuoteObj */

        $cartInterface = $this->context->getObjectManager()->create('Magento\Quote\Api\Data\CartInterface');
        $cartInterface->load($basedCart->getId());
        $cartInterface->setIsActive(false);
        $this->cartRepositoryInterface->save($cartInterface);
    }

    private function prepareVendorCart()
    {
        try {
            $basedCart = $this->cartRepositoryInterface->getActiveForCustomer($this->user_id);
        } catch (NoSuchEntityException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        $basedCartAddress = $basedCart->getShippingAddress();
        $streetsPreparation = $basedCartAddress->getStreet();
        $cartStreets = [];
        for ($i = 0, $streetsCount = count($streetsPreparation); $i < $streetsCount; $i++) {
            $cartStreets[] = [Responses::KEY_NAME => $streetsPreparation[$i]];
        }

        $basedAddress = [
            Responses::KEY_FIRSTNAME => $basedCartAddress->getFirstname(),
            Responses::KEY_LASTNAME => $basedCartAddress->getLastname(),
            Responses::KEY_STREETS => $cartStreets,
            Responses::KEY_CITY => $basedCartAddress->getCity(),
            Responses::KEY_POSTCODE => $basedCartAddress->getPostcode(),
            Responses::KEY_TELEPHONE => $basedCartAddress->getTelephone(),
            Responses::KEY_FAX => $basedCartAddress->getFax(),
            Responses::KEY_COUNTRY_ID => $basedCartAddress->getCountryId(),
            Responses::KEY_REGION => $basedCartAddress->getRegion(),
            Responses::KEY_REGION_ID => $basedCartAddress->getRegionId(),
            Responses::KEY_REGION_CODE => $basedCartAddress->getRegionCode(),
            Responses::KEY_SHIPPING_METHOD => $basedCartAddress->getShippingMethod(),
            Responses::KEY_SAVE_IN_ADDRESS_BOOK => $basedCartAddress->getSaveInAddressBook()
        ];

        $basedItems = array_reverse($basedCart->getAllItems());
        $vendorList = [];
        $vendorsItemList = [];
        foreach ($basedItems as $item) {
            $productInformation = $this->getProduct($item->getProductId());
            $vendorId = $productInformation[Responses::KEY_USER_ID];
            $vendorName = $productInformation[Responses::KEY_VENDOR_NAME];

            $vendorList[$vendorId] = [
                Responses::KEY_VENDOR_ID => $vendorId,
                Responses::KEY_VENDOR_NAME => $vendorName,
                Responses::KEY_SHIPPING_ADDRESS => $basedAddress
            ];

            $vendorsItemList[$vendorId][Responses::KEY_ITEMS][] = [
                'cart_item_id' => $item->getItemId(),
                Responses::KEY_PRODUCT_ID => $item->getProductId(),
                Responses::KEY_VENDOR_ID => $vendorId,
                Responses::KEY_NAME => $item->getName(),
                Responses::KEY_QTY => (int) $item->getQty(),
                Responses::KEY_PRICE => $price = empty($item->getPrice()) ? 0 : $item->getPrice()
            ];

            $vendorList[$vendorId][Responses::KEY_ITEMS] = $vendorsItemList[$vendorId][Responses::KEY_ITEMS];
        }

        return array_values($vendorList);
    }

    private function getPaymentMethods()
    {
        try {
            $cart = $this->cartRepositoryInterface->getForCustomer($this->user_id);
        } catch (\Exception $e) {
            return [Responses::ERROR => \Zend_Debug::dump($e->getMessage())];
        }

        $paymentList = [];
        try {
            $availablePayments = $this->paymentMethodManagementInterface->getList($cart->getId());
        } catch (NoSuchEntityException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        foreach ($availablePayments as $payment) {
            $paymentList[] = [
                'payment_code' => $payment->getCode(),
                'payment_title' => $payment->getTitle()
            ];
        }

        $this->cartRepositoryInterface->save($cart);

        return [
            'payment_methods_count' => count($paymentList),
            'payment_methods' => $paymentList
        ];
    }

    private function getDefaultShippingAddress()
    {
        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
            return $customer->getDefaultShipping();
        } catch (NoSuchEntityException $e) {
            return [Responses::ERROR => $e->getMessage()];
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
    }

    private function getDefaultBillingAddress()
    {
        try {
            $customer = $this->customerRepositoryInterface->getById($this->user_id);
            return $customer->getDefaultBilling();
        } catch (NoSuchEntityException $e) {
            return [Responses::ERROR => $e->getMessage()];
        } catch (LocalizedException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
    }

    private function getOrdersClient()
    {
        $orders = [];

        $sortOrder = $this->sortOrder->setField(Constants::COLUMN_CREATED_AT)->setDirection(SortOrder::SORT_DESC);
        $filterOne = $this->filterBuilder
            ->setField(Constants::COLUMN_CUSTOMER_ID)
            ->setConditionType('eq')
            ->setValue($this->user_id)
            ->create();
        $filterGroupOne = $this->filterGroupBuilder->addFilter($filterOne)->create();

        $orderPageSize = $this->page_size;
        $orderCount = $this->orderCount([$filterGroupOne]);
        if ((int) ceil($orderCount/$orderPageSize) < $this->page) {
            return $this->ordersResponse($orderCount, $orders);
        }

        $this->searchCriteriaBuilder->setSortOrders([$sortOrder]);
        $this->searchCriteriaBuilder->setFilterGroups([$filterGroupOne]);
        $this->searchCriteriaBuilder->setPageSize($this->page_size);
        $this->searchCriteriaBuilder->setCurrentPage($this->page);

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $orderList = $this->orderRepository->getList($searchCriteria);

        $orderItems = [];
        foreach ($orderList->getItems() as $order) {
            $orderAddress = $order->getShippingAddress();
            $streetsPreparation = $orderAddress->getStreet();
            $cartStreets = [];
            for ($i = 0, $streetsCount = count($streetsPreparation); $i < $streetsCount; $i++) {
                $cartStreets[] = [Responses::KEY_NAME => $streetsPreparation[$i]];
            }

            $orderAddress = [
                Responses::KEY_FIRSTNAME => $orderAddress->getFirstname(),
                Responses::KEY_LASTNAME => $orderAddress->getLastname(),
                Responses::KEY_STREETS => $cartStreets,
                Responses::KEY_CITY => $orderAddress->getCity(),
                Responses::KEY_POSTCODE => $orderAddress->getPostcode(),
                Responses::KEY_TELEPHONE => $orderAddress->getTelephone(),
                Responses::KEY_COUNTRY_ID => $orderAddress->getCountryId(),
                'country' => $this->getCountryTitleById($orderAddress->getCountryId())
            ];

            foreach ($order->getItems() as $orderItem) {
                $productInformation = $this->getProduct($orderItem->getProductId());
                $orderItems[] = [
                    Responses::KEY_PRODUCT_ID => $orderItem->getProductId(),
                    Responses::KEY_NAME => $orderItem->getName(),
                    Responses::KEY_QTY_ORDERED => (int) $orderItem->getQtyOrdered(),
                    Responses::KEY_PRICE => $price = empty($orderItem->getPrice()) ? 0 : $orderItem->getPrice(),
                    Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                    Responses::KEY_IMAGE_URL => !empty($productInformation[Responses::KEY_IMAGES]) ? $productInformation[Responses::KEY_IMAGES][0][Responses::KEY_URL] : '',
                    Responses::KEY_PRICE_TOTAL => $priceTotal = empty($orderItem->getRowTotal()) ? 0 : $orderItem->getRowTotal(),
                    Responses::KEY_FORMATTED_PRICE_TOTAL => Tools::getConvertedAndFormattedPrice($priceTotal, $this->def_currency, $this->currency_code),
                ];
            }

            $orderCommentsList = null;
            $orderComments = $order->getStatusHistories();
            if (!empty($orderComments) && count($orderComments) > 0) {
                foreach ($orderComments as $orderComment) {
                    if (!empty($orderCommentsList)) {
                        continue;
                    }
                    $orderCommentsList = $orderComment->getComment();
                }
            }

            $orders[] = [
                Responses::KEY_ORDER_ID => $order->getEntityId(),
                Responses::KEY_ORDER_NUMBER => $order->getIncrementId(),
                Responses::KEY_DATE_CREATED => $order->getCreatedAt(),
                Responses::KEY_STATUS => $order->getStatus(),
                Responses::KEY_COMMENT => $orderCommentsList,
                Responses::KEY_ITEMS_COUNT => $order->getTotalItemCount(),
                Responses::KEY_TOTAL => $totalPrice = (double) $order->getGrandTotal(),
                Responses::KEY_FORMATTED_TOTAL => Tools::getConvertedAndFormattedPrice($totalPrice, $this->def_currency, $this->currency_code),
                Responses::KEY_SHIPPING_ADDRESS => $orderAddress
            ];

            unset($orderItems);
        }

        return $this->ordersResponse($orderCount, $orders);
    }

    private function getOrdersClientRe($order_ids)
    {
        $orders = [];

        $sortOrder = $this->sortOrder->setField(Constants::COLUMN_CREATED_AT)->setDirection(SortOrder::SORT_DESC);
        $filterOne = $this->filterBuilder
            ->setField(Constants::COLUMN_CUSTOMER_ID)
            ->setConditionType('eq')
            ->setValue($this->user_id)
            ->create();
        $filterGroupOne = $this->filterGroupBuilder->addFilter($filterOne)->create();
        $filterTwo = $this->filterBuilder
            ->setField(Constants::COLUMN_ENTITY_ID)
            ->setConditionType('in')
            ->setValue($order_ids)
            ->create();
        $filterGroupTwo = $this->filterGroupBuilder->addFilter($filterTwo)->create();

        $orderCount = $this->orderCount([$filterGroupOne]);
        $this->searchCriteriaBuilder->setSortOrders([$sortOrder]);
        $this->searchCriteriaBuilder->setFilterGroups([$filterGroupOne, $filterGroupTwo]);


        $searchCriteria = $this->searchCriteriaBuilder->create();
        $orderList = $this->orderRepository->getList($searchCriteria);

        $orderItems = [];
        foreach ($orderList->getItems() as $order) {
            $orderAddress = $order->getShippingAddress();
            $streetsPreparation = $orderAddress->getStreet();
            $cartStreets = [];
            for ($i = 0, $streetsCount = count($streetsPreparation); $i < $streetsCount; $i++) {
                $cartStreets[] = [Responses::KEY_NAME => $streetsPreparation[$i]];
            }

            foreach ($order->getItems() as $orderItem) {
                $productInformation = $this->getProduct($orderItem->getProductId());
                $orderItems[] = [
                    Responses::KEY_PRODUCT_ID => $orderItem->getProductId(),
                    Responses::KEY_NAME => $orderItem->getName(),
                    Responses::KEY_QTY_ORDERED => (int) $orderItem->getQtyOrdered(),
                    Responses::KEY_PRICE => $price = empty($orderItem->getPrice()) ? 0 : $orderItem->getPrice(),
                    Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                    Responses::KEY_IMAGE_URL => !empty($productInformation[Responses::KEY_IMAGES]) ? $productInformation[Responses::KEY_IMAGES][0][Responses::KEY_URL] : '',
                    Responses::KEY_PRICE_TOTAL => $priceTotal = empty($orderItem->getRowTotal()) ? 0 : $orderItem->getRowTotal(),
                    Responses::KEY_FORMATTED_PRICE_TOTAL => Tools::getConvertedAndFormattedPrice($priceTotal, $this->def_currency, $this->currency_code),
                ];
            }

            $orderCommentsList = null;
            $orderComments = $order->getStatusHistories();
            if (!empty($orderComments) && count($orderComments) > 0) {
                foreach ($orderComments as $orderComment) {
                    if (!empty($orderCommentsList)) {
                        continue;
                    }
                    $orderCommentsList = $orderComment->getComment();
                }
            }

            $orders[] = [
                Responses::KEY_ORDER_ID => $order->getEntityId(),
                'client_id' => (int)$order->getCustomerId(),
                'vendor_id' => $this->getVendorIdByOrderId($order->getEntityId()),
                Responses::KEY_ORDER_NUMBER => $order->getIncrementId(),
                Responses::KEY_DATE_CREATED => $order->getCreatedAt(),
                Responses::KEY_STATUS => $order->getStatus(),
                Responses::KEY_ITEMS_COUNT => $order->getTotalItemCount(),
                Responses::KEY_TOTAL => $totalPrice = (double) $order->getGrandTotal(),
                Responses::KEY_FORMATTED_TOTAL => Tools::getConvertedAndFormattedPrice($totalPrice, $this->def_currency, $this->currency_code)
            ];

            unset($orderItems);
        }

        return $this->ordersResponse($orderCount, $orders);
    }

    private function getOrderClient()
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter(Constants::COLUMN_CUSTOMER_ID, $this->user_id)
            ->addFilter(Constants::COLUMN_ENTITY_ID, $this->order_id)
            ->create();

        $orderItems = [];
        $orders = [];
        $orderList = $this->orderRepository->getList($searchCriteria);

        foreach ($orderList->getItems() as $order) {
            $orderAddress = $order->getShippingAddress();
            $streetsPreparation = $orderAddress->getStreet();
            $cartStreets = [];
            for ($i = 0, $streetsCount = count($streetsPreparation); $i < $streetsCount; $i++) {
                $cartStreets[] = [Responses::KEY_NAME => $streetsPreparation[$i]];
            }

            $orderAddress = [
                Responses::KEY_FIRSTNAME => $orderAddress->getFirstname(),
                Responses::KEY_LASTNAME => $orderAddress->getLastname(),
                Responses::KEY_STREETS => $cartStreets,
                Responses::KEY_CITY => $orderAddress->getCity(),
                Responses::KEY_POSTCODE => $orderAddress->getPostcode(),
                Responses::KEY_TELEPHONE => $orderAddress->getTelephone(),
                Responses::KEY_COUNTRY_ID => $orderAddress->getCountryId(),
                'country' => $this->getCountryTitleById($orderAddress->getCountryId())
            ];

            foreach ($order->getItems() as $orderItem) {
                $productInformation = $this->getProduct($orderItem->getProductId());
                $orderItems[] = [
                    Responses::KEY_ORDER_ID => $order->getEntityId(),
                    'item_id' => $orderItem->getItemId(),
                    Responses::KEY_PRODUCT_ID => $orderItem->getProductId(),
                    Responses::KEY_NAME => $orderItem->getName(),
                    Responses::KEY_QTY_ORDERED => (int) $orderItem->getQtyOrdered(),
                    Responses::KEY_PRICE => $price = empty($orderItem->getPrice()) ? 0 : $orderItem->getPrice(),
                    Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                    Responses::KEY_IMAGE_URL => !empty($productInformation[Responses::KEY_IMAGES]) ? $productInformation[Responses::KEY_IMAGES][0][Responses::KEY_URL] : '',
                    Responses::KEY_PRICE_TOTAL => $priceTotal = empty($orderItem->getRowTotal()) ? 0 : $orderItem->getRowTotal(),
                    Responses::KEY_FORMATTED_PRICE_TOTAL => Tools::getConvertedAndFormattedPrice($priceTotal, $this->def_currency, $this->currency_code),
                ];
            }

            $orderCommentsList = null;
            $orderComments = $order->getStatusHistories();
            if (!empty($orderComments) && count($orderComments) > 0) {
                foreach ($orderComments as $orderComment) {
                    if (!empty($orderCommentsList)) {
                        continue;
                    }
                    $orderCommentsList = $orderComment->getComment();
                }
            }

            $orders = [
                Responses::KEY_ORDER_ID => $order->getEntityId(),
                'client_id' => (int)$order->getCustomerId(),
                'vendor_id' => $this->getVendorIdByOrderId($order->getEntityId()),
                Responses::KEY_ORDER_NUMBER => $order->getIncrementId(),
                Responses::KEY_DATE_CREATED => $order->getCreatedAt(),
                Responses::KEY_STATUS => $order->getStatus(),
                Responses::KEY_COMMENT => $orderCommentsList,
                Responses::KEY_ITEMS_COUNT => $order->getTotalItemCount(),
                Responses::KEY_TOTAL => $totalPrice = (double) $order->getGrandTotal(),
                Responses::KEY_FORMATTED_TOTAL => Tools::getConvertedAndFormattedPrice($totalPrice, $this->def_currency, $this->currency_code),
                Responses::KEY_SHIPPING_ADDRESS => $orderAddress
            ];
        }

        return $this->orderResponse($orders, $orderItems);
    }

    private function getOrdersVendor()
    {
        $vendorOrders               = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\VendorOrders');
        $vendorOrdersCollection     = $vendorOrders->getCollection()->addFieldToFilter(Constants::COLUMN_VENDOR_ID, ['eq' => $this->user_id]);
        $vendorOrdersCount          = $vendorOrdersCollection->getSize();
        $vendorOrdersCollection->getSelect()->limit(Constants::VENDOR_PAGE_LIMIT, ($this->page - 1) * Constants::VENDOR_PAGE_LIMIT);
        $vendorOrdersList = $vendorOrdersCollection->getConnection()->fetchAll($vendorOrdersCollection->getSelectSql());

        $orders = [];
        if ((int) ceil($vendorOrdersCount/$this->page_size) < $this->page) {
            return $this->ordersResponse($vendorOrdersCount, $orders);
        }

        $vendorOrdersIdList = [];
        for ($i = 0, $vendorOrdersCount = count($vendorOrdersList); $i < $vendorOrdersCount; $i++) {
            $vendorOrdersIdList[] = $vendorOrdersList[$i][Responses::KEY_ORDER_ID];
        }

        $sortOrder = $this->sortOrder->setField(Constants::COLUMN_CREATED_AT)->setDirection(SortOrder::SORT_DESC);
        $filterOne = $this->filterBuilder
            ->setField(Constants::COLUMN_ENTITY_ID)
            ->setConditionType('in')
            ->setValue(implode(',', $vendorOrdersIdList))
            ->create();
        $filterGroupOne = $this->filterGroupBuilder->addFilter($filterOne)->create();

        $this->searchCriteriaBuilder->setSortOrders([$sortOrder]);
        $this->searchCriteriaBuilder->setFilterGroups([$filterGroupOne]);

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $orderList = $this->orderRepository->getList($searchCriteria);

        $orderItems = [];
        foreach ($orderList->getItems() as $order) {
            $orderAddress = $order->getShippingAddress();
            $streetsPreparation = $orderAddress->getStreet();
            $cartStreets = [];
            for ($i = 0, $streetsCount = count($streetsPreparation); $i < $streetsCount; $i++) {
                $cartStreets[] = [Responses::KEY_NAME => $streetsPreparation[$i]];
            }

            $orderAddress = [
                Responses::KEY_FIRSTNAME => $orderAddress->getFirstname(),
                Responses::KEY_LASTNAME => $orderAddress->getLastname(),
                Responses::KEY_STREETS => $cartStreets,
                Responses::KEY_CITY => $orderAddress->getCity(),
                Responses::KEY_POSTCODE => $orderAddress->getPostcode(),
                Responses::KEY_TELEPHONE => $orderAddress->getTelephone(),
                Responses::KEY_FAX => $orderAddress->getFax(),
                Responses::KEY_COUNTRY_ID => $orderAddress->getCountryId(),
                Responses::KEY_REGION => $orderAddress->getRegion(),
                Responses::KEY_REGION_ID => $orderAddress->getRegionId(),
                Responses::KEY_REGION_CODE => $orderAddress->getRegionCode(),
                Responses::KEY_SHIPPING_METHOD => $orderAddress->getShippingMethod(),
                Responses::KEY_SAVE_IN_ADDRESS_BOOK => $orderAddress->getSaveInAddressBook()
            ];

            foreach ($order->getItems() as $orderItem) {
                $productInformation = $this->getProduct($orderItem->getProductId());
                $orderItems[] = [
                    Responses::KEY_PRODUCT_ID => $orderItem->getProductId(),
                    Responses::KEY_NAME => $orderItem->getName(),
                    Responses::KEY_QTY_ORDERED => (int) $orderItem->getQtyOrdered(),
                    Responses::KEY_PRICE => $price = empty($orderItem->getPrice()) ? 0 : $orderItem->getPrice(),
                    Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                    Responses::KEY_IMAGE_URL => !empty($productInformation[Responses::KEY_IMAGES]) ? $productInformation[Responses::KEY_IMAGES][0][Responses::KEY_URL] : '',
                    Responses::KEY_PRICE_TOTAL => $priceTotal = empty($orderItem->getRowTotal()) ? 0 : $orderItem->getRowTotal(),
                    Responses::KEY_FORMATTED_PRICE_TOTAL => Tools::getConvertedAndFormattedPrice($priceTotal, $this->def_currency, $this->currency_code),
                    Responses::KEY_SKU => !empty($productInformation[Responses::KEY_SKU]) ? $productInformation[Responses::KEY_SKU] : '',
                ];
            }

            $orderCommentsList = null;
            $orderComments = $order->getStatusHistories();
            if (!empty($orderComments) && count($orderComments) > 0) {
                foreach ($orderComments as $orderComment) {
                    if (!empty($orderCommentsList)) {
                        continue;
                    }
                    $orderCommentsList = $orderComment->getComment();
                }
            }

            $orders[] = [
                Responses::KEY_ORDER_ID => $order->getEntityId(),
                'client_id' => (int)$order->getCustomerId(),
                'vendor_id' => $this->getVendorIdByOrderId($order->getEntityId()),
                Responses::KEY_ORDER_NUMBER => $order->getIncrementId(),
                Responses::KEY_DATE_CREATED => $order->getCreatedAt(),
                Responses::KEY_STATUS => $order->getStatus(),
                Responses::KEY_COMMENT => $orderCommentsList,
                Responses::KEY_ITEMS_COUNT => $order->getTotalItemCount(),
                Responses::KEY_TOTAL => $totalPrice = (double) $order->getGrandTotal(),
                Responses::KEY_FORMATTED_TOTAL => Tools::getConvertedAndFormattedPrice(
                    $totalPrice,
                    $this->def_currency,
                    $this->currency_code
                ),
                Responses::KEY_SHIPPING_ADDRESS => $orderAddress,
                Responses::KEY_ITEMS => $orderItems
            ];

            unset($orderItems);
        }

        return $this->ordersResponse($vendorOrdersCount, $orders);
    }

    private function getOrderVendor()
    {
        $sortOrder = $this->sortOrder->setField(Constants::COLUMN_CREATED_AT)->setDirection(SortOrder::SORT_DESC);
        $filterOne = $this->filterBuilder
            ->setField(Constants::COLUMN_ENTITY_ID)
            ->setConditionType('eq')
            ->setValue($this->order_id)
            ->create();
        $filterGroupOne = $this->filterGroupBuilder->addFilter($filterOne)->create();

        $this->searchCriteriaBuilder->setSortOrders([$sortOrder]);
        $this->searchCriteriaBuilder->setFilterGroups([$filterGroupOne]);

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $orderList = $this->orderRepository->getList($searchCriteria);

        $orders = [];
        $orderItems = [];
        foreach ($orderList->getItems() as $order) {
            $orderAddress = $order->getShippingAddress();
            $streetsPreparation = $orderAddress->getStreet();
            $cartStreets = [];
            for ($i = 0, $streetsCount = count($streetsPreparation); $i < $streetsCount; $i++) {
                $cartStreets[] = [Responses::KEY_NAME => $streetsPreparation[$i]];
            }

            $orderAddress = [
                Responses::KEY_FIRSTNAME => $orderAddress->getFirstname(),
                Responses::KEY_LASTNAME => $orderAddress->getLastname(),
                Responses::KEY_STREETS => $cartStreets,
                Responses::KEY_CITY => $orderAddress->getCity(),
                Responses::KEY_POSTCODE => $orderAddress->getPostcode(),
                Responses::KEY_TELEPHONE => $orderAddress->getTelephone(),
                Responses::KEY_FAX => $orderAddress->getFax(),
                Responses::KEY_COUNTRY_ID => $orderAddress->getCountryId(),
                'country' => $this->getCountryTitleById($orderAddress->getCountryId()),
                Responses::KEY_REGION => $orderAddress->getRegion(),
                Responses::KEY_REGION_ID => $orderAddress->getRegionId(),
                Responses::KEY_REGION_CODE => $orderAddress->getRegionCode(),
                Responses::KEY_SHIPPING_METHOD => $orderAddress->getShippingMethod(),
                Responses::KEY_SAVE_IN_ADDRESS_BOOK => $orderAddress->getSaveInAddressBook()
            ];

            foreach ($order->getItems() as $orderItem) {
                $productInformation = $this->getProduct($orderItem->getProductId());
                $orderItems[] = [
                    Responses::KEY_ORDER_ID => $order->getEntityId(),
                    'item_id' => $orderItem->getItemId(),
                    Responses::KEY_PRODUCT_ID => $orderItem->getProductId(),
                    Responses::KEY_NAME => $orderItem->getName(),
                    Responses::KEY_QTY_ORDERED => (int) $orderItem->getQtyOrdered(),
                    Responses::KEY_PRICE => $price = empty($orderItem->getPrice()) ? 0 : $orderItem->getPrice(),
                    Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                    Responses::KEY_IMAGE_URL => !empty($productInformation[Responses::KEY_IMAGES]) ? $productInformation[Responses::KEY_IMAGES][0][Responses::KEY_URL] : '',
                    Responses::KEY_PRICE_TOTAL => $priceTotal = empty($orderItem->getRowTotal()) ? 0 : $orderItem->getRowTotal(),
                    Responses::KEY_FORMATTED_PRICE_TOTAL => Tools::getConvertedAndFormattedPrice($priceTotal, $this->def_currency, $this->currency_code),
                    Responses::KEY_SKU => !empty($productInformation[Responses::KEY_SKU]) ? $productInformation[Responses::KEY_SKU] : '',
                ];
            }

            $orderCommentsList = null;
            $orderComments = $order->getStatusHistories();
            if (!empty($orderComments) && count($orderComments) > 0) {
                foreach ($orderComments as $orderComment) {
                    if (!empty($orderCommentsList)) {
                        continue;
                    }
                    $orderCommentsList = $orderComment->getComment();
                }
            }

            $orders = [
                Responses::KEY_ORDER_ID => $order->getEntityId(),
                'client_id' => (int)$order->getCustomerId(),
                'vendor_id' => $this->getVendorIdByOrderId($order->getEntityId()),
                Responses::KEY_ORDER_NUMBER => $order->getIncrementId(),
                Responses::KEY_DATE_CREATED => $order->getCreatedAt(),
                Responses::KEY_STATUS => $order->getStatus(),
                Responses::KEY_COMMENT => $orderCommentsList,
                Responses::KEY_ITEMS_COUNT => $order->getTotalItemCount(),
                Responses::KEY_TOTAL => $totalPrice = (double) $order->getGrandTotal(),
                Responses::KEY_FORMATTED_TOTAL => Tools::getConvertedAndFormattedPrice($totalPrice, $this->def_currency, $this->currency_code),
                Responses::KEY_SHIPPING_ADDRESS => $orderAddress
            ];
        }

        return $this->orderResponse($orders, $orderItems);
    }

    private function orderCount(array $filterGroups)
    {
        $this->searchCriteriaBuilder->setFilterGroups($filterGroups);
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $orders = $this->orderRepository->getList($searchCriteria);

        return $orders->getTotalCount();
    }

    private function ordersResponse(int $orderCount, array $orders)
    {
        return [
            Responses::KEY_ORDER_COUNT  => $orderCount,
            Responses::KEY_ORDERS       => $orders
        ];
    }

    private function orderResponse(array $order, array $items)
    {
        return [
            Responses::KEY_ORDER => $order,
            Responses::KEY_ITEMS => $items
        ];
    }

    private function getVendorIdByOrderId(int $orderId)
    {
        $vendorOrders               = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\VendorOrders');
        $vendorOrdersCollection     = $vendorOrders->getCollection()->addFieldToFilter(Constants::COLUMN_ORDER_ID, ['eq' => $orderId]);
        $vendorOrdersList           = $vendorOrdersCollection->getConnection()->fetchAll($vendorOrdersCollection->getSelectSql());

        $vendorOrdersIdList = [];
        for ($i = 0, $vendorOrdersCount = count($vendorOrdersList); $i < $vendorOrdersCount; $i++) {
            $vendorOrdersIdList[] = $vendorOrdersList[$i][Constants::COLUMN_VENDOR_ID];
        }

        return is_array($vendorOrdersIdList) ? $vendorOrdersIdList[0] : '';
    }

    private function getCountryTitleById(string $countryId)
    {
        $countryName = '';

        try {
            $collection = $this->countryCollectionFactory->create()->loadByStore();
            $countries = $collection->getItems();
            foreach ($countries as $key => $country) {
                if ($key == $countryId) {
                    $countryName = $country->getName();
                }
                continue;
            }
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        return (string)$countryName;

    }

    private function getVendorRating($vendorId)
    {
        $ratingFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\VendorRating');
        $ratingByVendor = $ratingFactory->create()->getCollection()->addFieldToFilter(Constants::COLUMN_VENDOR_ID, ['eq' => $vendorId])->getData();

        $vendorsRating = 0;
        $countVendorRating = count($ratingByVendor);
        for ($i = 0; $i < $countVendorRating; $i++) {
            $vendorsRating = + (double) $ratingByVendor[$i];
        }

        return $vendorsRating/$countVendorRating;
    }

    private function changeVendorRating($vendorId)
    {
        $ratingFactory = $this->context->getObjectManager()->create('Emagicone\Karavaniumconnector\Model\VendorRatingFactory')->create();
        $ratingByUser = $ratingFactory->getCollection()->addFieldToFilter(Constants::COLUMN_USER_ID, ['eq' => $this->user_id])->getData();

        if (empty($ratingByUser)) {
            $ratingFactory->setVendorId($vendorId);
            $ratingFactory->setUserId($this->user_id);
        }
        $ratingFactory->setRating($this->rating);

        $ratingFactory->save();
        return true;
    }

    private function reindexAll()
    {
        $indexerFactory = $this->_objectManager->get('Magento\Indexer\Model\IndexerFactory');
        $indexerIds = array(
            'catalog_category_product',
            'catalog_product_category',
            'catalog_product_price',
            'catalog_product_attribute',
            'cataloginventory_stock',
            'catalogrule_product',
            'catalogsearch_fulltext',
        );
        foreach ($indexerIds as $indexerId) {
            /** @var \Magento\Indexer\Model\Indexer $indexer*/
            echo " create index: " . $indexerId . "\n";
            $indexer = $indexerFactory->create();
            $indexer->load($indexerId);
            try {
                $indexer->reindexAll();
            } catch (\Exception $e) {
                return [Responses::ERROR => $e->getMessage()];
            }
        }
    }

    private function resetNull(&$item)
    {
        if (empty($item) && $item != 0) {
            $item = '';
        }

        $item = trim($item);
    }

    private function getProduct($productId)
    {
        $productsCollection = Tools::getProductModel($this->context->getObjectManager())->getCollection()->addAttributeToSelect(
            [
                'entity_id',
                'name',
                'sku',
                'status',
                'entity_type'
            ]
        )->addAttributeToFilter('entity_id', $productId);

        /** @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $productsCollection */
        $productsCollection->getSelect()->joinLeft(
            ['cpkui' => Tools::getDbTablePrefix() . 'catalog_product_entity_varchar'],
            'cpkui.entity_id = e.entity_id AND cpkui.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_user_id' LIMIT 1) AND cpkui.store_id = 0", ['cpkui.value AS karavanium_user_id']
        )->joinLeft(
            ['cs' => Tools::getDbTablePrefix() . 'customer_entity'], 'cs.entity_id = cpkui.value', []
        )->joinLeft(
            ['cekr' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekr.entity_id = cs.entity_id AND cekr.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_rating' LIMIT 1)", ['cekr.value AS karavanium_rating']
        )->joinLeft(
            ['cekvn' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'cekvn.entity_id = cs.entity_id AND cekvn.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_vendor_name' LIMIT 1)", ['cekvn.value AS karavanium_vendor_name']
        )->joinLeft(
            ['ceka' => Tools::getDbTablePrefix() . 'customer_entity_varchar'],
            'ceka.entity_id = cs.entity_id AND ceka.attribute_id = (SELECT attribute_id FROM ' . Tools::getDbTablePrefix() . "eav_attribute WHERE attribute_code = 'karavanium_avatar' LIMIT 1)", ['ceka.value AS karavanium_avatar']
        );

        $products = [];
        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
            $baseUrl = $this->_url->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);
        } catch (FileSystemException $e) {
            return [Responses::ERROR => $e->getMessage()];
        }

        foreach ($productsCollection as $product) {
            /** @var \Magento\Catalog\Model\Product $product*/
            if ($product->getEntityId() != $productId) {
                continue;
            }

            try {
                $product->load($product->getEntityId());
            } catch (\Exception $e) {
                $e->getMessage();
            }

            $stockState = $this->context->getObjectManager()->create('Magento\CatalogInventory\Api\StockRegistryInterface');
            $stock = $stockState->getStockItem(
                $product->getEntityId(), $product->getStore()->getWebsiteId()
            );

            try {
                $customer = $this->customerRepositoryInterface->getById($this->user_id);
                $vendorSubscribed = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS);
                $baseImageId = $this->getBaseImageId($product->getMediaGalleryEntries());
                $mediaGallery = $this->getMediaGalleryByProduct($product->getEntityId(), $product->getMediaGalleryImages()->getItems(), $baseImageId, $mediaDirectory);
            } catch (\Exception $e) {
                return [Responses::ERROR => $e->getMessage()];
            }

            $vendorSubscribedValue = !empty($vendorSubscribed) ? $vendorSubscribed->getValue() : '';

            $products = [
                Responses::KEY_PRODUCT_ID => $product->getEntityId(),
                Responses::KEY_USER_ID => empty($userId = $product->getKaravaniumUserId()) ? 0 : $userId,
                Responses::KEY_VENDOR_ID => empty($userId = $product->getKaravaniumUserId()) ? 0 : $userId,
                Responses::KEY_VENDOR_NAME => $product->getData(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME),
                Responses::KEY_VENDOR_RATING => empty($product->getData(Constants::ATTRIBUTE_KARAVANIUM_RATING)) ? 0 : $product->getData(
                    Constants::ATTRIBUTE_KARAVANIUM_RATING
                ),
                Responses::KEY_VENDOR_AVATAR => empty(
                $product->getData(
                    Constants::ATTRIBUTE_KARAVANIUM_AVATAR
                )
                ) ? '' : $baseUrl . $product->getData(
                        Constants::ATTRIBUTE_KARAVANIUM_AVATAR
                    ),
                Responses::KEY_VENDOR_SUBSCRIBED => !empty(strstr($vendorSubscribedValue, $product->getKaravaniumUserId())) ? 1 : 0,
                Responses::KEY_SKU => !empty($customSku = $product->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_SKU))
                    ? $customSku->getValue()
                    : '',
                Responses::KEY_NAME => $product->getName(),
                Responses::KEY_DESCRIPTION => $product->getDescription(),
                Responses::KEY_IMAGES => $mediaGallery,
                Responses::KEY_STATUS => ($product->getStatus() == ProductStatus::STATUS_ENABLED) ? 1 : 0,
                Responses::KEY_QTY => (int) $stock->getQty(),
                Responses::KEY_PRICE => $price = empty($product->getPrice()) ? 0 : $product->getPrice(),
                Responses::KEY_FORMATTED_PRICE => Tools::getConvertedAndFormattedPrice($price, $this->def_currency, $this->currency_code),
                Responses::KEY_FORMATTED_PRICE_TOTAL => Tools::getConvertedAndFormattedPrice($price * $stock->getQty(), $this->def_currency, $this->currency_code)
            ];
        }
        return $products;
    }

    /**
     * @return mixed
     */
    private function getMaxFileUploadInBytes()
    {
        // select maximum upload size
        $maxUpload = $this->calculateBytes(ini_get('upload_max_filesize'));

        // select post limit
        $maxPost = $this->calculateBytes(ini_get('post_max_size'));

        // select memory limit
        $memoryLimit = $this->calculateBytes(ini_get('memory_limit'));

        // return the smallest of them, this defines the real limit
        return min($maxUpload, $maxPost, $memoryLimit);
    }

    private function calculateBytes($val)
    {
        $val = trim($val);
        $numeric = filter_var($val, FILTER_SANITIZE_NUMBER_INT);
        $last = strtolower($val[strlen($val) - 1]);

        switch ($last) {
            case 'g':
                $numeric *= 1024 * 1024 * 1024;
                break;
            case 'm':
                $numeric *= 1024 * 1024;
                break;
            case 'k':
                $numeric *= 1024;
                break;
        }

        return $numeric;
    }

    private function getCartSummary($userId, $cart = null)
    {
        $cartProductsCount = 0;
        $cartProductsTotal = 0.00;

        /** @var \Magento\Quote\Model\Quote $cart */
        if (!empty($cart)) {
            $cartProductsCount = $cart->getData('items_count');
            $cartProductsTotal = $cart->getData('grand_total');
        } else {
            try {
                $cart = $this->cartManagementInterface->getCartForCustomer($userId);

                if (!empty($cart)) {
                    $cartProductsCount = $cart->getData('items_count');
                    $cartProductsTotal = $cart->getData('grand_total');
                }
            } catch (\Exception $e) {
            }
        }

        return [
            'cart_products_count' => $cartProductsCount,
            'cart_products_total' => Tools::getConvertedAndFormattedPrice(
                $cartProductsTotal, $this->def_currency, $this->currency_code
            )
        ];
    }

    private function getItems()
    {
        $items = [];
        try {
            $cart = $this->cartRepositoryInterface->getActiveForCustomer($this->user_id);
            $items = $cart->getItemsCollection();
        } catch (\Exception $e) {}

        $get = [];
        $mediaDirectory = '';
        try {
            $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        } catch (FileSystemException $e) {}

        foreach ($items as $item) {
            if ($item->getProductId() == $this->product_id) {
                try {
                    $product = $this->productRepository->getById($item->getProductId());
                    try {
                        $baseImageId = $this->getBaseImageId($product->getMediaGalleryEntries());
                        $mediaGallery = $this->getMediaGalleryByProduct($product->getId(), $product->getMediaGalleryImages()->getItems(), $baseImageId, $mediaDirectory);
                    } catch (\Exception $e) {
                        return Responses::ERROR_NO_SUCH_PRODUCT;
                    }
                    $get = [
                        'cart_item_id'          => $item->getItemId(),
                        'product_id'            => $item->getProductId(),
                        'vendor_id'             => $product->getKaravaniumUserId(),
                        'name'                  => $item->getName(),
                        'images'                => !empty($mediaGallery) ? $mediaGallery : [],
                        'qty_ordered'           => $qty = (int) $item->getQty(),
                        'price'                 => $price = empty($item->getPrice()) ? 0 : $item->getPrice(),
                        'formatted_price'       => Tools::getConvertedAndFormattedPrice(
                            $price, $this->def_currency, $this->currency_code
                        ),
                        'formatted_price_total' => Tools::getConvertedAndFormattedPrice(
                            $price * $qty, $this->def_currency, $this->currency_code
                        )
                    ];
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
        }

        return $get;
    }

    private function getProductTypeLabelByTypeId($product_type_id)
    {
        $productTypeLabel = '';
        $productTypes = Tools::getObjectManager()->get('Magento\Catalog\Model\ProductTypeList')->getProductTypes();

        foreach ($productTypes as $productType) {
            if ($productType->getName() == $product_type_id) {
                $productTypeLabel = $productType->getLabel();
                break;
            }
        }

        return $productTypeLabel;
    }

    private function getStatusOptions()
    {
        $statuses = $this->productStatus::getOptionArray();

        /** @var \Magento\Framework\Phrase $statuses */
        return [
            ProductStatus::STATUS_ENABLED => $statuses[ProductStatus::STATUS_ENABLED]->getText(),
            ProductStatus::STATUS_ENABLED => $statuses[ProductStatus::STATUS_ENABLED]->getText()
        ];
    }

    private function getStockOptions()
    {
        $stockOptions = [];
        $options = Tools::getObjectManager()->create('Magento\CatalogInventory\Model\Source\Stock')->getAllOptions();

        for ($i = 0, $count = count($options); $i < $count; $i++) {
            $stockOptions[$options[$i]['value']] = $options[$i]['label']->getText();
        }

        return $stockOptions;
    }

    private function getProductImageUrl(\Magento\Catalog\Model\Product $product, array $size, $imageFile = false)
    {
        $image = Tools::getObjectManager()->get('Magento\Catalog\Helper\Image')->init(
            $product, 'image', array_merge(['type' => 'image'], $size)
        );

        if ($imageFile) {
            $image->setImageFile($imageFile);
        }

        return $image->getUrl();
    }

    private function checkProductUrlKeyDuplicates($urlKey, $n = 0)
    {
        $n++;
        $productsUrlKeyValidation = Tools::getProductModel($this->context->getObjectManager())
            ->getCollection()
            ->addAttributeToFilter('url_key', ['eq' => $urlKey])
            ->getSize();

        if ($productsUrlKeyValidation > 0) {
            return $this->checkProductUrlKeyDuplicates($urlKey . '-' . $n, $n);
        }

        return $urlKey;
    }

    private function getRegionId($countryId, $regionCode)
    {
        $countryFactory = $this->context->getObjectManager()->get('Magento\Directory\Model\CountryFactory');
        $country = $countryFactory->create()->loadByCode($countryId);

        if (strlen($regionCode) > 3) {
            return $country->getRegionCollection()->addFieldToFilter('name', $regionCode)->getFirstItem()->getId();
        }

        return $country->getRegionCollection()->addFieldToFilter('code', $regionCode)->getFirstItem()->getId();
    }

    private function resizeImage($imagePath, $mediaDirectory)
    {
        /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageFactory */

        if (empty($this->image_size) || !$mediaDirectory->isFile($imagePath)) {
            return $imagePath;
        }

        $mediaFolder = "products/karavanium_cache/$this->image_size";

        $cachePath = strstr($imagePath, 'product');
        $urlOfImage = strstr($imagePath, 'catalog', true);

        $imageResize = $mediaDirectory->getAbsolutePath($mediaFolder) . '/' . $cachePath;

        $imageFactory = $this->adapterFactory->create();
        if ($mediaDirectory->isFile($imageResize)) {
//            $imageFactory->open($imageResize);
            return $urlOfImage . $mediaFolder . '/' . $cachePath;
        }
        $imageFactory->open($imagePath);
        $originalWidth = $imageFactory->getOriginalWidth();
        $originalHeight = $imageFactory->getOriginalHeight();
        $oldAspectRatio = $originalWidth / $originalHeight;
        $newAspectRatio = $this->image_size / $this->image_size;
        if ($originalWidth < $this->image_size || $originalHeight < $this->image_size) {
            return $imagePath;
        }

        $imageFactory->constrainOnly(true);
        $imageFactory->keepTransparency(true);
        $imageFactory->backgroundColor([255, 255, 255]);
        $imageFactory->keepAspectRatio(true);
        $imageFactory->keepFrame(false);

        if ($oldAspectRatio > $newAspectRatio) {
            // original image is wider than the desired dimensions
            $imageFactory->resize(null, $this->image_size);
            $imageFactory->crop(0, 0, 0, 0);
        } else {
            // it's taller...
            $imageFactory->resize($this->image_size, null);
            $imageFactory->crop(0, 0, 0, 0);
        }

        try {
            $imageFactory->save($imageResize);
        } catch (\Exception $e) {
            return [Responses::ERROR => $e->getMessage()];
        }
    }

    public static function snakeCaseToCamelCase($string)
    {
        if (!$string) {
            return '';
        }

        $str    = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));
        $str[0] = strtolower($str[0]);

        return $str;
    }

    /**
     * Create exception in case CSRF validation failed.
     * Return null if default exception will suffice.
     *
     * @param RequestInterface $request
     *
     * @return InvalidRequestException|null
     * @SuppressWarnings(PMD.UnusedFormalParameter)
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * Perform custom request validation.
     * Return null if default validation is needed.
     *
     * @param RequestInterface $request
     *
     * @return bool|null
     * @SuppressWarnings(PMD.UnusedFormalParameter)
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}