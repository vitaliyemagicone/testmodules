<?php
/**
 *    This file is part of Karavanium Connector.
 *
 *   Karavanium Connector is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Karavanium Connector is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Karavanium Connector.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Emagicone\Karavaniumconnector\Model\ResourceModel;

use Emagicone\Karavaniumconnector\Helper\Constants;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class VendorRating
 * @package Emagicone\Karavaniumconnector\Model\ResourceModel
 */
class VendorRating extends AbstractDb
{
    /**
     * Initialize resource model
     *
     * @internal param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */

    protected function _construct()
    {
        $this->_init(Constants::TABLE_VENDOR_RATING, Constants::COLUMN_ID);
    }
}
