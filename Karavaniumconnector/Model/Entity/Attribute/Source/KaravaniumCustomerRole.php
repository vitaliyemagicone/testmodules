<?php

namespace Emagicone\Karavaniumconnector\Model\Entity\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class KaravaniumCustomerRole extends AbstractSource
{
    public function getAllOptions()
    {
        return [
            'option1' => [
                'label' => 'Client',
                'value' => 'client'
            ],
            'option2' => [
                'label' => 'Vendor',
                'value' => 'vendor'
            ]
        ];
    }
}