<?php
/**
 * Created by PhpStorm.
 * User: vitaliy
 * Date: 9/27/18
 * Time: 2:27 PM
 */

namespace Emagicone\Karavaniumconnector\Helper;

use Google\Cloud\Firestore\FirestoreClient;
use Symfony\Component\DependencyInjection\Tests\Compiler\F;

class Firestore
{
    const FS_ENV = "GOOGLE_APPLICATION_CREDENTIALS";

    protected $fireStoreClient;

    /**
     * Firestore constructor.
     * @param $fireStoreClient
     * @throws \Google\Cloud\Core\Exception\GoogleException
     */
    public function __construct()
    {
        if (empty(getenv(self::FS_ENV))) {
            $path = "/home/qaemagicone/public_html/karavaniumsd/app/code/Emagicone/Karavaniumconnector/etc/karavanium-google-services.json";
            putenv("GOOGLE_APPLICATION_CREDENTIALS=$path");
        }
        $this->fireStoreClient = new FirestoreClient();
    }

    /**
     * @return Firestore
     * @throws \Google\Cloud\Core\Exception\GoogleException
     */
    public static function create()
    {
        return new self();
    }

    public function createDocument($collectionPath, $values, $documentId, $options = [])
    {
        $documentReference = $this->fireStoreClient
            ->collection($collectionPath)
            ->document($documentId);

        $documentReference->set($values, $options);
    }

    public function readDocuments($collectionPath, $options = [])
    {
        $documentReference = $this->fireStoreClient->collection($collectionPath);
        $documentSnapshot = $documentReference->documents($options);

        if ($documentSnapshot->isEmpty() || $documentSnapshot->size() < 1) {
            return false;
        }

        return $documentSnapshot;
    }

    public function updateDocument($collectionPath, $values, $documentId, $options = [])
    {
        $documentReference = $this->fireStoreClient
            ->collection($collectionPath)
            ->document($documentId);

        $documentReference->update($values, $options);
    }

    public function deleteDocument($collectionPath, $documentId, $options = [])
    {
        $documentReference = $this->fireStoreClient
            ->collection($collectionPath)
            ->document($documentId);

        $documentReference->delete($options);
    }


    public function createNewDocument($collectionPath, $values, $options = [])
    {
        $documentReference = $this->fireStoreClient
        ->collection($collectionPath)
        ->newDocument();

        $documentReference->set($values, $options);
    }
}