<?php

namespace Emagicone\Karavaniumconnector\Helper;

class AESCiphers
{
    const OPENSSL_CIPHER_NAME   = 'aes-128-cbc';         // Name of OpenSSL Cipher
    const IV                    = 'f42aacad57b7dffc';
    const SECRET_KEY            = '8d95140df15aeabc';

    /**
     * Encrypt data using AES Cipher (CBC) with 128 bit key
     *
     * @param string $data - data to encrypt
     * @return string|bool encrypted data in base64 encoding with iv attached at end after a :
     */
    public static function encrypt($data)
    {
        $encodedEncryptedData = base64_encode(
            openssl_encrypt($data, self::OPENSSL_CIPHER_NAME, self::SECRET_KEY, OPENSSL_RAW_DATA, self::IV)
        );
        return $encodedEncryptedData;
    }

    /**
     * Decrypt data using AES Cipher (CBC) with 128 bit key
     *
     * @param string $data - data to be decrypted in base64 encoding with iv attached at the end after a :
     * @return string|bool decrypted data
     */
    public static function decrypt($data)
    {
        if (empty($data)) {
            return false;
        }

        $parts = explode(':', $data); //Separate Encrypted data from iv.

        $decryptedData = openssl_decrypt(
            base64_decode($parts[0]),
            self::OPENSSL_CIPHER_NAME,
            self::SECRET_KEY,
            OPENSSL_RAW_DATA,
            self::IV
        );

        return $decryptedData;
    }
}
