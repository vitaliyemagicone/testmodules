<?php

/**
 *    This file is part of Karavanium Connector.
 *
 *   Karavanium Connector is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Karavanium Connector is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Karavanium Connector.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Emagicone\Karavaniumconnector\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Emagicone\Karavaniumconnector\Helper\Responses;

/**
 * Class Access
 * @package Emagicone\Karavaniumconnector\Helper
 */
class Access
{
    public static function checkAuth($username, $password)
    {
        $decryptedUsername = AESCiphers::decrypt($username);
        $decryptedPassword = AESCiphers::decrypt($password);

        if (!$decryptedUsername || !$decryptedPassword) {
            self::addFailedAttempt();
            return Responses::ERROR_NO_LOGIN_DATA;
        }

        $customers = Tools::getCustomerModel(Tools::getObjectManager())->getCollection()
            ->addAttributeToSelect(Constants::ATTRIBUTE_KARAVANIUM_ROLE)
            ->addAttributeToFilter(Constants::ATTRIBUTE_KARAVANIUM_USERNAME, ['eq' => $decryptedUsername]);


        foreach ($customers as $customer) {
            if (!$customer->validatePassword($decryptedPassword)) {
                return Responses::ERROR_INCORRECT_PASSWORD;
            }

            //@todo: is active error response
            if (!$customer->getIsActive() == 1) {
                return Responses::ERROR_INCORRECT_PASSWORD;
            }

            return $customer;
        }

        return Responses::ERROR_NO_SUCH_USERNAME;
    }

    public static function getSessionKey($username, $password = null, $customer = null, $deviceToken = '')
    {
        if ($customer == null) {
            $customer = self::checkAuth($username, $password);
        }

        if (!is_object($customer)) {
            return $customer;
        }

        $sessionKey = self::generateSessionKey($customer->getId());
        $userDeviceToken = self::updateDeviceTokens($customer->getId(), $deviceToken);
        if (method_exists($customer, 'getDataByKey')) {
            $array = explode(',', $customer->getDataByKey(Constants::ATTRIBUTE_KARAVANIUM_ROLE));
        } else {
            $karavaniumRole = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_ROLE);
            $array = explode(',', empty($karavaniumRole) ? '' : $karavaniumRole->getValue());
        }

        $userRoles = [];
        for ($i = 0, $count = count($array); $i < $count; $i++) {
            $userRoles[] = ['code' => $array[$i]];
        }

        return [
            Responses::SESSION_KEY  => $sessionKey,
            Responses::USER_ROLE    => $userRoles,
            Responses::USER_ID      => $customer->getId(),
            'device_token'          => $userDeviceToken,
        ];
    }

    public static function checkSessionKey($userId, $key)
    {
        if ($userId < 1) {
            self::addFailedAttempt();
            return false;
        }

        $customerRepository = Tools::getObjectManager()->create('Magento\Customer\Api\CustomerRepositoryInterface');

        try {
            $customer = $customerRepository->getById($userId);
        } catch (\Exception $e) {
            self::addFailedAttempt();
            return false;
        }

        $currentSessionKey = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_SESSION_KEY);
        $currentSessionKeyValue = empty($currentSessionKey) ? '' : $currentSessionKey->getValue();
        $currentLastActivity = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_LAST_ACTIVITY);
        $currentLastActivityValue = empty($currentLastActivity) ? '' : $currentLastActivity->getValue();

        if (!empty($currentLastActivityValue) && !empty($currentSessionKeyValue) && $currentSessionKeyValue != $key) {
            self::addFailedAttempt();
            return false;
        }

        if ($currentLastActivityValue < date('Y-m-d H:i:s', (time() - Constants::MAX_LIFETIME))) {
            self::addFailedAttempt();
            return false;
        }

        $customer->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_LAST_ACTIVITY, date('Y-m-d H:i:s'));
        try {
            $customerRepository->save($customer);
        } catch (\Exception $e) {
            self::addFailedAttempt();
            return false;
        }

        return $customer;
    }

    public static function generateSessionKey($customer_id)
    {
        $customerRepository = Tools::getObjectManager()->create('Magento\Customer\Api\CustomerRepositoryInterface');

        $customer = $customerRepository->getById($customer_id);
        $currentSessionKey = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_SESSION_KEY);
        $currentLastActivity = $customer->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_LAST_ACTIVITY);
        $currentSessionKeyValue = empty($currentSessionKey) ? '' : $currentSessionKey->getValue();
        $currentLastActivityValue = empty($currentLastActivity) ? '' : $currentLastActivity->getValue();

        if (!empty($currentLastActivityValue) && !empty($currentSessionKeyValue) && $currentLastActivityValue >= date('Y-m-d H:i:s', (time() - Constants::MAX_LIFETIME))) {
            $customer->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_LAST_ACTIVITY, date('Y-m-d H:i:s'));
            try {
                $customerRepository->save($customer);
            } catch (\Exception $e) {
                self::addFailedAttempt();
                return Responses::ERROR_CAN_NOT_SAVE_USER;
            }

            return $currentSessionKeyValue;
        }

        $newSessionKey = hash(Constants::HASH_ALGORITHM, microtime() . rand());

        $customer
            ->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_SESSION_KEY, $newSessionKey)
            ->setCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_LAST_ACTIVITY, date('Y-m-d H:i:s'));

        try {
            $customerRepository->save($customer);
        } catch (\Exception $e) {
            self::addFailedAttempt();
            return false;
        }

        return $newSessionKey;
    }

    private static function updateDeviceTokens($userId, $deviceToken)
    {
        $userDeviceTokensFactory = Tools::getObjectManager()->create('Emagicone\Karavaniumconnector\Model\UserDeviceTokens');

        /** @var \Emagicone\Karavaniumconnector\Model\UserDeviceTokens $userDeviceTokensFactory */

        $specifiedDeviceToken = $userDeviceTokensFactory
            ->getCollection()
            ->addFieldToFilter(Constants::COLUMN_USER_ID, ['eq' => $userId])
            ->addFieldToFilter(Constants::COLUMN_DEVICE_TOKEN, ['eq' => $deviceToken]);

        if ($specifiedDeviceToken->getSize() < 1) {
            return self::saveUserDeviceToken($userId, $deviceToken, $userDeviceTokensFactory);
        }

        return false;
    }

    private static function saveUserDeviceToken($userId, $deviceToken, $deviceTokenFactory)
    {
        try {
            $deviceTokenFactory->setData(
                [
                    Constants::COLUMN_USER_ID => $userId,
                    Constants::COLUMN_DEVICE_TOKEN => $deviceToken
                ]
            );
            $deviceTokenFactory->save();
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public static function clearOldData()
    {
        $timestamp = time();
        $dateClearPrev = Tools::getConfigValue(Constants::CONFIG_PATH_CLEAR_DATE);
        $date = date('Y-m-d H:i:s', ($timestamp - Constants::MAX_LIFETIME));

        if (!$dateClearPrev || ($timestamp - (int)$dateClearPrev) > Constants::MAX_LIFETIME) {

            // Delete old failed logins
            $attempts = Tools::getObjectManager()->create('Emagicone\Karavaniumconnector\Model\FailedLogin')
                ->getCollection()
                ->addFieldToFilter('date_added', ['lt' => $date]);
            foreach ($attempts as $attempt) {
                $attempt->delete();
            }

            // Update clearing date in core_config_data table
            Tools::saveConfigValue(
                Constants::CONFIG_PATH_CLEAR_DATE,
                $timestamp,
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT
            );
        }
    }

    public static function addFailedAttempt()
    {
        $timestamp = time();

        // Add data to database
        $failedCollection = Tools::getObjectManager()->create('Emagicone\Karavaniumconnector\Model\FailedLogin');

        $failedCollection->setData(['ip' => $_SERVER['REMOTE_ADDR'], 'date_added' => $timestamp])
            ->save();

        // Get count of failed attempts for last time and set delay
        $attempts = $failedCollection->getCollection()
            ->addFieldToFilter('date_added', ['gt' => ($timestamp - Constants::MAX_LIFETIME)])
            ->addFieldToFilter('ip', ['eq' => $_SERVER['REMOTE_ADDR']]);

        self::setDelay((int)$attempts->getSize());
    }

    private static function setDelay($count_attempts)
    {
        sleep(0);
//        if ($count_attempts > 3 && $count_attempts <= 10) {
//            sleep(1);
//        } elseif ($count_attempts <= 20) {
//            sleep(5);
//        } elseif ($count_attempts <= 50) {
//            sleep(15);
//        } else {
//            sleep(30);
//        }
    }
}
