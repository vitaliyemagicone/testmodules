<?php
/**
 *    This file is part of Karavanium Connector.
 *
 *   Karavanium Connector is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Karavanium Connector is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Karavanium Connector.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Emagicone\Karavaniumconnector\Helper;

/**
 * Class Constants
 * @package Emagicone\Karavaniumconnector\Helper
 */
class Constants
{
    const HASH_ALGORITHM                        = 'sha256';
    const MAX_LIFETIME                          = 3600;
    const PRODUCT_PAGE_LIMIT                    = 20;
    const VENDOR_PAGE_LIMIT                     = 50;

    const PARAM_ACTION                          = 'action';
    const PARAM_LOGIN                           = 'login';
    const PARAM_CHECK_USER_EXISTS               = 'check_user_exists';
    const PARAM_REGISTRATION                    = 'register';
    const PARAM_VERIFY_CODE                     = 'verify_code';
    const PARAM_RESEND_VERIFICATION_CODE        = 'resend_verification_code';
    const PARAM_SUPER_BETA_TEST                 = 'run_super_beta_test';

    const ALLOWED_ACTIONS = [
        self::PARAM_LOGIN,
        self::PARAM_REGISTRATION,
        self::PARAM_CHECK_USER_EXISTS
    ];
    const ACTION_CALL = [
        self::PARAM_LOGIN => 'userLogin',
        self::PARAM_REGISTRATION => 'userRegister',
        self::PARAM_CHECK_USER_EXISTS => 'checkUserExists'
    ];

    const TABLE_FAILED_LOGIN                    = 'karavanium_failed_login';
    const TABLE_USER_DEVICE_TOKENS              = 'karavanium_user_device_tokens';
    const TABLE_TAGS                            = 'karavanium_tags';
    const TABLE_TAG_GROUPS                      = 'karavanium_tag_groups';
    const TABLE_TAG_TO_GROUP                    = 'karavanium_tag_to_group';
    const TABLE_TAG_TO_PRODUCT                  = 'karavanium_tag_to_product';
    const TABLE_TAG_GROUP_TO_PRODUCT            = 'karavanium_tag_group_to_product';
    const TABLE_VENDOR_RATING                   = 'karavanium_vendor_rating';
    const TABLE_VENDOR_ORDERS                   = 'karavanium_vendor_orders';
    const TABLE_USER_TO_TAG                     = 'karavanium_user_to_tag';
    const TABLE_USER_TO_VENDOR                  = 'karavanium_user_to_vendor';

    const TABLE_CATALOG_PRODUCT_ENTITY          = 'catalog_product_entity';
    const TABLE_CATALOG_PRODUCT_ENTITY_VARCHAR  = 'catalog_product_entity_varchar';
    const TABLE_CUSTOMER_ENTITY                 = 'customer_entity';
    const TABLE_CUSTOMER_ENTITY_VARCHAR         = 'customer_entity_varchar';

    const COLUMN_ID                             = 'id';
    const COLUMN_IP                             = 'ip';
    const COLUMN_DATE_ADDED                     = 'date_added';
    const COLUMN_VENDOR_ID                      = 'vendor_id';
    const COLUMN_RATING                         = 'rating';
    const COLUMN_TAG_ID                         = 'tag_id';
    const COLUMN_DEVICE_TOKEN                   = 'device_token';
    const COLUMN_GROUP_ID                       = 'group_id';
    const COLUMN_USER_ID                        = 'user_id';
    const COLUMN_PRODUCT_ID                     = 'product_id';
    const COLUMN_ENTITY_ID                      = 'entity_id';
    const COLUMN_VALUE                          = 'value';
    const COLUMN_NAME                           = 'name';
    const COLUMN_PRICE                          = 'price';
    const COLUMN_QTY                            = 'qty';
    const COLUMN_STATUS                         = 'status';
    const COLUMN_IS_IN_STOCK                    = 'is_in_stock';
    const COLUMN_IMAGES                         = 'images';
    const COLUMN_STATUS_CODE                    = 'status_code';
    const COLUMN_SKU                            = 'sku';
    const COLUMN_ORDER_ID                       = 'order_id';
    const COLUMN_CREATED_AT                     = 'created_at';
    const COLUMN_CUSTOMER_ID                    = 'customer_id';

    const ATTRIBUTE_KARAVANIUM_ROLE             = 'karavanium_role';
    const ATTRIBUTE_KARAVANIUM_SESSION_KEY      = 'karavanium_session_key';
    const ATTRIBUTE_KARAVANIUM_LAST_ACTIVITY    = 'karavanium_last_activity';
    const ATTRIBUTE_KARAVANIUM_USERNAME         = 'karavanium_username';
    const ATTRIBUTE_KARAVANIUM_USER_ID          = 'karavanium_user_id';
    const ATTRIBUTE_KARAVANIUM_SKU              = 'karavanium_sku';
    const ATTRIBUTE_KARAVANIUM_AVATAR           = 'karavanium_avatar';
    const ATTRIBUTE_KARAVANIUM_RATING           = 'karavanium_rating';
    const ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS = 'karavanium_favorite_vendors';
    const ATTRIBUTE_KARAVANIUM_VENDOR_NAME      = 'karavanium_vendor_name';
    const ATTRIBUTE_KARAVANIUM_VENDOR_COUNTRY   = 'karavanium_vendor_country';
    const ATTRIBUTE_KARAVANIUM_IS_NEW           = 'karavanium_is_new';

    const CONFIG_PATH_CLEAR_DATE                = 'emagicone/karavaniumconnector/access/cl_date';
    const CONFIG_PATH_API_KEY                   = 'emagicone/karavaniumconnector/access/api_key';

    const VALIDATION_TYPE_ENUM = [
        'username' => 'STR',
        'password' => 'STR',
        'user_id' => 'STR',
        'key' => 'STR',
        'action' => 'STR',
        'data' => 'HTML_REQUEST',
        'page' => 'INT',
        'user_role' => 'STR',
        'vendor_id' => 'STR',
        'user_avatar' => 'STR',
        'name' => 'STR',
        'sort_by' => 'STR',
        'order_by' => 'STR',
        'val' => 'STR',
        'product_id' => 'STR',
        'qty' => 'STR',
        'product_ids' => 'STR',
        'all' => 'INT',
        'address_id' => 'INT',
        'address_ids' => 'STR',
        'code' => 'STR',
        'address' => 'HTML_REQUEST',
        'carrier' => 'HTML_REQUEST',
        'shipping_method' => 'HTML_REQUEST',
        'text' => 'STR',
        'order_id' => 'STR',
        'rating' => 'STR',
        'image_size' => 'STR',
        'page_size' => 'STR',
        'tag_id' => 'STR',
        'ids' => 'STR',
    ];

    const AUTH_REQUEST_VALUES = [
        Responses::USERNAME,
        Responses::PASSWORD,
        Responses::USER_ID,
        Responses::KEY,
        Responses::USER_ROLE,
        Responses::CODE,
        Responses::DEVICE_TOKEN
    ];
}
