<?php
/**
 *    This file is part of Karavanium Connector.
 *
 *   Karavanium Connector is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Karavanium Connector is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Karavanium Connector.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Emagicone\Karavaniumconnector\Helper;

/**
 * Class Responses
 * @package Emagicone\Karavaniumconnector\Helper
 */
class Responses
{

    const USER_ID                           = 'user_id';
    const USERNAME                          = 'username';
    const NAME                              = 'name';
    const USER_ROLE                         = 'user_role';
    const USER_AVATAR                       = 'user_avatar';
    const FIRST_NAME                        = 'firstname';
    const KEY                               = 'key';
    const CODE                              = 'code';
    const DEVICE_TOKEN                      = 'device_token';
    const LAST_NAME                         = 'lastname';
    const EMAIL                             = 'email';
    const MAX_FILE_UPLOAD_IN_BYTES          = 'max_file_upload_in_bytes';
    const SESSION_KEY                       = 'session_key';
    const SUCCESS                           = 'success';
    const PASSWORD                          = 'password';
    const FULLNAME                          = 'fullname';
    const VERIFICATION_CODE_SENT            = 'verification_code_sent';
    const VERIFICATION_REQUIRED             = 'verification_required';
    const TAGS                              = 'tags';
    const PRODUCT                           = 'product';
    const TAG_ID                            = 'tag_id';
    const TAG_TITLE                         = 'tag_title';

    const KEY_USER_ROLE                         = 'user_role';
    const KEY_FIRST_NAME                        = 'firstname';
    const KEY_KEY                               = 'key';
    const KEY_CODE                              = 'code';
    const KEY_LAST_NAME                         = 'lastname';
    const KEY_MAX_FILE_UPLOAD_IN_BYTES          = 'max_file_upload_in_bytes';
    const KEY_SESSION_KEY                       = 'session_key';
    const KEY_PASSWORD                          = 'password';
    const KEY_VERIFICATION_CODE_SENT            = 'verification_code_sent';
    const KEY_VERIFICATION_REQUIRED             = 'verification_required';
    const KEY_TAGS                              = 'tags';
    const KEY_TAG_ID                            = 'tag_id';
    const KEY_TAG_TITLE                         = 'tag_title';
    const KEY_SUCCESS                           = 'success';
    const KEY_PRODUCT_ID                        = 'product_id';
    const KEY_PRODUCT                           = 'product';
    const KEY_PRODUCTS                          = 'products';
    const KEY_PRODUCTS_COUNT                    = 'products_count';
    const KEY_SKU                               = 'sku';
    const KEY_NAME                              = 'name';
    const KEY_DESCRIPTION                       = 'description';
    const KEY_STATUS                            = 'status';
    const KEY_QTY                               = 'qty';
    const KEY_PRICE                             = 'price';
    const KEY_FORMATTED_PRICE                   = 'formatted_price';
    const KEY_IMAGES                            = 'images';
    const KEY_IMAGE_ID                          = 'image_id';
    const KEY_IMAGE_URL                         = 'image_url';
    const KEY_BASE_IMAGE                        = 'base_image';
    const KEY_URL                               = 'url';
    const KEY_EMAIL                             = 'email';
    const KEY_USER_ID                           = 'user_id';
    const KEY_USERNAME                          = 'username';
    const KEY_FIRSTNAME                         = 'firstname';
    const KEY_LASTNAME                          = 'lastname';
    const KEY_FULLNAME                          = 'fullname';
    const KEY_REMOVE_AVATAR                     = 'remove_avatar';
    const KEY_USER_AVATAR                       = 'user_avatar';
    const KEY_VENDOR_ID                         = 'vendor_id';
    const KEY_VENDOR_NAME                       = 'vendor_name';
    const KEY_VENDOR_COUNTRY                    = 'vendor_country';
    const KEY_VENDOR_RATING                     = 'vendor_rating';
    const KEY_VENDOR_AVATAR                     = 'vendor_avatar';
    const KEY_VENDOR_SUBSCRIBED                 = 'vendor_subscribed';
    const KEY_VENDORS                           = 'vendors';
    const KEY_VENDORS_COUNT                     = 'vendors_count';
    const KEY_ADDRESS                           = 'address';
    const KEY_ADDRESSES                         = 'addresses';
    const KEY_ADDRESSES_COUNT                   = 'addresses_count';
    const KEY_ADDRESS_ID                        = 'address_id';
    const KEY_REGION                            = 'region';
    const KEY_REGION_ID                         = 'region_id';
    const KEY_REGION_CODE                       = 'region_code';
    const KEY_REGIONS                           = 'regions';
    const KEY_COUNTRY_ID                        = 'country_id';
    const KEY_COUNTRY                           = 'country';
    const KEY_STREETS                           = 'streets';
    const KEY_COMPANY                           = 'company';
    const KEY_TELEPHONE                         = 'telephone';
    const KEY_FAX                               = 'fax';
    const KEY_POSTCODE                          = 'postcode';
    const KEY_CITY                              = 'city';
    const KEY_MIDDLENAME                        = 'middlename';
    const KEY_PREFIX                            = 'prefix';
    const KEY_SUFFIX                            = 'suffix';
    const KEY_VAT_ID                            = 'vat_id';
    const KEY_CUSTOMER_ID                       = 'customer_id';
    const KEY_IS_DEFAULT_SHIPPING               = 'is_default_shipping';
    const KEY_IS_DEFAULT_BILLING                = 'is_default_billing';
    const KEY_VALUE                             = 'value';
    const KEY_COUNTRIES                         = 'countries';
    const KEY_FAVORITE                          = 'favorite';
    const KEY_SHIPPING_METHOD                   = 'shipping_method';
    const KEY_SAVE_IN_ADDRESS_BOOK              = 'save_in_address_book';
    const KEY_SHIPPING_ADDRESS                  = 'shipping_address';
    const KEY_QTY_ORDERED                       = 'qty_ordered';
    const KEY_PRICE_TOTAL                       = 'price_total';
    const KEY_FORMATTED_PRICE_TOTAL             = 'formatted_price_total';
    const KEY_ORDER_ID                          = 'order_id';
    const KEY_ORDER_NUMBER                      = 'order_number';
    const KEY_DATE_CREATED                      = 'date_created';
    const KEY_COMMENT                           = 'comment';
    const KEY_ITEMS_COUNT                       = 'items_count';
    const KEY_ITEM_GROUPS                       = 'item_groups';
    const KEY_TOTAL                             = 'total';
    const KEY_FORMATTED_TOTAL                   = 'formatted_total';
    const KEY_ITEMS                             = 'items';
    const KEY_ORDER                             = 'order';
    const KEY_ORDERS                            = 'orders';
    const KEY_ORDER_COUNT                       = 'order_count';
    const KEY_AVAILABLE_FOR_ORDER               = 'available_for_order';
    const KEY_HAS_IMAGE                         = 'has_image';

    const ERROR                                 = 'error';
    const ERROR_NO_USER_ID                      = [self::ERROR => 'no_user_id'];
    const ERROR_NO_SUCH_USER_ID                 = [self::ERROR => 'no_such_user_id'];
    const ERROR_NO_DATA                         = [self::ERROR => 'no_data'];
    const ERROR_INCORRECT_FORMAT_OF_DATA        = [self::ERROR => 'incorrect_format_of_data'];
    const ERROR_NO_PRODUCT_DATA                 = [self::ERROR => 'no_product_data'];
    const ERROR_NO_SUCH_PRODUCT                 = [self::ERROR => 'no_such_product'];
    const ERROR_NO_SUCH_CART                    = [self::ERROR => 'no_such_cart'];
    const ERROR_PRODUCT_NOT_ADDED               = [self::ERROR => 'product_not_added'];
    const ERROR_NO_SUCH_USERNAME                = [self::ERROR => 'no_such_username'];
    const ERROR_NO_LOGIN_DATA                   = [self::ERROR => 'no_login_data'];
    const ERROR_INCORRECT_PASSWORD              = [self::ERROR => 'incorrect_password'];
    const ERROR_AUTH_ERROR                      = [self::ERROR => 'auth_error'];
    const ERROR_SESSION_KEY_EXPIRED             = [self::ERROR => 'session_key_expired'];
    const ERROR_UNKNOWN_ACTION                  = [self::ERROR => 'unknown_action'];
    const ERROR_METHOD_MISSING                  = [self::ERROR => 'method_missing'];
    const ERROR_INCORRECT_CREDENTIALS           = [self::ERROR => 'incorrect_credentials'];
    const ERROR_ALREADY_REGISTERED              = [self::ERROR => 'already_registered'];
    const ERROR_CAN_NOT_SAVE_USER               = [self::ERROR => 'can_not_save_user'];
    const ERROR_QTY_TO_HIGH                     = [self::ERROR => 'qty_To_high'];
    const ERROR_INCORRECT_IMAGE_TYPE            = [self::ERROR => 'incorrect_image_type'];
    const ERROR_IMAGE_CANNOT_BE_SAVE            = [self::ERROR => 'image_cannot_be_save'];
    const ERROR_NO_ADDRESS_ID_SPECIFIED         = [self::ERROR => 'no_address_id_specified'];
    const ERROR_NO_VERIFICATION_REQUIRED        = [self::ERROR => 'no_verification_required'];
    const ERROR_VERIFICATION_CODE_ERROR         = [self::ERROR => 'verification_code_error'];
    const ERROR_VERIFICATION_CODE_NOT_REQUIRED  = [self::ERROR => 'verification_code_not_required'];
    const ERROR_ADDRESS_INFO_ISSUE              = [self::ERROR => 'address_info_issue'];
    const ERROR_NO_SUCH_ADDRESS_ID              = [self::ERROR => 'no_such_address_id'];
    const ERROR_EMPTY_ADDRESS_IDS               = [self::ERROR => 'empty_address_ids'];
    const ERROR_EMPTY_PRODUCT_IDS               = [self::ERROR => 'empty_product_ids'];

    const SMS_URL                           = 'https://alphasms.ua/api/http.php';
    const SMS_LOGIN                         = '';
    const SMS_PASSWORD                      = '';
    const SMS_API_KEY                       = 'b60b9ee67d4370ae55eca80e4a90631f121a1832';
    const SMS_PROTOCOL                      = 'http';
    const SMS_FROM                          = 'Karavanium';
    const SMS_CMD_SEND                      = 'send';
    const SMS_CMD_RECEIVE                   = 'receive';
    const SMS_CMD_DELETE                    = 'delete';
    const SMS_CMD_PRICE                     = 'price';
    const SMS_CMD_BALANCE                   = 'balance';

    const SMS_SEND_URL                      = self::SMS_URL . '?version=' . self::SMS_PROTOCOL . '&key=' . self::SMS_API_KEY . '&command=' . self::SMS_CMD_SEND . '&from=' . self::SMS_FROM . '&to=';
    /* all urls
    $sendMessageApi = "$requestBaseUrl?version=$requestVersion&key=$requestApiKey&command=$requestCommandSend&from=$sendFrom&to=$userTelephone&message=$userMessage";
    $sendMessage = "$requestBaseUrl?version=$requestVersion&login=$requestLogin&password=$requestPassword&command=$requestCommandSend&from=$sendFrom&to=$userTelephone&message=$userMessage";
    $statusMessage = "$requestBaseUrl?version=$requestVersion&login=$requestLogin&password=$requestPassword&command=$requestCommandReceive&id=$messageId";
    $deleteMessage = "$requestBaseUrl?version=$requestVersion&login=$requestLogin&password=$requestPassword&command=$requestCommandDelete&id=$messageId";
    $priceMessage = "$requestBaseUrl?version=$requestVersion&login=$requestLogin&password=$requestPassword&command=$requestCommandPrice&to=$userTelephone";
    $balanceMessage = "$requestBaseUrl?version=$requestVersion&login=$requestLogin&password=$requestPassword&command=$requestCommandBalance";
    */

    /*
        <======== Відправка ========>
        response:
        id:84523377 sms_count:1

        codes
        id = message_id
        sms_count = count of messages

        error
        errors:{ERROR1_TEXT}
        errors:{ERROR2_TEXT}
        errors:{ERROR#_TEXT}

        Причины возникновения ошибок:
        неправильно указан телефон получателя;
        не написали сообщение втеле message;
        SMS сообщение слишком длинное;
        отсутствуют деньги на балансе

        <======== Статус ========>
        response:
        status:code:3 status_time:год/месяц/время.

        codes
        0 - Ожидает
        1 - Отправляется
        2 - Отправлено
        3 - Доставлено
        5 - Нет на связи
        10 - Удалено
        50 - Частично доставлено
        96 - Сбой сети при доставке SMS
        99 - Ошибка в номере
        99 - Номер не обслуживается

        error
        errors:{ERROR1_TEXT}

        Причины возникновения ошибок:
        SMS сообщение не найдено

        <======== Видалення ========>
        response:
        status:{STATUS_TEXT} – статус сообщения в кодировке UTF8
        code:{STATUS_CODE} - код сообщения

        codes
        0 - Ожидает
        1 - Отправляется
        2 - Отправлено
        3 - Доставлено
        5 - Нет на связи
        10 - Удалено
        50 - Частично доставлено
        96 - Сбой сети при доставке SMS
        99 - Ошибка в номере
        99 - Номер не обслуживается

        error
        errors:{ERROR1_TEXT}

        Причины возникновения ошибок:
        - SMS сообщение не может быть удалено

        <======== Ціна ========>
        response:
        price:{SMS_PRICE} – цена за SMS сообщения
        currency:{CURRENCY_CODE} – валюта стоимости SMS сообщения

        Примечание: Если цена не указана – доставка на запрашиваемый номер не производится.

        Баланс ========>
        response:
        balane:{AMOUNT}

        Причины возникновения ошибок:
        - неправильный логин/пароль;
        - неизвестная команда.
        */

    const TEST_ADD_PRODUCT          = '{
                                          "product_id" : 54,
                                          "entity_type" : "simple",
                                          "name" : "TestProductName123",
                                          "status" : "1",
                                          "weight" : "1",
                                          "visibility" : "4",
                                          "images" : [
                                                      {"image_id":"7"}
                                          ],
                                          "tax_class" : "0",
                                          "price" : "10.123",
                                          "cost" : "9.123",
                                          "is_in_stock" : "1",
                                          "qty" : "123",
                                          "karavanium_tag" : "test1, test2, test3",
                                          "karavanium_user_id" : "1",
                                          "tags" : [
                                          ]
                                        }';

    const TEST_ORDER                = '{
                                         "customer_id" : "1",
                                         "currency_id" : "USD",
                                         "carrier" : {
                                           "carrier_code" : "ups",
                                           "method_code" : "GND"
                                         },
                                         "shipping_method" : {
                                           "method_code" : "3DS"
                                         },
                                         "shipping_address" : {
                                           "firstname" : "Vitalii",
                                           "lastname" : "Test",
                                           "company" : "eMagicOne",
                                           "street" : "123 Demo",
                                           "city" : "Mageplaza",
                                           "region" : "New York",
                                           "country_id" : "US",
                                           "region_code" : "NY",
                                           "region_id" : "43",
                                           "postcode" : "10001",
                                           "telephone" : "666999666",
                                           "fax" : "666999666"
                                         },
                                         "items" : [
                                           {
                                             "product_id" : 1,
                                             "qty" : 1,
                                             "price" : "111.00"
                                           },
                                           {
                                             "product_id" : 2,
                                             "qty" : 2,
                                             "price" : "111.00"
                                           }
                                         ],
                                         "status" : "4"
                                       }';

//carrier={"carrier_code":"ups","method_code":"GND"}&shipping_method={"method_code":"3DS"}
//carrier={"carrier_code":"freeshipping","method_code":"freeshipping"}&shipping_method=

    const TEST_UPDATE_PROFILE       = '{
                                        "firstname" : "Vitalii",
                                        "lastname" : "Vasylyk",
                                        "email" : "kobseeu@gmail.com",
                                        "remove_avatar" : "0"
                                       }';

    const TEST_CREATE_ADDRESS       = '{
	                                    "address_id" : "41",
                                        "user_id" : "1",
                                        "firstname" : "Vitalii",
                                        "lastname" : "Test",
                                        "company" : "eMagicOne",
                                        "streets" : [
                                                    { "name" : "123 Demo"},
                                                    { "name" : "234 Demo"}
                                                    ],
                                        "city" : "Mageplaza",
                                        "region" : "",
                                        "country_id" : "US",
                                        "region_code" : "NY",
                                        "region_id" : "43",
                                        "postcode" : "10001",
                                        "telephone" : "666999666",
                                        "fax" : "666999666",
                                        "is_default_shipping" : "1",
                                        "is_default_billing" : "1"
                                      }'; // region_id - state/province, only needed if the country is USA
    const TEST_SM_ADDRESS       = '{
                                        "firstname" : "Vitalii",
                                        "lastname" : "Test",
                                        "company" : "eMagicOne",
                                        "streets" : [
                                                    { "name" : "123 Demo"},
                                                    { "name" : "234 Demo"}
                                                    ],
                                        "city" : "Mageplaza",
                                        "region" : "New York",
                                        "country_id" : "US",
                                        "region_code" : "NY",
                                        "region_id" : "43",
                                        "postcode" : "10001",
                                        "telephone" : "666999666",
                                        "fax" : "666999666"

                                      }';

}
