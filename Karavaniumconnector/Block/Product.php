<?php

namespace Emagicone\Karavaniumconnector\Block;

use Emagicone\Karavaniumconnector\Helper\Constants;
use Emagicone\Karavaniumconnector\Helper\Responses;
use Emagicone\Karavaniumconnector\Helper\Tools;
use Magento\Framework\Exception\FileSystemException;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\View\Element\Template;
use Magento\Framework\Filesystem                        as FileSystem;
use Magento\Framework\App\Filesystem\DirectoryList      as DirectoryList;
use Magento\Catalog\Model\Product                       as ProductModel;
use Magento\Catalog\Api\ProductRepositoryInterface      as ProductRepository;
use Magento\Framework\UrlInterface                      as UrlInterface;

class Product extends Template
{
    private     $id;

    protected   $productRepository;
    protected   $urlInterface;
    protected   $fileSystem;

    public function __construct(
        ProductRepository               $productRepository,
        UrlInterface                    $urlInterface,
        FileSystem                      $fileSystem,
        Context                         $context
    ) {
        $this->productRepository      = $productRepository;
        $this->urlInterface           = $urlInterface;
        $this->fileSystem             = $fileSystem;

        parent::__construct($context);
    }


    private function getProductObject()
    {
        if ($this->getRequest()->has(Constants::COLUMN_ID)) {
            $this->id = $this->getRequest()->get(Constants::COLUMN_ID);
        }

        if (empty($productId)) {
            $productId = $this->id;
        }

        try {
            return $this->productRepository->getById($productId);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_PRODUCT;
        }
    }

    private function getName()
    {
        $product = $this->getProductObject();
        $productName = '';
        if ($product instanceof ProductModel) {
            $productName = $product->getName();
        }

        return $productName;
    }

    private function getDescription()
    {
        $product = $this->getProductObject();
        $productDescription = '';
        if ($product instanceof ProductModel) {
            $productDescription = $product->getDescription();
        }

        return $productDescription;
    }

    private function getPrice()
    {
        $product = $this->getProductObject();
        $productPrice = 0;
        if ($product instanceof ProductModel) {
            $productPrice = $product->getPrice();
        }

        return Tools::getConvertedAndFormattedPrice($productPrice, '', '');
    }



    private function getSku()
    {
        $product = $this->getProductObject();
        $productSku = '';
        if ($product instanceof ProductModel) {
            $productSku = !empty($customSku = $product->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_SKU))
                ? $customSku->getValue()
                : '';
        }

        return $productSku;
    }

    private function getImage()
    {
        $product     = $this->getProductObject();
        $baseImage   = [];
        $otherImages = [];
        if ($product instanceof ProductModel) {
            $baseImageId = 0;
            $mediaEntity = $product->getMediaGalleryEntries();
            if (!empty($mediaEntity)) {
                foreach ($product->getMediaGalleryEntries() as $media) {
                    if (!in_array('image', $imageType = $media->getTypes())) {
                        continue;
                    }
                    $baseImageId = $media->getId();
                }
            }

            $mediaGallery = $product->getMediaGalleryImages();
            if (!empty($mediaGallery) && !empty($mediaEntity)) {
                foreach ($mediaGallery->getItems() as $mediaItem) {
                    if ($mediaItem->getValueId() == $baseImageId) {
                        $isImageBase = ($mediaItem->getValueId() == $baseImageId) ? 1 : 0;
                        $baseImage[] = [
                            Responses::KEY_PRODUCT_ID   => $product->getId(),
                            Responses::KEY_IMAGE_ID     => $mediaItem->getValueId(),
                            Responses::KEY_BASE_IMAGE   => $isImageBase,
                            Responses::KEY_URL          => $mediaItem->getUrl()
                        ];
                        continue;
                    }
                    $isImageBase = ($mediaItem->getValueId() == $baseImageId) ? 1 : 0;
                    $otherImages[] = [
                        Responses::KEY_PRODUCT_ID   => $product->getId(),
                        Responses::KEY_IMAGE_ID     => $mediaItem->getValueId(),
                        Responses::KEY_BASE_IMAGE   => $isImageBase,
                        Responses::KEY_URL          => $mediaItem->getUrl()
                    ];
                }
            }

        }

        return count($baseImage) > 0 ? $baseImage[0][Responses::KEY_URL] : (count($otherImages)
            ? $otherImages[0][Responses::KEY_URL] : '');
    }

    private function getProductApplicationUrl()
    {
        $product = $this->getProductObject();
        $productId = 0;
        if ($product instanceof ProductModel) {
            $productId = $product->getId();
        }

        return 'karavanium://share.product?id=' . $productId;
    }

    public function getPageEntity()
    {
        return 'Product';
    }

    public function getPageTitle()
    {
        return $this->getName();
    }

    public function getPageDescription()
    {
        return $this->getDescription();
    }

    public function getPageAdditionalInfo()
    {
        return $this->getPrice();
    }

    public function getPageLogoUrl()
    {
        return getcwd().'img/karavanium.png';
    }

    public function getPageImageUrl()
    {
        return $this->getImage();
    }

    public function getPageApplicationUrl()
    {
        return $this->getProductApplicationUrl();
    }

}

