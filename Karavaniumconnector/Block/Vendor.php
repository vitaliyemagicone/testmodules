<?php

namespace Emagicone\Karavaniumconnector\Block;

use Emagicone\Karavaniumconnector\Helper\Constants;
use Emagicone\Karavaniumconnector\Helper\Responses;
use Magento\Customer\Model\Data\Customer;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\View\Element\Template;
use Magento\Customer\Api\CustomerRepositoryInterface    as CustomerRepository;
use Magento\Framework\UrlInterface                      as UrlInterface;
use Magento\Framework\Api\FilterBuilder                 as FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder         as SearchCriteriaBuilder;

class Vendor extends Template
{
    private     $id;

    protected   $productRepository;
    protected   $customerRepository;
    protected   $searchCriteriaBuilder;
    protected   $filterBuilder;
    protected   $urlInterface;

    public function __construct(
        CustomerRepository              $customerRepository,
        SearchCriteriaBuilder           $searchCriteriaBuilder,
        FilterBuilder                   $filterBuilder,
        UrlInterface                    $urlInterface,
        Context                         $context
    ) {
        $this->customerRepository     = $customerRepository;
        $this->searchCriteriaBuilder  = $searchCriteriaBuilder;
        $this->filterBuilder          = $filterBuilder;
        $this->urlInterface           = $urlInterface;

        parent::__construct($context);
    }


    private function getVendorObject()
    {
        if ($this->getRequest()->has(Constants::COLUMN_ID)) {
            $this->id = $this->getRequest()->get(Constants::COLUMN_ID);
        }

        if (empty($userId)) {
            $userId = $this->id;
        }

        try {
            return $this->customerRepository->getById($userId);
        } catch (\Exception $e) {
            return Responses::ERROR_NO_SUCH_USER_ID;
        }
    }

    private function getFirstName()
    {
        $vendor = $this->getVendorObject();
        $vendorFirstName = '';
        if ($vendor instanceof Customer) {
            $vendorFirstName = $vendor->getFirstname();
        }

        return $vendorFirstName;
    }

    private function getLastName()
    {
        $vendor = $this->getVendorObject();
        $vendorLastName = '';
        if ($vendor instanceof Customer) {
            $vendorLastName = $vendor->getLastname();
        }

        return $vendorLastName;
    }

    private function getUserName()
    {
        $vendor = $this->getVendorObject();
        $vendorUserName = '';
        if ($vendor instanceof Customer) {
            $vendorUserName = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_USERNAME);
        }

        return !empty($vendorUserName) ? $vendorUserName->getValue() : '';
    }

    private function getVendorName()
    {
        $vendor = $this->getVendorObject();
        $vendorName = '';
        if ($vendor instanceof Customer) {
            $vendorName = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME);
        }

        return !empty($vendorName) ? $vendorName->getValue() : '';
    }

    private function getVendorAvatar()
    {
        $vendor = $this->getVendorObject();
        if ($vendor instanceof Customer) {
            $vendorAvatar = $vendor->getCustomAttribute(Constants::ATTRIBUTE_KARAVANIUM_AVATAR);
        }
        $vendorAvatarValue = !empty($vendorAvatar) ? $vendorAvatar->getValue() : '';

        $vendorAvatarUrl = '';
        if (!empty($vendorAvatarValue)) {
            $vendorAvatarUrl = $this->urlInterface->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $vendorAvatarValue;
        }

        return $vendorAvatarUrl;
    }

    private function getVendorApplicationUrl()
    {
        $vendor = $this->getVendorObject();
        $vendorId = 0;
        if ($vendor instanceof Customer) {
            $vendorId = $vendor->getId();
        }

        return 'karavanium://share.vendor?id=' . $vendorId;
    }

    private function getVendorSubscriptionCount()
    {
        $vendor = $this->getVendorObject();
        $vendorId = 0;
        if ($vendor instanceof Customer) {
            $vendorId = $vendor->getId();
        }

        $subscribers = $this->getUserIdsForVendor($vendorId);
        return count($subscribers) . " subscribers";
    }

    private function getUserIdsForVendor($vendorId)
    {
        $vendorFilterOne = $this->filterBuilder
            ->setField(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS)
            ->setConditionType('like')
            ->setValue("% $vendorId,%")
            ->create();
        $vendorFilterTwo = $this->filterBuilder
            ->setField(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS)
            ->setConditionType('like')
            ->setValue("$vendorId")
            ->create();
        $vendorFilterThree = $this->filterBuilder
            ->setField(Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS)
            ->setConditionType('like')
            ->setValue("%$vendorId,%")
            ->create();

        $this->searchCriteriaBuilder->addFilters([$vendorFilterOne, $vendorFilterTwo, $vendorFilterThree]);
        $customerList = $this->customerRepository->getList($this->searchCriteriaBuilder->create());

        $subscriberList = [];
        foreach ($customerList->getItems() as $customer) {
            $subscriberList[] = $customer->getId();
        }

        return $subscriberList;
    }

    public function getPageEntity()
    {
        return 'Vendor';
    }

    public function getPageTitle()
    {
        return $this->getVendorName() /*. ' (' . $this->getFirstName() . ' ' . $this->getLastName() . ')'*/;
    }

    public function getPageDescription()
    {
        return $this->getVendorName() /*. ' (' . $this->getFirstName() . ' ' . $this->getLastName() . ')'*/;
    }

    public function getPageAdditionalInfo()
    {
        return $this->getVendorSubscriptionCount();
    }

    public function getPageLogoUrl()
    {
        return getcwd().'img/karavanium.png';
    }

    public function getPageImageUrl()
    {
        return $this->getVendorAvatar();
    }

    public function getPageApplicationUrl()
    {
        return $this->getVendorApplicationUrl();
    }

}

