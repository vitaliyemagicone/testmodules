<?php
/**
 *    This file is part of Karavanium Connector.
 *
 *   Karavanium Connector is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Karavanium Connector is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Karavanium Connector.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Emagicone\Karavaniumconnector\Setup;

use Emagicone\Karavaniumconnector\Helper;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Catalog\Model\Product;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;

/**
 * Class InstallData
 * @package Emagicone\Karavaniumconnector\Setup
 */

class InstallData implements InstallDataInterface
{
    /**
     * Installation of custom attributes for customers and products that will be used in module.
     *
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     *
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $productSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * Init
     *
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        EavSetupFactory $productSetupFactory,
        AttributeSetFactory $attributeSetFactory

    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->productSetupFactory = $productSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        /** @var InstallData $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        /** @var InstallData $productSetup */
        $productSetup = $this->productSetupFactory->create(['setup' => $setup]);

        $productTypes = [
            \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE
        ];
        $productTypes = join(',', $productTypes);

        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $customerAttributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var $customerAttributeSet AttributeSet */
        $customerAttributeSet = $this->attributeSetFactory->create();
        $customerAttributeGroupId = $customerAttributeSet->getDefaultGroupId($customerAttributeSetId);

        $customerSetup->addAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_ROLE, [
                'type' => 'text',
                'label' => 'Karavanium Customer Role',
                'input' => 'multiselect',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => false,
                'required' => false,
                'unique' => false,
                'visible' => true,
                'searchable' => true,
                'filterable' => true,
                'filterable_in_search' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'sort_order' => 9997,
                'position' => 9997,
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'source' => 'Emagicone\Karavaniumconnector\Model\Entity\Attribute\Source\KaravaniumCustomerRole'
            ]
        );

        $customerSetup->addAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_SESSION_KEY, [
                'type' => 'varchar',
                'label' => 'Karavanium Session Key',
                'input' => 'text',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => false,
                'required' => false,
                'unique' => false,
                'visible' => true,
                'searchable' => true,
                'filterable' => true,
                'filterable_in_search' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'sort_order' => 9998,
                'position' => 9998
            ]
        );

        $customerSetup->addAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_LAST_ACTIVITY, [
                'type' => 'datetime',
                'label' => 'Karavanium Last Activity',
                'input' => 'date',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => false,
                'required' => false,
                'unique' => false,
                'visible' => true,
                'searchable' => true,
                'filterable' => true,
                'filterable_in_search' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'sort_order' => 9999,
                'position' => 9999,
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\Datetime'
            ]
        );

        $customerSetup->addAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_USERNAME, [
                'type' => 'varchar',
                'label' => 'Karavanium Username',
                'input' => 'text',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => false,
                'required' => false,
                'unique' => true,
                'visible' => true,
                'searchable' => true,
                'filterable' => true,
                'filterable_in_search' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'sort_order' => 9999,
                'position' => 9999
            ]
        );

        $customerSetup->addAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME, [
                'type' => 'varchar',
                'label' => 'Karavanium Vendor Name',
                'input' => 'text',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => false,
                'required' => false,
                'unique' => false,
                'visible' => true,
                'searchable' => true,
                'filterable' => true,
                'filterable_in_search' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'sort_order' => 9999,
                'position' => 9999
            ]
        );


        $customerSetup->addAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_VENDOR_COUNTRY, [
                'type' => 'varchar',
                'label' => 'Karavanium Vendor Country',
                'input' => 'text',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => false,
                'required' => false,
                'unique' => false,
                'visible' => true,
                'searchable' => true,
                'filterable' => true,
                'filterable_in_search' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'sort_order' => 9999,
                'position' => 9999

            ]
        );

        $customerSetup->addAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_RATING, [
                'type' => 'varchar',
                'label' => 'Karavanium Rating',
                'input' => 'text',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => false,
                'required' => false,
                'unique' => false,
                'visible' => true,
                'searchable' => true,
                'filterable' => true,
                'filterable_in_search' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'sort_order' => 9999,
                'position' => 9999
            ]
        );

        $customerSetup->addAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_AVATAR, [
                'type' => 'varchar',
                'label' => 'Karavanium Avatar',
                'input' => 'text',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => false,
                'required' => false,
                'unique' => true,
                'visible' => true,
                'searchable' => false,
                'filterable' => false,
                'filterable_in_search' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'sort_order' => 9999,
                'position' => 9999
            ]
        );

        $customerSetup->addAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS, [
                'type' => 'varchar',
                'label' => 'Karavanium Favorite Vendors',
                'input' => 'text',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => false,
                'required' => false,
                'unique' => false,
                'visible' => true,
                'searchable' => true,
                'filterable' => true,
                'filterable_in_search' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'sort_order' => 9999,
                'position' => 9999

            ]
        );

        $productSetup->addAttribute(Product::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_USER_ID, [
                'type' => 'varchar',
                'label' => 'Karavanium User ID',
                'input' => 'text',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => false,
                'required' => false,
                'unique' => false,
                'visible' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'backend' => '',
                'frontend' => '',
                'class' => '',
                'source' => '',
                'default' => 0,
                'apply_to' => $productTypes
            ]
        );

        $productSetup->addAttribute(Product::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_SKU, [
                'type' => 'varchar',
                'label' => 'Karavanium SKU',
                'input' => 'text',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'required' => false,
                'unique' => false,
                'visible' => true,
                'searchable' => true,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'backend' => '',
                'frontend' => '',
                'class' => '',
                'source' => '',
                'default' => 0,
                'apply_to' => $productTypes
            ]
        );

        $productSetup->addAttribute(Product::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_IS_NEW, [
                'type' => 'varchar',
                'label' => 'Karavanium Product View Flag',
                'input' => 'text',
                'group' => 'General',
                'system' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'required' => false,
                'unique' => false,
                'visible' => true,
                'searchable' => true,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'backend' => '',
                'frontend' => '',
                'class' => '',
                'source' => '',
                'default' => 0,
                'apply_to' => $productTypes
            ]
        );

        /** @var  $attribute */
        $customerSetup->getEavConfig()
            ->getAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_ROLE)
            ->addData(
                [
                    'attribute_set_id' => $customerAttributeSetId,
                    'attribute_group_id' => $customerAttributeGroupId,
                    'used_in_forms' => ['adminhtml_customer']
                ]
            )
            ->save();

        $customerSetup->getEavConfig()
            ->getAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_SESSION_KEY)
            ->addData(
                [
                    'attribute_set_id' => $customerAttributeSetId,
                    'attribute_group_id' => $customerAttributeGroupId,
                    'used_in_forms' => ['adminhtml_customer']
                ]
            )
            ->save();

        $customerSetup->getEavConfig()
            ->getAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_LAST_ACTIVITY)
            ->addData(
                [
                    'attribute_set_id' => $customerAttributeSetId,
                    'attribute_group_id' => $customerAttributeGroupId,
                    'used_in_forms' => ['adminhtml_customer']
                ]
            )
            ->save();

        $customerSetup->getEavConfig()
            ->getAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_USERNAME)
            ->addData(
                [
                    'attribute_set_id' => $customerAttributeSetId,
                    'attribute_group_id' => $customerAttributeGroupId,
                    'used_in_forms' => ['adminhtml_customer']
                ]
            )
            ->save();

        $customerSetup->getEavConfig()
            ->getAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_VENDOR_NAME)
            ->addData(
                [
                    'attribute_set_id' => $customerAttributeSetId,
                    'attribute_group_id' => $customerAttributeGroupId,
                    'used_in_forms' => ['adminhtml_customer']
                ]
            )
            ->save();

        $customerSetup->getEavConfig()
            ->getAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_VENDOR_COUNTRY)
            ->addData(
                [
                    'attribute_set_id' => $customerAttributeSetId,
                    'attribute_group_id' => $customerAttributeGroupId,
                    'used_in_forms' => ['adminhtml_customer']
                ]
            )
            ->save();

        $customerSetup->getEavConfig()
            ->getAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_RATING)
            ->addData(
                [
                    'attribute_set_id' => $customerAttributeSetId,
                    'attribute_group_id' => $customerAttributeGroupId,
                    'used_in_forms' => ['adminhtml_customer']
                ]
            )
            ->save();

        $customerSetup->getEavConfig()
        ->getAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_AVATAR)
        ->addData(
            [
                'attribute_set_id' => $customerAttributeSetId,
                'attribute_group_id' => $customerAttributeGroupId,
                'used_in_forms' => ['adminhtml_customer']
            ]
        )
        ->save();

        $customerSetup->getEavConfig()
            ->getAttribute(Customer::ENTITY, Helper\Constants::ATTRIBUTE_KARAVANIUM_FAVORITE_VENDORS)
            ->addData(
                [
                    'attribute_set_id' => $customerAttributeSetId,
                    'attribute_group_id' => $customerAttributeGroupId,
                    'used_in_forms' => ['adminhtml_customer']
                ]
            )
            ->save();

        $setup->endSetup();

    }

}