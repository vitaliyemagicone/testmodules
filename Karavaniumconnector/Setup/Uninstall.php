<?php
/**
 *    This file is part of Karavanium Connector.
 *
 *   Karavanium Connector is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Karavanium Connector is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Karavanium Connector.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Emagicone\Karavaniumconnector\Setup;

use Emagicone\Karavaniumconnector\Helper\Constants;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class Uninstall
 * @package Emagicone\Karavaniumconnector\Setup
 */
class Uninstall implements \Magento\Framework\Setup\UninstallInterface
{
    protected $eavSetupFactory;
    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }
    /**
     * Invoked when remove-data flag is set during module uninstall.
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $setup->getConnection()->dropTable(Constants::TABLE_DEVICES);
        $setup->getConnection()->dropTable(Constants::TABLE_PUSH_NOTIFICATIONS);
        $setup->getConnection()->dropTable(Constants::TABLE_USERS);
        $setup->getConnection()->dropTable(Constants::TABLE_FAILED_LOGIN);
        $setup->getConnection()->dropTable(Constants::TABLE_SESSION_KEYS);

        //delete customer attributes
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->removeAttribute(\Magento\Customer\Model\Customer::ENTITY, 'karavanium_role');
//        $eavSetup->removeAttribute(\Magento\Customer\Model\Customer::ENTITY, 'karavanium_session_key');
//        $eavSetup->removeAttribute(\Magento\Customer\Model\Customer::ENTITY, 'karavanium_last_activity');

        $setup->endSetup();
    }
}

