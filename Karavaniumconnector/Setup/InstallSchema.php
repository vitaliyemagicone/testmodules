<?php
/**
 *    This file is part of Mobile Assistant Connector.
 *
 *   Mobile Assistant Connector is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Mobile Assistant Connector is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Mobile Assistant Connector.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Emagicone\Karavaniumconnector\Setup;

use Emagicone\Karavaniumconnector\Helper\Constants;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class InstallSchema
 * @package Emagicone\Mobassistantconnector\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $this->createTableFailedLogin($installer);
        $this->createTableTags($installer);
        $this->createTableTagToProduct($installer);
        $this->createTableUserToTag($installer);
        $this->createTableUserToVendor($installer);
        $this->createTableVendorOrders($installer);
        $this->createTableVendorRating($installer);
        $this->createTableUserDeviceTokens($installer);

        $installer->endSetup();
    }

    /**
     * @param $installer
     */
    private function createTableFailedLogin($installer)
    {
        /** @var \Magento\Framework\Setup\SchemaSetupInterface $installer */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(Constants::TABLE_FAILED_LOGIN))
            ->addColumn(
                Constants::COLUMN_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true, 'auto_increment' => true],
                'Id'
            )
            ->addColumn(
                Constants::COLUMN_IP,
                Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Remote Ip'
            )
            ->addColumn(
                Constants::COLUMN_DATE_ADDED,
                Table::TYPE_DATETIME,
                null,
                ['nullable' => false],
                'Date Added'
            );

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param $installer
     */
    private function createTableVendorRating($installer)
    {
        /** @var \Magento\Framework\Setup\SchemaSetupInterface $installer */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(Constants::TABLE_VENDOR_RATING))
            ->addColumn(
                Constants::COLUMN_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true, 'auto_increment' => true],
                'Id'
            )
            ->addColumn(
                Constants::COLUMN_USER_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'User Id'
            )
            ->addColumn(
                Constants::COLUMN_VENDOR_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Vendor Id'
            )
            ->addColumn(
                Constants::COLUMN_RATING,
                Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Vendor Rating Value'
            );

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param $installer
     */
    private function createTableVendorOrders($installer)
    {
        /** @var \Magento\Framework\Setup\SchemaSetupInterface $installer */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(Constants::TABLE_VENDOR_ORDERS))
            ->addColumn(
                Constants::COLUMN_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true, 'auto_increment' => true],
                'Id'
            )
            ->addColumn(
                Constants::COLUMN_VENDOR_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Vendor Id'
            )
            ->addColumn(
                Constants::COLUMN_ORDER_ID,
                Table::TYPE_INTEGER ,
                null,
                ['nullable' => false],
                'User Order ID (Related to vendor)'
            );

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param $installer
     */
    private function createTableTags($installer)
    {
        /** @var \Magento\Framework\Setup\SchemaSetupInterface $installer */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(Constants::TABLE_TAGS))
            ->addColumn(
                Constants::COLUMN_TAG_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true, 'auto_increment' => true],
                'Tag Id'
            )
            ->addColumn(
                Constants::COLUMN_VALUE,
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Tag Value'
            )
            ->addIndex(
                $installer->getIdxName(
                    Constants::TABLE_TAGS,
                    [Constants::COLUMN_VALUE],
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                [Constants::COLUMN_VALUE],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment('Karavanium Tags Table. Index [value] UNIQUE.');

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param $installer
     */
    private function createTableTagToProduct($installer)
    {
        /** @var \Magento\Framework\Setup\SchemaSetupInterface $installer */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(Constants::TABLE_TAG_TO_PRODUCT))
            ->addColumn(
                Constants::COLUMN_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true, 'auto_increment' => true],
                'Id'
            )
            ->addColumn(
                Constants::COLUMN_TAG_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned' => true],
                'Tag Id'
            )
            ->addColumn(
                Constants::COLUMN_PRODUCT_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned' => true],
                'Product Id'
            )
            ->addIndex(
                $installer->getIdxName(
                    Constants::TABLE_TAG_TO_PRODUCT,
                    [Constants::COLUMN_TAG_ID]
                ),
                [Constants::COLUMN_TAG_ID]
            )
            ->addIndex(
                $installer->getIdxName(
                    Constants::TABLE_TAG_TO_PRODUCT,
                    [Constants::COLUMN_PRODUCT_ID]
                ),
                [Constants::COLUMN_PRODUCT_ID]
            )
            ->addForeignKey(
                $installer->getFkName(
                    Constants::TABLE_TAG_TO_PRODUCT,
                    Constants::COLUMN_TAG_ID,
                    Constants::TABLE_TAGS,
                    Constants::COLUMN_TAG_ID
                ),
                Constants::COLUMN_TAG_ID,
                $installer->getTable(Constants::TABLE_TAGS),
                Constants::COLUMN_TAG_ID,
                Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $installer->getFkName(
                    Constants::TABLE_TAG_TO_PRODUCT,
                    Constants::COLUMN_PRODUCT_ID,
                    Constants::TABLE_CATALOG_PRODUCT_ENTITY,
                    Constants::COLUMN_ENTITY_ID
                ),
                Constants::COLUMN_PRODUCT_ID,
                $installer->getTable(Constants::TABLE_CATALOG_PRODUCT_ENTITY),
                Constants::COLUMN_ENTITY_ID,
                Table::ACTION_CASCADE
            )
            ->setComment('Karavanium Tag To Product Table. Index [tag_id], [product_id], [tag_id, product_id] UNIQUE. Foreign keys - karavanium_tags, catalog_product_entity.');

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param $installer
     */
    private function createTableUserToTag($installer)
    {
        /** @var \Magento\Framework\Setup\SchemaSetupInterface $installer */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(Constants::TABLE_USER_TO_TAG))
            ->addColumn(
                Constants::COLUMN_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true, 'auto_increment' => true],
                'Unique Id'
            )
            ->addColumn(
                Constants::COLUMN_USER_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned' => true],
                'User Id'
            )
            ->addColumn(
                Constants::COLUMN_TAG_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned' => true],
                'Tag Id'
            )
            ->addIndex(
                $installer->getIdxName(
                    Constants::TABLE_USER_TO_TAG,
                    [Constants::COLUMN_USER_ID]
                ),
                [Constants::COLUMN_USER_ID]
            )
            ->addIndex(
                $installer->getIdxName(
                    Constants::TABLE_USER_TO_TAG,
                    [Constants::COLUMN_TAG_ID]
                ),
                [Constants::COLUMN_TAG_ID]
            )
            ->addIndex(
                $installer->getIdxName(
                    Constants::TABLE_USER_TO_TAG,
                    ['user_id', 'tag_id'],
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['user_id', 'tag_id'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->addForeignKey(
                $installer->getFkName(
                    Constants::TABLE_USER_TO_TAG,
                    Constants::COLUMN_USER_ID,
                    Constants::TABLE_CUSTOMER_ENTITY,
                    Constants::COLUMN_ENTITY_ID
                ),
                Constants::COLUMN_USER_ID,
                $installer->getTable(Constants::TABLE_CUSTOMER_ENTITY),
                Constants::COLUMN_ENTITY_ID,
                Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $installer->getFkName(
                    Constants::TABLE_USER_TO_TAG,
                    Constants::COLUMN_TAG_ID,
                    Constants::TABLE_TAGS,
                    Constants::COLUMN_TAG_ID
                ),
                Constants::COLUMN_TAG_ID,
                $installer->getTable(Constants::TABLE_TAGS),
                Constants::COLUMN_TAG_ID,
                Table::ACTION_CASCADE
            )
            ->setComment('Karavanium User To Tag Subscription relation table. Index [user_id], [tag_id] and [user_id, tag_id] UNIQUE. Foreign key - customer_entity, karavanium_tags.');

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param $installer
     */
    private function createTableUserToVendor($installer)
    {
        /** @var \Magento\Framework\Setup\SchemaSetupInterface $installer */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(Constants::TABLE_USER_TO_VENDOR))
            ->addColumn(
                Constants::COLUMN_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true, 'auto_increment' => true],
                'Unique Id'
            )
            ->addColumn(
                Constants::COLUMN_USER_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned' => true],
                'User Id'
            )
            ->addColumn(
                Constants::COLUMN_VENDOR_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned' => true],
                'Vendor Id'
            )
            ->addIndex(
                $installer->getIdxName(
                    Constants::TABLE_USER_TO_VENDOR,
                    [Constants::COLUMN_USER_ID]
                ),
                [Constants::COLUMN_USER_ID]
            )
            ->addIndex(
                $installer->getIdxName(
                    Constants::TABLE_USER_TO_VENDOR,
                    [Constants::COLUMN_VENDOR_ID]
                ),
                [Constants::COLUMN_VENDOR_ID]
            )
            ->addIndex(
                $installer->getIdxName(
                    Constants::TABLE_USER_TO_TAG,
                    ['user_id', 'vendor_id'],
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['user_id', 'vendor_id'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->addForeignKey(
                $installer->getFkName(
                    Constants::TABLE_USER_TO_VENDOR,
                    Constants::COLUMN_USER_ID,
                    Constants::TABLE_CUSTOMER_ENTITY,
                    Constants::COLUMN_ENTITY_ID
                ),
                Constants::COLUMN_USER_ID,
                $installer->getTable(Constants::TABLE_CUSTOMER_ENTITY),
                Constants::COLUMN_ENTITY_ID,
                Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $installer->getFkName(
                    Constants::TABLE_USER_TO_VENDOR,
                    Constants::COLUMN_VENDOR_ID,
                    Constants::TABLE_CUSTOMER_ENTITY,
                    Constants::COLUMN_ENTITY_ID
                ),
                Constants::COLUMN_VENDOR_ID,
                $installer->getTable(Constants::TABLE_CUSTOMER_ENTITY),
                Constants::COLUMN_ENTITY_ID,
                Table::ACTION_CASCADE
            )
            ->setComment('Karavanium User To Vendor Subscription relation table. Index [user_id], [vendor_id] and [user_id, vendor_id] UNIQUE. Foreign key - customer_entity.');

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param $installer
     */
    private function createTableUserDeviceTokens($installer)
    {
        /** @var \Magento\Framework\Setup\SchemaSetupInterface $installer */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(Constants::TABLE_USER_DEVICE_TOKENS))
            ->addColumn(
                Constants::COLUMN_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true, 'auto_increment' => true],
                'Unique Id'
            )
            ->addColumn(
                Constants::COLUMN_USER_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned' => true],
                'User Id'
            )
            ->addColumn(
                Constants::COLUMN_DEVICE_TOKEN,
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Device Token'
            )
            ->addIndex(
                $installer->getIdxName(
                    Constants::TABLE_USER_DEVICE_TOKENS,
                    [Constants::COLUMN_USER_ID]
                ),
                [Constants::COLUMN_USER_ID]
            )
            ->addIndex(
                $installer->getIdxName(
                    Constants::TABLE_USER_DEVICE_TOKENS,
                    [Constants::COLUMN_USER_ID, Constants::COLUMN_DEVICE_TOKEN],
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                [Constants::COLUMN_USER_ID, Constants::COLUMN_DEVICE_TOKEN],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->addForeignKey(
                $installer->getFkName(
                    Constants::TABLE_USER_DEVICE_TOKENS,
                    Constants::COLUMN_USER_ID,
                    Constants::TABLE_CUSTOMER_ENTITY,
                    Constants::COLUMN_ENTITY_ID
                ),
                Constants::COLUMN_USER_ID,
                $installer->getTable(Constants::TABLE_CUSTOMER_ENTITY),
                Constants::COLUMN_ENTITY_ID,
                Table::ACTION_CASCADE
            )
            ->setComment('Karavanium User Device Tokens. Index [user_id], [user_id, device_token] UNIQUE. Foreign key - customer_entity.');

        $installer->getConnection()->createTable($table);
    }
}