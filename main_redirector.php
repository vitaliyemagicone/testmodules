<?php

define('_GLOBAL_URL_', 'https://emagicone.com');
define('_WRITE_LOG_', 0);

if (_WRITE_LOG_) {
    $log = @fopen('./request_log.txt', 'a');
    fwrite($log, var_export($_SERVER, 1) . "\n");
    fwrite($log, var_export($_REQUEST, 1) . "\n");
    fwrite($log, var_export($_FILES, 1) . "\n");
    fclose($log);
}

$sm_product_code = str_replace(array("\r", "\n"), '', addslashes(htmlspecialchars(trim($_REQUEST["product_code"]))));

if (isset($_REQUEST['parent_product_code'])) {
    $sm_product_code = str_replace(array("\r", "\n"), '', addslashes(htmlspecialchars(trim($_REQUEST["parent_product_code"]))));
}

$action = str_replace(array("\r", "\n"), '', addslashes(htmlspecialchars(trim($_REQUEST["action"]))));

if (empty($sm_product_code)) {
    header("Location: " . _GLOBAL_URL_);
    exit;
}

if (empty($action)) {
    $action = 'product_page';
}

$new_url = get_url($sm_product_code, $action);


if (!$new_url) {
    $new_url = _GLOBAL_URL_;
}

if (!empty($new_url)) {
    // For testing AdWords
    // if(strpos($new_url, '?')){
    //     $new_url .= '&utm_source=storemanagerapp&utm_medium=storemanagerappbutton&utm_campaign=storemanagerapp';
    // } else {
    //     $new_url .= '?utm_source=storemanagerapp&utm_medium=storemanagerappbutton&utm_campaign=storemanagerapp';
    // }
}

if (count($_FILES) > 0) {
    send_files($new_url);
}
header("Location: " . $new_url, false, 301);
exit;

function check_params($action) {
    $params = array();
    foreach ($_REQUEST as $key => $val) {
        $val = str_replace(array("\r", "\n", "\0"), '', addslashes(trim($val)));
        if (in_array($action, array('get_update')) && $key == 'key') {
            //$val = urlencode($val); //remove for cre
            $licenseKey = $val;
            $licenseKey = base64_decode(urldecode(decrypt_request(urldecode($licenseKey))));

            if (!validateLicenseKey($licenseKey)) {
                $licenseKey = urlencode($val);
                $licenseKey = base64_decode(urldecode(decrypt_request(urldecode($licenseKey))));
            }
            $val = $licenseKey;
        }
        $params[$key] = $val;
    }
    return $params;
}

function send_files($url) {
    $attachments = array();

    //$url='https://emagicone.com/redirect/test_main_redirector.php';
    $header = array('Content-Type: multipart/form-data');

    foreach ($_FILES as $file => $name) {
        $fields = array($file => '@' . $_FILES[$file]['tmp_name']);
    }

    $token = 'NfxoS9oGjA6MiArPtwg4aR3Cp4ygAbNA2uv6Gg4m';
    $resource = curl_init();

    curl_setopt($resource, CURLOPT_URL, $url);
    curl_setopt($resource, CURLOPT_HTTPHEADER, $header);
    curl_setopt($resource, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($resource, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
    curl_setopt($resource, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($resource, CURLOPT_POST, 1);
    curl_setopt($resource, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($resource, CURLOPT_COOKIE, 'apiToken=' . $token);

    $result = json_decode(curl_exec($resource));

    $log = @fopen('./curl_log.txt', 'a');
    fwrite($log, var_export($result, 1));
    fclose($log);

    //var_dump($result);die(':-<>');
    curl_close($resource);

    return $attachments;
}

function validateLicenseKey($licenseKey) {
    $licenseKeyParts = explode('-', $licenseKey);
    $valid = count($licenseKeyParts) === 5 && preg_match('/\d+/', $licenseKeyParts[0]) === 1 && preg_match('/\d+/', $licenseKeyParts[1]) === 1 && preg_match('/\d+/', $licenseKeyParts[2]) === 1 && preg_match('/\d+/', $licenseKeyParts[3]) === 1 && preg_match('/[\da-z]+/', $licenseKeyParts[4]) === 1;

    return $valid;
}

function get_url($product_code, $action) {
    $urls = array();
    $params = check_params($action);

    $main_urls = array(

		//9132018 magemac
		"get_license_agreement_page" => 'https://emagicone.com/?req=license&id=121',
		"get_service_agreement_page" => 'https://emagicone.com/?req=license&id=163',
		"get_privacy_policy_page" => 'https://emagicone.com/?req=license&id=103',
		//9132018 end magemac
		
        "get_emagic" => _GLOBAL_URL_,
        "get_oscommerce_sunset_url" => 'https://emagicone.com/products/solutions-for-oscommerce/',
        "get_cubecart_sunset_url" => 'https://emagicone.com/products/solutions-for-cubecart/',
        "get_pinacle_sunset_url" => 'https://emagicone.com/products/pinnaclecart/',
        "get_crloaded_sunset_url" => 'https://emagicone.com/products/solutions-for-cre-loaded/',
        "get_bridge_connector_module" => 'https://prestashopmanager.com/files/bridge_connector_module.zip',
        "check_ip" => 'https://www.whatismyip.com',
        "register" => "https://license.emagicone.com/remote1.php?code=" . $params['key'] . (isset($params['force_update']) ? "&force_update=1" : ""),
        "get_license_page" => "https://license.emagicone.com",
        "get_addon_store_page" => "https://store.emagicone.com/catalogsearch/result/?q=" . $params['tag'],
        "get_addon_blog_page" => get_addon_blog_page($product_code, $params['product_code']),
        "get_addon_documentation_page" => get_addon_documentation_page($product_code, $params['product_code']),
        "get_addon_affiliate_page" => get_addon_affiliate_page($product_code, $params['product_code']),
        "get_troubleshooter" => "https://support.emagicone.com/index.php?_m=troubleshooter&_a=steps&troubleshootercatid=1&parentid=0", // coming soon
        "get_logs" => "https://license.emagicone.com/getlogs.php?hash=%s",
        "send_error_report" => "https://license.emagicone.com/getlogs.php?hash=" . $params['hash'],	
        "get_faq_tag" => "https://support.emagicone.com/index.php?/Base/Search/Index/" . $params['tag'],
        "get_home_page" => "https://license.emagicone.com/storemanager/get_info.php?key=" . $params['key'] . "&product_code=" . $product_code,
        //"get_bridge" => "https://emagicone.com/shared_files/bridge_7_66.zip",	
		
		// new bridge added 01-05-2019		
        "get_bridge" => "https://emagicone.com/shared_files/bridge_7.70.zip",		
        "get_forum" => "https://support.emagicone.comaddon_page&product_code=plugin_quickbooks_online&parent_product_code=prestashop_storemanager/",
        "get_support_center" => "https://support.emagicone.com/", //?utm_source=storemanagerapp_homepage&utm_medium=storemanagerapp_homepage&utm_campaign=storemanagerapp_homepage",
        "get_trial_key" => "https://license.emagicone.com/storemanager/trial_license_silent.php"
        . "?tlfFirstName=" . $params['tlfFirstName']
        . "&tlfLastName=" . $params['tlfLastName']
        . "&tlfEmail=" . $params['tlfEmail']
        . "&tlfComments=" . $params['tlfComments']
        . "&product_code=" . $product_code
        . "&tlfEmailHash=" . $params['tlfEmailHash']
        . ((strlen(intval($params['tlfCobranderId'])) > 0) ? "&tflCobranderId=" . $params['tflCobranderId'] : "")
    );

    if ($product_code == 'virtuemart_storemanager_2') {
        $product_code = 'virtuemart_storemanager';
    }
    if ($product_code == 'virtuemart_demo_2') {
        $product_code = 'virtuemart_demo';
    }
    if (($product_code == 'virtuemart_storemanager' || $product_code == 'virtuemart_demo') && $action == 'get_faq_tag' && strtolower($params['tag']) == 'registration') {
        $main_urls["get_faq_tag"] = "https://support.emagicone.com/search?q=registration";
    }
    if (($product_code == 'sm_woocm_primary' || $product_code == 'sm_woocm_additional') && $action == 'get_faq_tag' && strtolower($params['tag']) == 'registration') {
        $main_urls["get_faq_tag"] = "https://support.emagicone.com/328826-Knowledgebase";
    }

    if ($action == 'get_order_page') {
        send_data_to_GA($product_code);
    }

    switch ($product_code) {
		case "mag_storemanager_mm":
			 $urls = array(
				"get_support_center" => "https://www.mag-manager.com/support-center/",
                "product_page" => "https://www.mag-manager.com",
                "product_description" => "https://www.mag-manager.com/product-information/",
                "get_splash_url" => "https://www.mag-manager.com",
                //"video_tutorial"	  => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://www.mag-manager.com/magento-video-tutorials",
                "send_feature" => "https://support.emagicone.com/609746-Magento-Manager-Feature-Request",
                "send_problem" => "https://support.emagicone.com/submit_ticket",
                "leave_comment" => "https://support.emagicone.com/submit_ticket",
                "update" => "https://www.mag-manager.com/update-services/",
               # "get_help" => "https://store-manager-for-magento-documentation.emagicone.com", 14092018 
               # "get_help" => "https://store-manager-for-magento-doc.emagicone.com/for-mac-users",
				"get_help" => (!empty($params['tag']) ? "https://store-manager-for-magento-doc.emagicone.com/_/search?query=" . $params['tag'] : "https://store-manager-for-magento-doc.emagicone.com/for-mac-users"),
                "get_faq" => "https://www.mag-manager.com/store-manager-for-magento-faq",
                "get_forum" => "https://support.emagicone.com/220725-Magento-Users-Forum",
                "get_order_page" => "https://www.mag-manager.com/order/" /* . (($product_code == 'mag_storemanager_demo') ? "?utm_source=storemanagerapp_demo&utm_medium=storemanagerapp_demo_button&utm_campaign=storemanagerapp_demo" :  "?utm_source=storemanagerapp&utm_medium=storemanagerappbutton&utm_campaign=storemanagerapp" ) */,
                "get_blog" => "https://blog.mag-manager.com",
                "get_last_news" => "https://www.mag-manager.com/discover-the-latest-updates-of-store-manager-for-magento",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                // "get_help_tag"        => "https://store-manager-for-magento-documentation.emagicone.com" . (!empty($params['tag']) ? "/system/app/pages/search?q=" . $params['tag'] : ""),
                "get_bridgeconnector" => "https://emagicone.com/shared_files/magento/magento_2/MagentoStoreManagerConnector_magento_2.zip",
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/magento-banner-for-software.php",
                "get_contact_support" => "https://www.mag-manager.com/support-center/contact-us",
                "get_contact_us" => "https://www.mag-manager.com/support-center/contact-us",
                "get_addon_order" => "https://www.mag-manager.com/" . get_addon_order($product_code, $params['product_code']),
                "get_addon_page" => "https://www.mag-manager.com/" . get_addon_order($product_code, $params['product_code']),
                "order_report" => "https://www.mag-manager.com/magento-report-development",                
                "get_buy_service" => "https://www.mag-manager.com/update-services",
                "get_rss_subscribe" => "https://feeds.feedburner.com/MagentoStoreManager",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-magento-was-removed-from-your-computer",
                "get_help_tag" => get_help_tag($product_code, $params['product_code'], $params['tag']),
                'wrong_version' => 'https://support.emagicone.com/738629-Database-Mismatch---Wrong-Shopping-Cart-version-oldnew-or-Store-Manager-Version-old',
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html",
                'get_flash_palyer' => 'https://get.adobe.com/flashplayer/npapi/'
            );

		break;
		
        case "oscommerce_storemanager":
        case "oscommerce_demo":
            $urls = array(
                "product_page" => "https://emagicone.com/products/solutions-for-oscommerce/",
                "product_description" => "https://emagicone.com/products/solutions-for-oscommerce/",
                //"video_tutorial"	  => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://emagicone.com/products/solutions-for-oscommerce/",
                "update" => "https://emagicone.com/products/solutions-for-oscommerce/",
                "get_help" => "https://oscommerce-store-manager-doc.emagicone.com/",
                "get_faq" => "https://support.emagicone.com/328826-Knowledgebase",
                "get_blog" => "https://emagicone.com/products/solutions-for-oscommerce/",
                "get_last_news" => "https://emagicone.com/products/solutions-for-oscommerce/",
                //"get_forum"			  => "https://oscommerce-manager.betaeasy.com",
                "get_order_page" => "https://emagicone.com/products/solutions-for-oscommerce/",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                "get_help_tag" => "https://oscommerce-store-manager-doc.emagicone.com/" . (!empty($params['tag']) ? "/_/search?query=" . $params['tag'] : ""),
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/oscommerce-banner-for-software.php",
                "get_contact_support" => "https://emagicone.com/contact-us/",
                "get_contact_us" => "https://oscommerce-manager.com/#form",
                "get_addon_order" => get_addon_help($product_code, $params['product_code']),
                "get_addon_page" => get_addon_help($product_code, $params['product_code']),
                "get_buy_service" => "https://emagicone.com/products/solutions-for-oscommerce/",
                "get_rss_subscribe" => "https://emagicone.com/products/solutions-for-oscommerce/",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-oscommerce-was-removed-from-your-computer",
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html"
            );
            break;


        case "zencart_storemanager":
        case "zencart_demo":
            $urls = array(
				"get_emagic" => 'https://store.emagicone.com/',
                "product_page" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "product_description" => "https://emagicone.com/products/solutions-for-zen-cart/",
                //"video_tutorial"	  => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "send_feature" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "update" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "get_help" => "https://support.emagicone.com/",
                "get_faq" => "https://support.emagicone.com/",
                "get_blog" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "get_last_news" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "get_forum" => "https://support.emagicone.com/",
                "get_order_page" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                "get_help_tag" => "https://zen-cart-store-manager-doc.emagicone.com" . (!empty($params['tag']) ? "/_/search?query=" . $params['tag'] : ""),
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://openx.emagicone.com/www/delivery/afr.php?zoneid=94&amp;cb=",
                "get_contact_support" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "get_contact_us" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "get_addon_order" => get_addon_help($product_code, $params['product_code']),
                "get_addon_page" => "https://emagicone.com/products/solutions-for-zen-cart/" . get_addon_order($product_code, $params['product_code']),
                "get_buy_service" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "order_report" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "get_rss_subscribe" => "https://emagicone.com/products/solutions-for-zen-cart/",
                "get_sm_removed_page" => "https://support.emagicone.com/",
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html",
				"get_twitter" => "https://twitter.com/emagicone",
				"get_facebook" => "https://www.facebook.com/EMagicOne/",
				
            );
            break;


        case "prestashop_storemanager":
        case "prestashop_sm_standard":
        case "prestashop_demo":
			
            $urls = array(
                "product_page" => "https://www.prestashopmanager.com",
                "product_description" => "https://www.prestashopmanager.com/prestashop-product-information/",
                //"video_tutorial"	  => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://www.prestashopmanager.com/prestashop-video-tutorials",
                "send_feature" => "https://support.emagicone.com/823795-PrestaShop-Manager-Feature-Request",
                "send_problem" => "https://support.emagicone.com/submit_ticket",
                "leave_comment" => "https://support.emagicone.com/submit_ticket",
                "update" => "https://www.prestashopmanager.com/update-services/",
                "get_bridgeconnector" => "https://emagicone.com/shared_files/prestashop/bridgeconnector.zip",
				"get_bridge_connector_module" => 'https://emagicone.com/shared_files/prestashop/bridgeconnector.zip',
				//"get_bridge_connector_module" => 'https://prestashopmanager.com/files/bridge_connector_module.zip',
                "get_help" => "https://prestashop-store-manager-doc.emagicone.com" /* ."?utm_source=storemanagerapp_homepage&utm_medium=storemanagerapp_homepage&utm_campaign=storemanagerapp_homepage" */,
                "get_faq" => "https://www.prestashopmanager.com/faq",
                "get_how_to" => "https://www.prestashopmanager.com/useful-articles/how-to/" /* ."?utm_source=storemanagerapp_homepage&utm_medium=storemanagerapp_homepage&utm_campaign=storemanagerapp_homepage" */,
                "get_blog" => "https://blog.prestashopmanager.com",
                "get_last_news" => "https://www.prestashopmanager.com/discover-the-latest-updates-of-store-manager-for-prestashop/" /* . "?utm_source=storemanagerapp_homepage&utm_medium=storemanagerapp_homepage&utm_campaign=storemanagerapp_homepage" */,
                "get_forum" => "https://support.emagicone.com/145902-PrestaShop-Users-Forum",
                "get_order_page" => "https://www.prestashopmanager.com/order/",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                // "get_help_tag"        => "https://prestashop-store-manager-doc.emagicone.com" . (!empty($params['tag']) ? "/system/app/pages/search?q=" . $params['tag'] : ""),
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/prestashop-banner-for-software.php",
                "get_contact_support" => "https://www.prestashopmanager.com/support-center/contact-us/",
                "get_contact_us" => "https://www.prestashopmanager.com/support-center/contact-us/",
                "get_addon_order" => "https://www.prestashopmanager.com/" . get_addon_order($product_code, $params['product_code']),
                "get_addon_page" => get_addon_help($product_code, $params['product_code']),	
                "get_buy_service" => "https://www.prestashopmanager.com/update-services/",
                "order_report" => "https://www.prestashopmanager.com/prestashop-services-on-demand/advanced-report-development/",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-prestashop-was-removed-from-your-computer",
                "get_rss_subscribe" => "https://feeds.feedburner.com/PrestaShopStoreManager",
                // "get_help_tag"        => ($params['tag'] != 'fmEbayIntegrationWizard') ? ("https://prestashop-store-manager-doc.emagicone.com" . (!empty($params['tag']) ? "/system/app/pages/search?q=" . $params['tag'] : "")) : "https://ebay-integration-doc.emagicone.com/"
                "get_help_tag" => get_help_tag($product_code, $params['product_code'], $params['tag']),
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html",
				"get_help_direct" => "https://prestashop-store-manager-doc.emagicone.com/quick-start-guide/direct-connection-to-mysql-database",
				"get_help_bridge" => "https://prestashop-store-manager-doc.emagicone.com/quick-start-guide/php-mysql-bridge-connection",
				"get_help_connector" => "https://prestashop-store-manager-doc.emagicone.com/quick-start-guide/2-prestashop-cloud-connection-wizard"
            );

            break;


        case "mag_storemanager":
        case "mag_storemanager_demo":
        // $urls = array(
        // 	"get_order_page"	  => "https://mag-manager.com/order?utm_source=storemanagerapp_demo&utm_medium=storemanagerapp_demo_button&utm_campaign=storemanagerapp_demo"
        // 	);
        // break;
        case "mag_storemanager_basic":
        case "mag_enterprise":
        case "mag_demo":
            $urls = array(
                "product_page" => "https://www.mag-manager.com",
                "product_description" => "https://www.mag-manager.com/product-information/",
                "get_splash_url" => "https://www.mag-manager.com",
                //"video_tutorial"	  => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://www.mag-manager.com/magento-video-tutorials",
                "send_feature" => "https://support.emagicone.com/609746-Magento-Manager-Feature-Request",
                "send_problem" => "https://support.emagicone.com/submit_ticket",
                "leave_comment" => "https://support.emagicone.com/submit_ticket",
                "update" => "https://www.mag-manager.com/update-services/",
                #"get_help" => "https://store-manager-for-magento-documentation.emagicone.com", 14092018
                "get_help" => "https://store-manager-for-magento-doc.emagicone.com",
                "get_faq" => "https://www.mag-manager.com/store-manager-for-magento-faq",
                "get_forum" => "https://support.emagicone.com/220725-Magento-Users-Forum",
                "get_order_page" => "https://www.mag-manager.com/order/" /* . (($product_code == 'mag_storemanager_demo') ? "?utm_source=storemanagerapp_demo&utm_medium=storemanagerapp_demo_button&utm_campaign=storemanagerapp_demo" :  "?utm_source=storemanagerapp&utm_medium=storemanagerappbutton&utm_campaign=storemanagerapp" ) */,
                "get_blog" => "https://blog.mag-manager.com",
                "get_last_news" => "https://www.mag-manager.com/discover-the-latest-updates-of-store-manager-for-magento",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                // "get_help_tag"        => "https://store-manager-for-magento-documentation.emagicone.com" . (!empty($params['tag']) ? "/system/app/pages/search?q=" . $params['tag'] : ""),
                "get_bridgeconnector" => "https://emagicone.com/shared_files/magento/magento_2/MagentoStoreManagerConnector_magento_2.zip",
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/magento-banner-for-software.php",
                "get_contact_support" => "https://www.mag-manager.com/support-center/contact-us",
                "get_contact_us" => "https://www.mag-manager.com/support-center/contact-us",
                "get_addon_order" => "https://www.mag-manager.com/" . get_addon_order($product_code, $params['product_code']),
                "get_addon_page" => "https://www.mag-manager.com/" . get_addon_order($product_code, $params['product_code']),
                "order_report" => "https://www.mag-manager.com/magento-report-development/",
                "get_buy_service" => "https://www.mag-manager.com/update-services/",
                "get_rss_subscribe" => "https://feeds.feedburner.com/MagentoStoreManager",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-magento-was-removed-from-your-computer",
                "get_help_tag" => get_help_tag($product_code, $params['product_code'], $params['tag']),
                'wrong_version' => 'https://support.emagicone.com/738629-Database-Mismatch---Wrong-Shopping-Cart-version-oldnew-or-Store-Manager-Version-old',
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html",
                'get_flash_palyer' => 'https://get.adobe.com/flashplayer/npapi/',
				"get_help_connector" => "https://store-manager-for-magento-doc.emagicone.com/quick-start-guide/1-2-magento-store-manager-connector"
            );
            break;


        case "creloaded_storemanager":
        case "creloaded_storemanager_demo":
        case "creloaded_demo":
            $urls = array(
                "product_page" => "https://emagicone.com/products/solutions-for-cre-loaded/",
                "product_description" => "https://emagicone.com/products/solutions-for-cre-loaded/",
                //"video_tutorial"	  => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://emagicone.com/products/solutions-for-cre-loaded/",
                "update" => "https://emagicone.com/products/solutions-for-cre-loaded/",
                "get_help" => "https://cre-loaded-store-manager-doc.emagicone.com/",
                "get_faq" => "https://support.emagicone.com/328826-Knowledgebase",
                "get_blog" => "https://emagicone.com/products/solutions-for-cre-loaded/",
                "get_last_news" => "https://emagicone.com/products/solutions-for-cre-loaded/",
                //"get_forum"			  => "https://creloaded-manager.betaeasy.com",
                "get_order_page" => "https://emagicone.com/products/solutions-for-cre-loaded/",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                "get_help_tag" => "https://cre-loaded-store-manager-doc.emagicone.com/" . (!empty($params['tag']) ? "/_/search?query=" . $params['tag'] : ""),
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/creloaded-banner-for-software.php",
                "get_contact_support" => "https://emagicone.com/products/solutions-for-cre-loaded/",
                "get_contact_us" => "https://emagicone.com/contact-us/",
                "get_addon_order" => ($params['product_code'] !== 'adbanner_remove_cre') ? "https://cre-loaded-store-manager-doc.emagicone.com/" . get_addon_order($product_code, $params['product_code']) : "https://www.regnow.com/checkout/cart/add/40817-122",
                "get_addon_page" => "https://emagicone.com/products/solutions-for-cre-loaded/" . get_addon_order($product_code, $params['product_code']),
                "get_buy_service" => "https://emagicone.com/products/solutions-for-cre-loaded/",
                "get_rss_subscribe" => "https://emagicone.com/products/solutions-for-cre-loaded/",
                "send_feature" => "https://emagicone.com/products/solutions-for-cre-loaded/",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-cre-loaded-was-removed-from-your-computer",
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html"
            );
            break;


        case "xcart_storemanager":
        case "xcart_demo":
            $urls = array(
                "product_page" => "https://emagicone.com/products/solutions-for-x-cart/",
                "product_description" => "https://emagicone.com/products/solutions-for-x-cart/",
                //"video_tutorial"	  => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://emagicone.com/products/solutions-for-x-cart/",
                "update" => "https://emagicone.com/products/solutions-for-x-cart/",
                "get_help" => "https://x-cart-store-manager-doc.emagicone.com/",
                "get_faq" => "https://support.emagicone.com/328826-Knowledgebase",
                "get_blog" => "https://emagicone.com/products/solutions-for-x-cart/",
                "get_last_news" => "https://emagicone.com/products/solutions-for-x-cart/",
                //"get_forum"			  => "https://xcart-manager.betaeasy.com",
                "get_order_page" => "https://emagicone.com/products/solutions-for-x-cart/",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                "get_help_tag" => "https://x-cart-store-manager-doc.emagicone.com/" . (!empty($params['tag']) ? "/_/search?query=" . $params['tag'] : ""),
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/xcart-banner-for-software.php",
                "get_contact_support" => "https://emagicone.com/products/solutions-for-x-cart/",
                "get_contact_us" => "https://emagicone.com/contact-us/",
                "get_addon_order" => "https://emagicone.com/products/solutions-for-x-cart/" . get_addon_order($product_code, $params['product_code']),
                "get_addon_page" => "https://emagicone.com/products/solutions-for-x-cart/" . get_addon_order($product_code, $params['product_code']),
                "get_buy_service" => "https://emagicone.com/products/solutions-for-x-cart/",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-x-cart-was-removed-from-your-computer",
                "send_problem" => "https://support.emagicone.com/submit_ticket",
                "leave_comment" => "https://support.emagicone.com/submit_ticket",
                "get_rss_subscribe" => "https://feeds.feedburner.com/XCartStoreManager",
                "send_feature " => "https://emagicone.com/products/solutions-for-x-cart/",
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html"
            );
            break;

        case "mijoshop_storemanager":
        case "mijoshop_storemanager_additional":
            $urls = array(
                "product_page" => "https://emagicone.com/products/store-manager-for-mijoshop/",
                "product_description" => "https://emagicone.com/products/store-manager-for-mijoshop/",
                //"video_tutorial"	  => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://emagicone.com/products/store-manager-for-mijoshop/",
                "send_feature" => "https://support.emagicone.com/345194-MijoShop-Manager-Feature-Request",
                "send_problem" => "https://support.emagicone.com/submit_ticket",
                "leave_comment" => "https://support.emagicone.com/submit_ticket",
                "update" => "https://emagicone.com/products/store-manager-for-mijoshop/",
                "get_help" => "https://opencart-store-manager-doc.emagicone.com/",
                "get_faq" => "https://support.emagicone.com/328826-Knowledgebase",
                "get_blog" => "https://emagicone.com/products/store-manager-for-mijoshop/",
                //"get_last_news"		  => "https://opencartmanager.com/new-alpha-version-of-store-manager-for-opencart-is-available-2012",
                // "get_last_news"		  => "https://opencartmanager.com/new-beta-version-of-store-manager-for-opencart-is-available",
                "get_last_news" => "https://emagicone.com/products/store-manager-for-mijoshop/",
                //"get_forum"			  => "https://opencartmanager.betaeasy.com",
                "get_order_page" => "https://emagicone.com/products/store-manager-for-mijoshop/",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                "get_help_tag" => "https://opencart-store-manager-doc.emagicone.com" . (!empty($params['tag']) ? "/_/search?query=" . $params['tag'] : ""),
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/opencart-banner-for-software.php",
                "get_contact_support" => "https://emagicone.com/contact-us/",
                "get_contact_us" => "https://emagicone.com/contact-us/",
                "get_addon_order" => "https://mijoshop-manager.com/" . get_addon_order($product_code, $params['product_code']),
                "get_addon_page" => get_addon_help($product_code, $params['product_code']),
                // "get_addon_page"      => "https://mijoshop-manager.com/" . get_addon_order($product_code, $params['product_code']),
                "get_buy_service" => "https://emagicone.com/products/store-manager-for-mijoshop/",
                "get_rss_subscribe" => "https://emagicone.com/products/store-manager-for-mijoshop/",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-mijoshop-was-removed-from-your-computer",
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html"
            );
            break;
        case "opencart_storemanager":
        case "opencart_demo":
            $urls = array(
                "product_page" => "https://www.opencartmanager.com",
                "product_description" => "https://www.opencartmanager.com/product-information",
                //"video_tutorial"	  => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://www.opencartmanager.com",
                "send_feature" => "https://support.emagicone.com/360753-OpenCart-Manager-Feature-Request",
                "send_problem" => "https://support.emagicone.com/submit_ticket",
                "leave_comment" => "https://support.emagicone.com/submit_ticket",
                "update" => "https://www.opencartmanager.com/update-services",
                "get_help" => "https://opencart-store-manager-doc.emagicone.com",
                "get_faq" => "https://support.emagicone.com/328826-Knowledgebase",
                "get_blog" => "https://blog.opencartmanager.com",
                //"get_last_news"		  => "https://opencartmanager.com/new-alpha-version-of-store-manager-for-opencart-is-available-2012",
                // "get_last_news"		  => "https://opencartmanager.com/new-beta-version-of-store-manager-for-opencart-is-available",
                "get_last_news" => "https://www.opencartmanager.com/discover-the-latest-updates-of-store-manager-for-opencart/",
                "get_forum" => "https://support.emagicone.com/404762-OpenCart-Users-Forum",
                "get_order_page" => "https://www.opencartmanager.com/order/",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                "get_help_tag" => "https://opencart-store-manager-doc.emagicone.com" . (!empty($params['tag']) ? "/_/search?query=" . $params['tag'] : ""),
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/opencart-banner-for-software.php",
                "get_contact_support" => "https://www.opencartmanager.com/contact",
                "get_contact_us" => "https://www.opencartmanager.com/contact",
                "get_addon_order" => "https://www.opencartmanager.com/" . get_addon_order($product_code, $params['product_code']),
                "get_addon_page" => "https://www.opencartmanager.com/" . get_addon_order($product_code, $params['product_code']),
                "order_report" => "https://www.opencartmanager.com/simple-report-development",
                "get_buy_service" => "https://www.opencartmanager.com/update-services",
                "get_rss_subscribe" => "https://feeds.feedburner.com/OpenCartStoreManager",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-opencart-was-removed-from-your-computer",
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html"
            );
            break;


        case "cubecart_storemanager":
        case "cubecart_demo":
            $urls = array(
                "product_page" => "https://emagicone.com/products/solutions-for-cubecart/",
                "product_description" => "https://emagicone.com/products/solutions-for-cubecart/",
                //"video_tutorial"	  => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://emagicone.com/products/solutions-for-cubecart/",
                "update" => "https://emagicone.com/products/solutions-for-cubecart/",
                "get_help" => "https://cubecart-store-manager-doc.emagicone.com/",
                "get_faq" => "https://support.emagicone.com/328826-Knowledgebase",
                "get_blog" => "https://emagicone.com/products/solutions-for-cubecart/",
                "get_last_news" => "https://emagicone.com/products/solutions-for-cubecart/",
                //"get_forum"			  => "https://cubecartmanager.betaeasy.com",
                "get_order_page" => "https://emagicone.com/products/solutions-for-cubecart/",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                "get_help_tag" => "https://cubecart-store-manager-doc.emagicone.com/" . (!empty($params['tag']) ? "/_/search?query=" . $params['tag'] : ""),
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/cubecart-banner-for-software.php",
                "get_contact_support" => "https://emagicone.com/products/solutions-for-cubecart/",
                "get_contact_us" => "https://emagicone.com/products/solutions-for-cubecart/",
                "get_addon_order" => "https://emagicone.com/products/solutions-for-cubecart/" . get_addon_order($product_code, $params['product_code']),
                "get_addon_page" => "https://emagicone.com/products/solutions-for-cubecart/" . get_addon_order($product_code, $params['product_code']),
                "get_buy_service" => "https://emagicone.com/products/solutions-for-cubecart/",
                "get_rss_subscribe" => "https://emagicone.com/products/solutions-for-cubecart/",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-cubecart-was-removed-from-your-computer",
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html"
            );
            break;


        case "pinnaclecart_storemanager":
        case "pinnaclecart_demo":
            $urls = array(
                "product_page" => "https://emagicone.com/products/pinnaclecart/",
                "product_description" => "https://emagicone.com/products/pinnaclecart/",
                //"video_tutorial"	  => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://emagicone.com/products/pinnaclecart/",
                "update" => "https://emagicone.com/products/pinnaclecart/",
                "get_help" => "https://pinnacle-cart-store-manager.emagicone.com/",
                "get_faq" => "https://support.emagicone.com/328826-Knowledgebase",
                "get_blog" => "https://emagicone.com/products/pinnaclecart/",
                "get_last_news" => "https://emagicone.com/products/pinnaclecart/",
                //"get_forum"			  => "https://cubecartmanager.betaeasy.com",
                "get_order_page" => "https://emagicone.com/products/pinnaclecart/",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                "get_help_tag" => "https://pinnacle-cart-store-manager.emagicone.com/" . (!empty($params['tag']) ? "/_/search?query=" . $params['tag'] : ""),
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/pinnaclecart-banner-for-software.php",
                "get_contact_support" => "https://emagicone.com/products/pinnaclecart/",
                "get_contact_us" => "https://emagicone.com/products/pinnaclecart/",
                "get_addon_order" => "https://emagicone.com/products/pinnaclecart/" . get_addon_order($product_code, $params['product_code']),
                "get_addon_page" => "https://emagicone.com/products/pinnaclecart/" . get_addon_order($product_code, $params['product_code']),
                "get_buy_service" => "https://emagicone.com/products/pinnaclecart/",
                "get_rss_subscribe" => "https://emagicone.com/products/pinnaclecart/",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-pinnaclecart-was-removed-from-your-computer",
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html"
            );
            break;


        case "virtuemart_storemanager":
        case "virtuemart_demo":
        case "virtuemart_storemanager_demo":
            $urls = array(
				"get_bridge" => "https://emagicone.com/shared_files/bridge_7.70.zip",
                "product_page" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "product_description" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "video_tutorial" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "update" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "get_help" => "https://virtuemart-store-manager-doc.emagicone.com/",
                "get_faq" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "get_blog" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "get_last_news" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "get_forum" => "https://support.emagicone.com/",
                "get_order_page" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                "get_help_tag" => "https://virtuemart-store-manager-doc.emagicone.com" . (!empty($params['tag']) ? "/_/search?query=" . $params['tag'] : ""),
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/virtuemart-banner-for-software.php",
                "get_contact_support" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "get_contact_us" => "https://emagicone.com/products/solutions-for-virtuemart/",
                // sunset 201905-14 ---> main page "get_addon_order" => "https://emagicone.com/products/solutions-for-virtuemart/" . get_addon_order($product_code, $params['product_code']),
				"get_addon_order" => "https://emagicone.com/products/solutions-for-virtuemart/",
                // sunset 2019-05-14 ---> main page "get_addon_page" => "https://emagicone.com/products/solutions-for-virtuemart/" . get_addon_order($product_code, $params['product_code']),
				"get_addon_page" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "order_report" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "get_buy_service" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "get_rss_subscribe" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-virtuemart-was-removed-from-your-computer",
                "send_feature" => "https://emagicone.com/products/solutions-for-virtuemart/",
                "get_alpha" => "https://license.emagicone.com/storemanager/download.php?specified_ver=600",
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html",
				"get_emagic" => "https://store.emagicone.com/",
				"get_twitter" => "https://twitter.com/virtuemartsm",
				"get_facebook" => "https://www.facebook.com/VirtueMartStoreManager/",
				
			);
            break;

        case "sm_woocm_primary":
        case "sm_woocm_additional":
            $urls = array(
				"get_bridge" => "https://emagicone.com/shared_files/bridge_7.70.zip",
                "product_page" => "https://woocommerce-manager.com/",
                "product_description" => "https://woocommerce-manager.com/#about",
                "send_feature" => "https://support.emagicone.com/690396-WooCommerce-Manager-Feature-Request",
                //"video_tutorial"    => "https://emagicone.com/store/video/xcart/Store_Manager_for_X-Cart_Overview/Store_Manager_for_X-Cart_Overview.html",
                "video_tutorial" => "https://woocommerce-manager.com/",
                "update" => "https://woocommerce-manager.com/update-services",
                "get_help" => "https://woocommerce-store-manager-doc.emagicone.com",
                "get_faq" => "https://woocommerce-store-manager-doc.emagicone.com",
                "get_blog" => "https://woocommerce-manager.com/",
                "get_last_news" => "https://woocommerce-manager.com/latest-updates-of-store-manager-for-woocommerce/",
                "get_forum" => "https://support.emagicone.com/192473-WooCommerce-Users-Forum",
                "get_order_page" => "https://woocommerce-manager.com/order/",
                "check_new_version" => "https://license.emagicone.com/storemanager/checkversion.php?ver=" . $params['version'] . "&verext=" . $product_code . "&licensekey=" . $params['key'],
                "get_help_tag" => "https://woocommerce-store-manager-doc.emagicone.com" . (!empty($params['tag']) ? "/_/search?query=" . $params['tag'] : ""),
                "get_update" => "https://license.emagicone.com/storemanager/check_download.php?version=" . $params['version'] . "&licensekey=" . $params['key'] . "&verext=" . $product_code,
                "get_addons_info" => "https://license.emagicone.com/storemanager/addons.php?parent_product_code=" . $product_code,
                "get_ads" => "https://emagicone.com/sm_banners/woocommerce-banner-for-software.php",
                "get_contact_support" => "https://woocommerce-manager.com/",
                "get_contact_us" => "https://woocommerce-manager.com/",
                "get_addon_order" => "https://woocommerce-manager.com/" . get_addon_order($product_code, $params['product_code']),
                "get_addon_page" => "https://woocommerce-manager.com/" . get_addon_order($product_code, $params['product_code']),
                "order_report" => "https://woocommerce-manager.com/woocommerce-report-development",
                "get_buy_service" => "https://woocommerce-manager.com/update-services",
                "get_rss_subscribe" => "https://woocommerce-manager.com/",
                "get_sm_removed_page" => "https://emagicone.com/store-manager-for-woocommerce-was-removed-from-your-computer",
                "get_help_fastreport" => "https://www.fast-report.com/public_download/html/FR5UserManual-HTML-en/index.html"
            );
            break;

        // Common addons
        case "plugin_doba":
            $urls = array(
                "get_help_tag" => "https://emagicone.com"
            );
            break;
        case "plugin_ebay":
            $urls = array(
                "get_help_tag" => "https://ebay-integration-doc.emagicone.com/"
            );
            break;
        case "plugin_amazon":
            $urls = array(
                "get_amazon_pro" => "https://www.amazon.com/gp/help/customer/display.html?nodeId=1161306",
                'get_amazon_guide' => 'https://amazon-integration-doc.emagicone.com/2-quick-start-guide-2-1-registration/2-0-login-to-amazon-seller-account'
            );
            break;
        case "common_addon":
            $urls = array(
                "get_amazon_pro" => "https://store.emagicone.com/catalogsearch/result/?q=Amazon"
            );



            break;
    }

    $urls = array_merge($main_urls, $urls);
    if (isset($urls[$action])) {
        return $urls[$action];
    }

    return false;
}

function send_data_to_GA($product_code) {
    require_once(dirname(__FILE__) . "/ga_api.php");
    require_once(dirname(__FILE__) . "/GA_codes.php");

    $ga_code = getGAcode_product_code($product_code);

    if ($ga_code) {
        $ga_api = new Analytics($ga_code);
        $ga_api->send_gaPage('/buy_now_link.htm');
    }
}

function get_addon_help($product_code, $addon_code) {

    switch ($product_code) {
        case "prestashop_storemanager":
        case "prestashop_sm_standard":
        case "prestashop_demo":
            switch ($addon_code) {
                case "plugin_doba":
                    return 'https://emagicone.com/';
                    break;
                case "plugin_autoimport":
                case "plugin_autoimport_prestashop":
                    return 'https://product-auto-import-export-doc.emagicone.com/';
                    break;
                case "plugin_ebay":
                    return 'https://ebay-integration-doc.emagicone.com/';
                    break;
                case "plugin_quickbooks":
                    return 'https://quickbooks-integration-doc.emagicone.com/';
                    break;
					
                case "plugin_quickbooks_pos":
                    return 'https://prestashop-store-manager-doc.emagicone.com/prestashop-customers-and-orders-management/8-point-of-sale-section';
                    break;
                case "plugin_icecat":
                    return 'https://icecat-integration-doc.emagicone.com/';
                    break;
                case "plugin_shipping_stamps":
                    return 'https://shipping-integration-doc.emagicone.com/';
                    break;
                case "plugin_peachtree":
                    return 'https://peachtree-integration-doc.emagicone.com/';
                    break;
                case "plugin_pdf_catalog_creator":
                    return 'https://print-lookbook-pdf-catalog-doc.emagicone.com/';
                    break;
                case "plugin_amazon":
                    return 'https://amazon-integration-doc.emagicone.com/';
                    break;
				case "plugin_quickbooks_online":
					
                    return 'https://quickbooks-online-doc.emagicone.com/';
                    break;
					
                default:
                    return 'product-information/store-manager-prestashop-addon';
                    break;
            }
            break;
        case "zencart_storemanager":
        case "zencart_demo":
            switch ($addon_code) {
                
            }
            break;
        case "mijoshop_storemanager":
        case "mijoshop_storemanager_additional":
            switch ($addon_code) {
                case "plugin_doba":
                    return 'https://emagicone.com/';
                    break;
                case "plugin_autoimport":
                case "plugin_autoimport_mijoshop":
                    return 'https://product-auto-import-export-doc.emagicone.com/';
                    break;
                case "plugin_ebay":
                    return 'https://ebay-integration-doc.emagicone.com/';
                    break;
                case "plugin_quickbooks":
                    return 'https://quickbooks-integration-doc.emagicone.com/';
                    break;
                case "plugin_quickbooks_pos":
                    return 'https://prestashop-store-manager-doc.emagicone.com/prestashop-customers-and-orders-management/8-point-of-sale-section';
                    break;
                case "plugin_icecat":
                    return 'https://icecat-integration-doc.emagicone.com/';
                    break;
                case "plugin_shipping_stamps":
                    return 'https://shipping-integration-doc.emagicone.com/';
                    break;
                case "plugin_peachtree":
                    return 'https://emagicone.com';
                    break;
                case "plugin_pdf_catalog_creator":
                    return 'https://print-lookbook-pdf-catalog-doc.emagicone.com/';
                    break;
                case "plugin_amazon":
                    return 'https://amazon-integration-doc.emagicone.com/';
                    break;
                default:
                    return 'product-information/store-manager-prestashop-addon';
                    break;
            }
        default:
            switch ($addon_code) {
                case "plugin_doba":
                    return 'https://emagicone.com';
                    break;
                case "plugin_autoimport":
                case "plugin_autoimport_mijoshop":
                case "plugin_autoimport_osc":
                    return 'https://product-auto-import-export-doc.emagicone.com/';
                    break;
                case "plugin_ebay":
                    return 'https://ebay-integration-doc.emagicone.com/';
                    break;
                case "plugin_quickbooks":
                    return 'https://quickbooks-integration-doc.emagicone.com/';
                    break;
				case "plugin_quickbooks_online":
                    return 'https://quickbooks-online-doc.emagicone.com/';
                    break;
                case "plugin_quickbooks_pos":
                    return 'https://prestashop-store-manager-doc.emagicone.com/prestashop-customers-and-orders-management/8-point-of-sale-section';
                    break;
                case "plugin_icecat":
                    return 'https://icecat-integration-doc.emagicone.com/';
                    break;
                case "plugin_shipping_stamps":
                    return 'https://shipping-integration-doc.emagicone.com/';
                    break;
                case "plugin_peachtree":
                    return 'https://peachtree-integration-doc.emagicone.com/';
                    break;
                case "plugin_pdf_catalog_creator":
                    return 'https://print-lookbook-pdf-catalog-doc.emagicone.com/';
                    break;
                case "plugin_amazon":
                    return 'https://amazon-integration-doc.emagicone.com/';
                    break;
            }
            break;
    }
}

function get_help_tag($product_code, $addon_code, $tag) {
    // emagicone.com/redirect/main_redirector_utm.php?product_code=plugin_amazon&action=get_help_tag&parent_product_code=common_addon&tag=AmazonPro
    switch ($tag) {
        case "fmEbayIntegrationWizard":
            return "https://ebay-integration-doc.emagicone.com/";
            break;
        case "QBCustomersImportExport":
            return "https://quickbooks-integration-doc.emagicone.com/";
            break;
        case "MainAutoImportForm":
            return "https://product-auto-import-export-doc.emagicone.com/";
            break;
        case "fmICEcatDialog":
            return "https://icecat-integration-doc.emagicone.com/";
            break;
        case "fmPTIntegrationWizard":
            return "https://peachtree-integration-doc.emagicone.com/";
            break;
        case "fmUSPSPage":
            return "https://www.stamps.com/partner/emagicone/?source=si10611822/";
            break;
			
        default:
            switch ($product_code) {
                case "prestashop_storemanager":
                case "prestashop_sm_standard":
                case "prestashop_demo":
                    return "https://prestashop-store-manager-doc.emagicone.com" . (!empty($tag) ? "/_/search?query=" . $tag : "");
                    break;
                case "mag_storemanager":
                case "mag_storemanager_demo":
                case "mag_storemanager_basic":
                case "mag_enterprise":				
					 if (!empty($tag) && $tag == 'Cloud') {
							$tag = 'fmConnectionWizard';
						}
					   # return "https://store-manager-for-magento-doc.emagicone.com/" . (!empty($tag) ? "/system/app/pages/search?q=" . $tag : ""); 14092018
						return "https://store-manager-for-magento-doc.emagicone.com" . (!empty($tag) ? "/_/search?query=" . $tag : "/");
						break;					
                case "mag_demo":
                    if (!empty($tag) && $tag == 'Cloud') {
                        $tag = 'fmConnectionWizard';
                    }
                   # return "https://store-manager-for-magento-doc.emagicone.com/" . (!empty($tag) ? "/system/app/pages/search?q=" . $tag : ""); 14092018
                    return "https://store-manager-for-magento-doc.emagicone.com/" . (!empty($tag) ? "/_/search?query=" . $tag . '&scope=site' : "");
                    break;
            }
            break;
    }
}

function get_addon_blog_page($product_code, $addon_code) {
    switch ($addon_code) {
        case "plugin_shipping_stamps":
            return 'https://blog.emagicone.com/2014/07/cut-down-expenses-on-usps-shipping-with.html ';
            break;
        default:
            return 'https://blog.emagicone.com';
            break;
    }
}

function get_addon_affiliate_page($product_code = null, $addon_code) {
    switch ($addon_code) {
        case "plugin_shipping_stamps":
            return 'https://www.stamps.com/shipping/integrations/emagicone';
            break;
        default:
            return 'https://blog.emagicone.com';
            break;
    }
}

function get_addon_documentation_page($product_code = null, $addon_code) {
    switch ($addon_code) {
        case "plugin_ebay":
            return 'https://ebay-integration-doc.emagicone.com/';
            break;
        default:
            return 'https://emagicone.com';
            break;
    }
}

function get_addon_order($product_code, $addon_code) {
    switch ($product_code) {
        case "oscommerce_storemanager":
        case "oscommerce_demo":
            switch ($addon_code) {
                case "plugin_price_changer_osc":
                    return 'mass-product-changer';
                    break;
                case "plugin_autoimport":
                case "plugin_autoimport_osc":
                    return 'automated-product-import';
                    break;
                case "plugin_ebay":
                    return 'ebay-export';
                    break;
                case "plugin_quickbooks":
                case "plugin_quickbooks_osc":
                    return 'quickbooks-integration';
                    break;
                case "plugin_icecat":
                    return 'icecat-product-catalogue-integration';
                    break;		
                case "addon_mom_import_export_osc":
                    return 'mail-order-manager-importexport';
                    break;
                case "plugin_doba":
                    return 'doba-dropshipper-integration';
                    break;
                case "plugin_shipping_stamps":
                    return 'shipping-integration';
                    break;
                case "plugin_adwords_creator_osc":
                    return 'mass-adwords-product-ads';
                    break;
                case "plugin_peachtree":
                    return 'peachtree-integration';
                    break;
                default:
                    return 'addons';
                    break;
            }
            break;


        case "zencart_storemanager":
        case "zencart_demo":
            switch ($addon_code) {
                case "plugin_price_changer_zen":
                    return 'mass-product-changer';
                    break;
                case "plugin_autoimport":
                case "plugin_autoimport_zen":
                    return 'automated-product-import';
                    break;
                case "plugin_ebay":
                    return 'zen-cart-ebay-integration';
                    break;
                case "plugin_quickbooks":
                case "plugin_quickbooks_zencart":
                    return 'quickbooks-integration';
                    break;
                case "plugin_icecat":
                    return 'icecat-product-catalogue-integration';
                    break;
                case "addon_mom_import_export_zen":
                    return 'mail-order-manager-importexport';
                    break;
                case "plugin_doba":
                    return 'zencart-doba-integration';
                    break;
                case "plugin_shipping_stamps":
                    return 'shipping-integration';
                    break;
                case "plugin_adwords_creator_zen":
                    return 'mass-adwords-product-ads';
                    break;
                case "plugin_peachtree":
                    return 'peachtree-integration';
                    break;
                default:
                    return 'buy-now-store-manager/addons';
                    break;
            }
            break;


        case "prestashop_storemanager":
        case "prestashop_sm_standard":
        case "prestashop_demo":
            switch ($addon_code) {
                case "plugin_doba":
                    return 'prestashop-doba-integration';
                    break;
                case "plugin_autoimport":
                case "plugin_autoimport_prestashop":
                    return 'prestashop-automated-product-import';
                    break;
                case "plugin_ebay":
                    return 'prestashop-ebay-integration';
                    break;
                case "plugin_quickbooks":
                    return 'prestaShop-quickbooks-integration';
                    break;
				case "plugin_quickbooks_online":					
                    return 'product/quickbooks-online-integration-for-prestashop-store-manager-addon/';
                    break;	
                case "plugin_icecat":
                    return 'prestashop-icecat-integration';
                    break;
                case "plugin_shipping_stamps":
                    return 'prestashop-shipping-integration';
                    break;
                case "plugin_peachtree":
                    return 'prestashop-peachtree-integration-addon';
                    break;
                case "plugin_pdf_catalog_creator":
                    return 'prestashop-pdf-catalog-creator';
                    break;
                case "plugin_amazon":
                    return 'integrations/prestashop-amazon-integration';
                    break;
                default:
                    return 'product-information/store-manager-prestashop-addon';
                    break;
            }
            break;


        case "mag_storemanager":
        case "mag_storemanager_demo":
        case "mag_storemanager_basic":
        case "mag_enterprise":
        case "mag_demo":
            switch ($addon_code) {
                case "plugin_doba":
                    return 'magento-doba-integration';
                    break;
                case "plugin_autoimport":
                case "plugin_autoimport_magento":
                    return 'magento-automated-product-import';
                    break;
                case "plugin_ebay":
                    return 'magento-ebay-integration';
                    break;
                case "plugin_amazon":
                    return 'magento-amazon-integration';
                    break;
                case "plugin_pdf_catalog_creator":
                    return 'magento-pdf-catalog-creator';
                    break;
                case "plugin_quickbooks":
                    return 'magento-quickbooks-integration';
                    break;
                case "plugin_icecat":
                    return 'magento-icecat-integration';
                    break;
                case "plugin_shipping_stamps":
                    return 'magento-shipping-integration';
                    break;
                case "plugin_peachtree":
                    return 'magento-peachtree-integration';
                    break;
                default:
                    return 'product-information/store-manager-magento-addon';
                    break;
            }
            break;


        case "creloaded_storemanager":
        case "creloaded_demo":
            switch ($addon_code) {
                case "plugin_price_changer_cre":
                    return 'mass-product-changer';
                    break;
                case "plugin_autoimport":
                case "plugin_autoimport_cre":
                    return 'automated-product-import';
                    break;
                case "adbanner_remove_cre":
                    return 'automated-product-import';
                    break;
                case "plugin_ebay":
                    return 'ebay-export';
                    break;
                case "plugin_quickbooks":
                    return 'quickbooks-integration';
                    break;
                case "plugin_icecat":
                    return 'icecat-product-catalogue-integration';
                    break;
                case "addon_mom_import_export_cre":
                    return 'mail-order-manager-importexport';
                    break;
                case "plugin_doba":
                    return 'doba-dropshipper-integration';
                    break;
                case "plugin_shipping_stamps":
                    return 'shipping-integration';
                    break;
                case "plugin_adwords_creator_cre":
                    return 'mass-adwords-product-ads';
                    break;
                case "plugin_peachtree":
                    return 'peachtree-integration';
                    break;
                default:
                    return 'addons';
                    break;
            }
            break;

        case "xcart_storemanager":
        case "xcart_demo":
            switch ($addon_code) {
                case "plugin_price_changer_xcart":
                    return 'mass-product-changer';
                    break;
                case "plugin_autoimport":
                case "plugin_autoimport_xcart":
                    return 'automated-product-import';
                    break;
                case "plugin_ebay":
                    return 'node/1678';
                    break;
                case "plugin_quickbooks":
                case "plugin_quickbooks_xcart":
                    return 'quickbooks-integration';
                    break;
                case "plugin_icecat":
                    return 'icecat-product-catalogue-integration';
                    break;
                case "addon_mom_import_export_xcart":
                    return 'mail-order-manager-importexport';
                    break;
                case "plugin_doba":
                    return 'doba-dropshipper-integration';
                    break;
                case "plugin_shipping_stamps":
                    return 'shipping-integration';
                    break;
                case "plugin_adwords_creator_xcart":
                    return 'mass-adwords-product-ads';
                    break;
                case "plugin_peachtree":
                    return 'peachtree-integration';
                    break;
                default:
                    return 'addons';
                    break;
            }
        case "mijoshop_storemanager":
        case "mijoshop_storemanager_additional":
            switch ($addon_code) {
                case "plugin_autoimport":
                case "plugin_autoimport_mijoshop":
                    return 'mjoshop-automated-product-import';
                    break;
                case "plugin_ebay":
                    return 'mijoshop-ebay-integration';
                    break;
                case "plugin_quickbooks":
                case "plugin_quickbooks_mijoshop":
                    return 'mjoshop-quickbooks-import-export';
                    break;
                case "plugin_icecat":
                    return 'mjoshop-icecat-integration';
                    break;
                default:
                    return 'addons';
                    break;
            }
        case "opencart_storemanager":
        case "opencart_demo":
            switch ($addon_code) {
                case "plugin_autoimport":
                case "plugin_autoimport_opencart":
                    return 'opencart-automated-product-import';
                    break;
                case "plugin_ebay":
                    return 'ebay-integration-for-opencart';
                    break;
                case "plugin_quickbooks":
                case "plugin_quickbooks_opencart":
                    return 'opencart-quickbooks-import-export';
                    break;
                case "plugin_icecat":
                    return 'opencart-icecat-integration';
                    break;
                default:
                    return 'addons';
                    break;
            }

        case "virtuemart_storemanager_2":
        case "virtuemart_demo_2":
            switch ($addon_code) {
                default:
                    //return  'addons';
                    return 'product-information/store-manager-virtuemart-addons';
                    break;
            }
            break;

        case "virtuemart_storemanager":
            switch ($addon_code) {
                case "plugin_doba":
                    return 'virtuemart-doba-integration';
                    break;
                case "plugin_ebay":
                    return 'virtuemart-ebay-integration ';
                    break;
                case "plugin_icecat":
                    return 'virtuemart-icecat-integration ';
                    break;
                case "plugin_peachtree":
                    return 'virtuemart-peachtree-integration';
                    break;
                case "plugin_quickbooks":
                    return 'virtuemart-quickbooks-integration';
                    break;
                case "plugin_autoimport_virtue":
                    return 'virtuemart-automated-product-import';
                    break;
                case "plugin_shipping_stamps":
                    return 'virtuemart-shipping-integration';
                    break;
                default:
                    //return  'addons';
                    return 'product-information/store-manager-virtuemart-addons';
                    break;
            }
            break;

        case "cubecart_storemanager":
        case "cubecart_demo":
            switch ($addon_code) {
                case "plugin_price_changer":
                    return 'mass-product-changer';
                    break;
                case "plugin_autoimport":
                case "plugin_autoimport_cubecart":
                    return 'automated-product-import';
                    break;
                case "plugin_ebay":
                    return 'node/1678';
                    break;
                case "plugin_quickbooks":
                    return 'quickbooks-integration';
                    break;
                case "plugin_icecat":
                    return 'icecat-product-catalogue-integration';
                    break;
                case "addon_mom_import_export_xcart":
                    return 'mail-order-manager-importexport';
                    break;
                case "plugin_shipping_stamps":
                    return 'shipping-integration';
                    break;
                case "plugin_adwords_creator_cubecart":
                    return 'mass-adwords-product-ads';
                    break;
                case "plugin_peachtree":
                    return 'peachtree-integration';
                    break;
                default:
                    return 'addons';
                    break;
            }
            break;



        case "pinnaclecart_storemanager":
        case "pinnaclecart_demo":
            switch ($addon_code) {
                case "plugin_price_changer":
                case "addon_masschanger_pinnaclecart":
                    return 'mass-product-changer';
                    break;
                case "plugin_autoimport":
                    return 'automated-product-import';
                    break;
                case "plugin_ebay":
                    return 'node/1678';
                    break;
                case "plugin_quickbooks":
                case "plugin_quickbooks_pinnacle":
                    return 'quickbooks-integration';
                    break;
                case "plugin_icecat":
                    return 'icecat-product-catalogue-integration';
                    break;
                case "addon_mom_import_export_pinnacle":
                    return 'mail-order-manager-importexport';
                    break;
                case "plugin_shipping_stamps":
                    return 'shipping-integration';
                    break;
                case "plugin_adwords_creator_pinnacle":
                    return 'mass-adwords-product-ads';
                    break;
                case "plugin_peachtree":
                    return 'peachtree-integration';
                    break;
                default:
                    return 'addons';
                    break;
            }
            break;
    }
}

function decrypt_request($data, $key = 'wm_Paint', $iv = 'CONTEXT_CONTROL1') {
    return mcrypt_cbc(MCRYPT_RIJNDAEL_128, $key, base64_decode($data), MCRYPT_DECRYPT, $iv);
}

?>
