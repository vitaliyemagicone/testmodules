<?php


namespace Emagicone\TestMagentoResponse\Model;

class GetOrdersManagement implements \Emagicone\TestMagentoResponse\Api\GetOrdersManagementInterface
{

    /**
     * {@inheritdoc}
     */
    public function postGetOrders($limit)
    {
        return 'hello api POST return the $limit ' . $limit;
    }

    /**
     * {@inheritdoc}
     */
    public function getGetOrders($limit)
    {
        return 'hello api GET return the $limit ' . $limit;
    }
}
