<?php


namespace Emagicone\TestMagentoResponse\Model;

class GetCustomersManagement implements \Emagicone\TestMagentoResponse\Api\GetCustomersManagementInterface
{

    /**
     * {@inheritdoc}
     */
    public function postGetCustomers($limit)
    {
        return 'hello api POST return the $limit ' . $limit;
    }

    /**
     * {@inheritdoc}
     */
    public function getGetCustomers($limit)
    {
        return 'hello api GET return the $limit ' . $limit;
    }
}
