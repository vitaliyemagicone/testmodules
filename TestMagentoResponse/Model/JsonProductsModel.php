<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Emagicone\TestMagentoResponse\Model;

use Magento\Framework\Model\AbstractModel;

class JsonProductsModel extends AbstractModel implements \Emagicone\TestMagentoResponse\Api\Data\GetProductsInterface
{
    const KEY_PRODUCT = 'product';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function getProducts()
    {
        return $this->_getData(self::KEY_PRODUCT);
    }

    /**
     * Set name
     *
     * @param array $products
     * @return array
     */
    public function setProducts($products)
    {
        $data = [];
        foreach ($products as $product) {
            $data[] = $this->setData(self::KEY_PRODUCT, $product);
        }

        return $data;
    }
}
