<?php


namespace Emagicone\TestMagentoResponse\Model;

class GetCategoriesManagement implements \Emagicone\TestMagentoResponse\Api\GetCategoriesManagementInterface
{

    /**
     * {@inheritdoc}
     */
    public function postGetCategories($limit)
    {
        return 'hello api POST return the $limit ' . $limit;
    }

    /**
     * {@inheritdoc}
     */
    public function getGetCategories($limit)
    {
        return 'hello api GET return the $limit ' . $limit;
    }
}
