<?php

namespace Emagicone\TestMagentoResponse\Model;

use Emagicone\TestMagentoResponse\Api\Data\GetProductsInterfaceFactory   as DataFactory;
use Magento\Catalog\Api\Data\ProductInterfaceFactory                     as ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface                       as ProductRepositoryInterface;
use Magento\Catalog\Model\Product                                        as ProductModel;
use Magento\Catalog\Model\Product\Visibility                             as ProductVisibility;
use Magento\Catalog\Model\ProductRepository                              as ProductRepository;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory        as ProductCollection;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem                                         as FileSystem;
use Magento\Framework\Image\AdapterFactory;
use Magento\Catalog\Helper\Image;

class GetProductsManagement implements \Emagicone\TestMagentoResponse\Api\GetProductsManagementInterface
{
    protected $productCollection;
    protected $productInterface;
    protected $productRepositoryInterface;
    protected $productRepository;
    protected $productModel;
    protected $productVisibility;

    protected $dataFactory;
    protected $fileSystem;
    protected $adapterFactory;

    /**
     * GetProductsManagement constructor.
     *
     * @param ProductCollection          $productCollection
     * @param ProductInterface           $productInterface
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param ProductRepository          $productRepository
     * @param ProductModel               $productModel
     * @param ProductVisibility          $productVisibility
     * @param DataFactory                $dataFactory
     * @param FileSystem                 $fileSystem
     */
    public function __construct(
        ProductCollection $productCollection,
        ProductInterface $productInterface,
        ProductRepositoryInterface $productRepositoryInterface,
        ProductRepository $productRepository,
        ProductModel $productModel,
        ProductVisibility $productVisibility,
        DataFactory $dataFactory,
        FileSystem $fileSystem,
        AdapterFactory $adapterFactory
    ) {
        $this->productCollection = $productCollection;
        $this->productInterface = $productInterface;
        $this->productRepositoryInterface = $productRepositoryInterface;
        $this->productRepository = $productRepository;
        $this->productModel = $productModel;
        $this->productVisibility = $productVisibility;
        $this->dataFactory = $dataFactory;
        $this->fileSystem = $fileSystem;
        $this->adapterFactory = $adapterFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function postGetProducts($limit, $how = 'collection')
    {
        $productCollection = $this->productCollection->create();
        return var_export($productCollection->getData(), true);
    }

    /**
     * {@inheritdoc}
     */
    public function getGetProducts($limit, $how = 'collection')
    {
        $return = [];
        switch ($how) {
            case 'repository':
                $return = $this->getProductRepositoryResponse($limit);
                break;
            case 'repositoryinterface':
                $return = $this->getProductRepositoryInterfaceResponse($limit);
                break;
            case 'collection':
                $return = $this->getProductCollectionResponse($limit);
                break;
            case 'interface':
                $return = $this->getProductInterfaceResponse($limit);
                break;
            case 'model':
                $return = $this->getProductModelResponse($limit);
                break;
        }

        $dataProvider = $this->dataFactory->create();
        return $dataProvider->setProducts([$return]);
    }

    private function getProductRepositoryResponse($limit) : array
    {
        return [];
    }

    private function getProductRepositoryInterfaceResponse($limit) : array
    {
        return [];
    }

    private function getProductCollectionResponse($limit)
    {
        $collection = $this->productCollection->create()
            ->setVisibility($this->productVisibility->getVisibleInCatalogIds())
            ->addFieldToSelect('entity_id')
            ->addFieldToSelect('name')
            ->addFieldToSelect('description')
            ->addFieldToSelect('status')
            ->addFieldToSelect('price')
            ->addFieldToSelect('karavanium_user_id')
            ->addFieldToSelect('karavanium_sku')
            ->joinField(
                'qty',
                'cataloginventory_stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            )
            ->joinTable('cataloginventory_stock_item', 'product_id=entity_id', ['stock_status' => 'is_in_stock'])
            ->setPageSize($limit);

        $products = [];

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $mediaDirectory = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        $items = $collection->getItems();
        foreach ($items as $item) {
            $products[] = array_merge(
                $item->getData(),
                ['images' => $this->loadImages($item['entity_id'], $objectManager, $mediaDirectory)]
            );
        }

        return $products;
    }

    private function loadImages($entityId, $objectManager, $mediaDirectory)
    {

        $helperImport = $objectManager->get(Image::class);
        $productImage = $objectManager->create(ProductModel::class);
        $productImage->load($entityId);
        $imageUrl = $helperImport->init($productImage, 'product_page_image_base')
            ->setImageFile($productImage->getImage()) // image,small_image,thumbnail
            ->resize(380)
            ->getUrl();

        return $this->imageResponse($entityId, 1, 1, $this->resizeImage($imageUrl, $mediaDirectory));

//        $baseImageId = $this->getBaseImageId($productImage->getMediaGalleryEntries());
//        return $this->getMediaGalleryByProduct(
//            $productImage->getId(),
//            $productImage->getMediaGalleryImages()->getItems(),
//            $baseImageId,
//            $mediaDirectory
//        );
    }

    private function getBaseImageId($mediaEntries)
    {
        $result = 0;
        foreach ($mediaEntries as $media) {
            if (!in_array('image', $imageType = $media->getTypes())) {
                continue;
            }

            $result = $media->getId();
        }

        return $result;
    }

    private function getMediaGalleryByProduct($productId, $mediaItems, $baseImageId, $mediaDirectory)
    {
        $baseImage = [];
        $otherImages = [];
        foreach ($mediaItems as $mediaItem) {
            if ($mediaItem->getValueId() == $baseImageId) {
                $isImageBase = ($mediaItem->getValueId() == $baseImageId) ? 1 : 0;
                $baseImage[] = $this->imageResponse($productId, $mediaItem->getValueId(), $isImageBase, $this->resizeImage($mediaItem->getUrl(), $mediaDirectory));
                continue;
            }
            $isImageBase = ($mediaItem->getValueId() == $baseImageId) ? 1 : 0;
            $otherImages[] = $this->imageResponse($productId, $mediaItem->getValueId(), $isImageBase, $this->resizeImage($mediaItem->getUrl(), $mediaDirectory));
        }

        return array_merge($baseImage, $otherImages);
    }

    private function imageResponse($productId, $valueId, $isImageBase, $imageUrl)
    {
        return [
            'product_id'   => (int) $productId,
            'image_id'     => (int) $valueId,
            'base_image'   => (int) $isImageBase,
            'url'          => (string) $imageUrl
        ];
    }
    private function resizeImage($imagePath, $mediaDirectory)
    {
        /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageFactory */

        if (empty($this->image_size) || !$mediaDirectory->isFile($imagePath)) {
            return $imagePath;
        }

        $mediaFolder = "products/karavanium_cache/$this->image_size";

        $cachePath = strstr($imagePath, 'product');
        $urlOfImage = strstr($imagePath, 'catalog', true);

        $imageResize = $mediaDirectory->getAbsolutePath($mediaFolder) . '/' . $cachePath;

        $imageFactory = $this->adapterFactory->create();
        if ($mediaDirectory->isFile($imageResize)) {
            return $urlOfImage . $mediaFolder . '/' . $cachePath;
        }

        $imageFactory->open($imagePath);
        $originalWidth = $imageFactory->getOriginalWidth();
        $originalHeight = $imageFactory->getOriginalHeight();
        $oldAspectRatio = $originalWidth / $originalHeight;
        $newAspectRatio = $this->image_size / $this->image_size;
        if ($originalWidth < $this->image_size || $originalHeight < $this->image_size) {
            return $imagePath;
        }

        $imageFactory->constrainOnly(true);
        $imageFactory->keepTransparency(true);
        $imageFactory->backgroundColor([255, 255, 255]);
        $imageFactory->keepAspectRatio(true);
        $imageFactory->keepFrame(false);

        if ($oldAspectRatio > $newAspectRatio) {
            // original image is wider than the desired dimensions
            $imageFactory->resize(null, $this->image_size);
            $imageFactory->crop(0, 0, 0, 0);
        } else {
            // it's taller...
            $imageFactory->resize($this->image_size, null);
            $imageFactory->crop(0, 0, 0, 0);
        }

        try {
            $imageFactory->save($imageResize);
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }

    private function getProductInterfaceResponse($limit) : array
    {
        return [];
    }

    private function getProductModelResponse($limit) : array
    {
        return [];
    }
}
