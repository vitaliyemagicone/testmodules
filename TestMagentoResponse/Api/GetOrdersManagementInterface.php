<?php


namespace Emagicone\TestMagentoResponse\Api;

interface GetOrdersManagementInterface
{

    /**
     * POST for getOrders api
     * @param string $limit
     * @return string
     */
    public function postGetOrders($limit);

    /**
     * GET for getOrders api
     * @param string $limit
     * @return string
     */
    public function getGetOrders($limit);
}
