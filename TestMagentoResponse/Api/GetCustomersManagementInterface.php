<?php


namespace Emagicone\TestMagentoResponse\Api;

interface GetCustomersManagementInterface
{

    /**
     * POST for getCustomers api
     * @param string $limit
     * @return string
     */
    public function postGetCustomers($limit);

    /**
     * GET for getCustomers api
     * @param string $limit
     * @return string
     */
    public function getGetCustomers($limit);
}
