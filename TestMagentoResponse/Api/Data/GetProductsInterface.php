<?php
namespace Emagicone\TestMagentoResponse\Api\Data;

interface GetProductsInterface
{

    /**
     * Get name
     *
     * @return string
     */
    public function getProducts();

    /**
     * Set name
     *
     * @param array $products
     * @return array
     */
    public function setProducts($products);
}