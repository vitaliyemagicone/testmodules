<?php


namespace Emagicone\TestMagentoResponse\Api;

interface GetCategoriesManagementInterface
{

    /**
     * POST for getCategories api
     * @param string $limit
     * @return string
     */
    public function postGetCategories($limit);

    /**
     * GET for getCategories api
     * @param string $limit
     * @return string
     */
    public function getGetCategories($limit);
}
