<?php

namespace Emagicone\TestMagentoResponse\Api;

interface GetProductsManagementInterface
{

    /**
     * POST for getProducts api
     *
     * @param int $limit
     * @param string $how
     *
     * @return \Emagicone\TestMagentoResponse\Api\Data\GetProductsInterface[] containing Tree objects
     */
    public function postGetProducts($limit, $how);

    /**
     * GET for getProducts api
     *
     * @param int $limit
     * @param string $how
     *
     * @return \Emagicone\TestMagentoResponse\Api\Data\GetProductsInterface[] containing Tree objects
     */
    public function getGetProducts($limit, $how);
}
